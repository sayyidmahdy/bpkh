<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {

    /**
     * Kirim email dengan SMTP Gmail.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('file');
        $this->load->helper('download');
    }

    public function index()
    {
      // Konfigurasi email
        $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => 'sayyidmhd@gmail.com',  // Email gmail
            'smtp_pass'   => 'mahduul95',  // Password gmail
            'smtp_crypto' => 'tls',
            'smtp_port'   => 25,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];

        // Load library email dan konfigurasinya
        $this->load->library('email');

        // Email dan nama pengirim
        $this->email->from('no-reply@gmail.com', 'MasRud.com');

        // Email penerima
        $this->email->to('sayyidmhd@gmail.com'); // Ganti dengan email tujuan

        // Lampiran email, isi dengan url/path file
        $this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

        // Subject email
        $this->email->subject('Kirim Email dengan SMTP Gmail CodeIgniter | MasRud.com');

        // Isi email
        $this->email->message("Ini adalah contoh email yang dikirim menggunakan SMTP Gmail pada CodeIgniter.<br><br> Klik <strong><a href='https://masrud.com/post/kirim-email-dengan-smtp-gmail' target='_blank' rel='noopener'>disini</a></strong> untuk melihat tutorialnya.");
       	
       	
        // Tampilkan pesan sukses atau error
        if ($this->email->send()) {
            echo 'Sukses! email berhasil dikirim.';
        } else {
            echo 'Error! email tidak dapat dikirim.';
            dd($this->email->print_debugger());
        }
    }

    public function export_database(){
        $data['data'] = $this->db->select('*')->from('md_database a')->join('ci_users b', 'a.user_id = b.id')->get()->result();
        $data['view'] = 'export_database';
        $this->load->view('admin/layout', $data);
    }

    function backup_db()
	{
		// Load the DB utility class
		$this->load->dbutil();
		$date = date('Ymd-His');

		$config = [
			'format' => 'zip',
			'filename' => 'bpkh_backup_' .$date,
			'add_drop' => TRUE,
			'add_insert' => TRUE,
			'neweline' => '\n',
			'foreign_key_checks' => FALSE,
		];
		
		$backup = $this->dbutil->backup($config); 
		$filename = 'db_bpkh_' .$date. '.zip';

        $save = './/backup_database/'.$filename;
                
        if (write_file($save, $backup)) {
            $arrData = [
                'user_id' => $this->session->userdata('user_id'),
                'filename' => $filename
            ];
            
            $this->db->insert('md_database', $arrData);
            
            $dataLog = [
                'user_id' => $this->session->userdata('user_id'),
                'modul' => 'Backup Database',
                'action' => 'Generate',
                'data' => $filename,
            ];

            write_log($dataLog);
            
            force_download($filename, $backup);

            echo 'success';
        }
		
	}

}