<section class="content-header">
	<h1><i class="fa fa-user"></i> &nbsp; Export database </h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-lg-10">&nbsp;</div>
		<div class="col-lg-2">
			<a href="<?php echo base_url('email/backup_db') ?>" class="btn btn-primary btn-block" >Export</a>
		</div>
	</div>
	<div class="box mt-4">
		<div class="box-body table-responsive">
			<table  class="table table-bordered table-striped datatabless">
				<thead>
					<tr>
						<th>No</th>
						<th>Generate Date</th>
						<th>User</th>
						<th>Filename</th>
						<th>Action</th>
				</thead>
				<tbody>'
					<?php $no=1; foreach ($data as $row): ?>
						<tr>
							<td><?php echo $no++ ?></td>
							<td><?php echo $row->date ?></td>
							<td><?php echo $row->username ?></td>
							<td><?php echo $row->filename ?></td>
							<td><?php echo $row->filename ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>

			</table>      
		</div>
	</div>
</section>

<script>
	$(document).ready(function (){
	   	var table = $('.datatabless').DataTable({
	      pageLength: 10,
	   	});


		$('#buttons').on('click', function(e){
		   e.preventDefault();

		   var data = table.$('input[type="checkbox"]').serializeArray();

		   $.ajax({
		      url: base_url + 'user_management/test',
		      data: data
		   }).done(function(response){
		      console.log('Response', response);
		   });
		});
	});
</script>
	