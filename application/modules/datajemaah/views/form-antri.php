<?php if ($id): ?>
<form role="form" action="<?php echo base_url() ?>datajemaah/submit_antri/<?php echo $id ?>" method="POST">
<?php else: ?>
<form role="form" action="<?php echo base_url() ?>datajemaah/submit_antri" method="POST">
<?php endif ?>
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
	<input type="hidden" name="data[kat_data_jemaah]" value="2">
	<div class="box-body">
		<?php if ($id): ?>
			<?php if ($tahun): ?>
				<div class="form-group">
					<label >Tahun</label>
					<input type="number" class="form-control" name="data[tahun]" value="<?php echo $tahun->tahun ?>" required readonly>
				</div>
			<?php else: ?>
				<div class="form-group">
					<label >Tahun</label>
					<select class="form-control" name="data[tahun]" required>
						<option value="">-- Silahkan Pilih Tahun --</option>
						<?php foreach ($data as $row): ?>
							<option value="<?php echo $row->tahun ?>"><?php echo $row->tahun ?></option>
						<?php endforeach ?>
					</select>
				</div>
			<?php endif ?>
		<?php else: ?>
			<div class="form-group">
				<label >Tahun</label>
				<input type="number" class="form-control" name="data[tahun]" required>
			</div>
		<?php endif ?>
		<?php if (!$id || $id == 'edit'): ?>
			<div class="form-group">
				<label >Total Jamaah Antri</label>
				<input type="text" class="form-control" name="data[jumlah]" required>
			</div>
		<?php endif ?>
	</div>
	<button type="submit" style="display: none;" id="btn-submit">submit</button>
</form>

<script>
	function submit(){
		$('#btn-submit').click();
	}
</script>