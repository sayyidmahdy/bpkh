<?php defined('BASEPATH') or exit('No direct script access allowed');

class Makro extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('makro_model', 'makro_model');
	}

	public function index($tahun = 0)
	{
		$data['view'] = 'index';
		$this->load->view('admin/layout', $data);
	}

	public function gdp_ina()
	{
		$data['json_gdp_ina'] = json_encode($this->makro_model->get_gdp_ina());
		$data['view'] = 'visitor/makro/gdp_ina';

		$this->load->view('admin/layout', $data);
	}

	public function form_gdp_ina($id = null){
		$data['id'] = $id;
		if ($id) {
			$data['data'] = $this->db->from('gdp_ina')->order_by('tahun', 'desc')->get()->result();
		}
		$this->load->view('makro/form-gdp-ina', $data);
	}

	public function submit_gdp_ina($id = null){
		$data = $this->input->post('data');
		$is_exist = $this->db->select('*')->from('gdp_ina')->where('tahun', $data['tahun'])->get()->row();
		if ($id) {
			if ($data['tahun'] == '') {
				$msg = 'error|Tahun GDP Indonesia belum di pilih';
			}
			else {
				if ($is_exist) {
					switch ($id) {
						case 'edit':
							$exec = $this->db->where('id', $is_exist->id)->update('gdp_ina', $data);
							if ($exec) {
								$msg = 'success|Data GDP Indonesia berhasil di ubah';
							}
							else {
								$msg = 'error|Data GDP Indonesia gagal di ubah';
							}

							break;
						
						default:
							$exec = $this->db->where('id', $is_exist->id)->delete('gdp_ina');
							if ($exec) {
								$msg = 'success|Data GDP Indonesia berhasil di hapus';
							}
							else {
								$msg = 'error|Data GDP Indonesia gagal di hapus';
							}
							break;
					}
					
				}
			}

		}
		else {
			if ($is_exist) {
				$msg = 'error|Data GDP Indonesia pada tahun ' .$data['tahun']. ' sudah terdaftar';
			}
			else {
				$exec = $this->db->insert('gdp_ina', $data);

				if ($exec) {
					$msg = 'success|Data GDP Indonesia berhasil di simpan';
				}
				else {
					$msg = 'error|Data GDP Indonesia gagal di simpan';
				}
			}

		}

		$this->session->set_flashdata('message', $msg);
		redirect('makro/gdp_ina');
	}

	public function tambah_gdp_ina()
	{
		$data = array(
			'tahun' => $this->input->post('tahun'),
			'gdp_ina' => $this->input->post('gdp_ina'),
			'date' => date('Y-m-d : h:i:s'),
		);

		$id = $this->makro_model->tambah_gdp_ina($data);
		$status = true;

		$data = $this->makro_model->get_gdp_ina_by_id($id);

		echo json_encode(array("status" => $status, 'data' => $data));
	}

	public function hapus_gdp_ina($id = 0, $uri = NULL)
	{
		$this->db->delete('gdp_ina', array('id' => $id));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('makro/gdp_ina'));
	}

	public function gdp_ksa()
	{
		$data['json_gdp_ksa'] = json_encode($this->makro_model->get_gdp_ksa());
		$data['view'] = 'visitor/makro/gdp_ksa';
		$this->load->view('admin/layout', $data);
	}

	public function form_gdp_ksa($id = null){
		$data['id'] = $id;
		if ($id) {
			$data['data'] = $this->db->from('gdp_ksa')->order_by('tahun', 'desc')->get()->result();
		}
		$this->load->view('makro/form-gdp-ksa', $data);
	}

	public function submit_gdp_ksa($id = null){
		$data = $this->input->post('data');
		$is_exist = $this->db->select('*')->from('gdp_ksa')->where('tahun', $data['tahun'])->get()->row();
		if ($id) {
			if ($data['tahun'] == '') {
				$msg = 'error|Tahun GDP Kerajaan Saudi Arabia belum di pilih';
			}
			else {
				if ($is_exist) {
					switch ($id) {
						case 'edit':
							$exec = $this->db->where('id', $is_exist->id)->update('gdp_ksa', $data);
							if ($exec) {
								$msg = 'success|Data GDP Kerajaan Saudi Arabia berhasil di ubah';
							}
							else {
								$msg = 'error|Data GDP Kerajaan Saudi Arabia gagal di ubah';
							}
							break;
						
						default:
							$exec = $this->db->where('id', $is_exist->id)->delete('gdp_ksa');
							if ($exec) {
								$msg = 'success|Data GDP Kerajaan Saudi Arabia berhasil di hapus';
							}
							else {
								$msg = 'error|Data GDP Kerajaan Saudi Arabia gagal di hapus';
							}
							break;
					}
				}
			}

		}
		else {
			if ($is_exist) {
				$msg = 'error|Data GDP Kerajaan Saudi Arabia pada tahun ' .$data['tahun']. ' sudah terdaftar';
			}
			else {
				$exec = $this->db->insert('gdp_ksa', $data);

				if ($exec) {
					$msg = 'success|Data GDP Kerajaan Saudi Arabia berhasil di simpan';
				}
				else {
					$msg = 'error|Data GDP Kerajaan Saudi Arabia gagal di simpan';
				}
			}

		}

		$this->session->set_flashdata('message', $msg);
		redirect('makro/gdp_ksa');
	}

	public function tambah_gdp_ksa()
	{
		$data = array(
			'tahun' => $this->input->post('tahun'),
			'gdp_ksa' => $this->input->post('gdp_ksa'),
			'date' => date('Y-m-d : h:i:s'),
		);

		$id = $this->makro_model->tambah_gdp_ksa($data);
		$status = true;

		$data = $this->makro_model->get_gdp_ksa_by_id($id);

		echo json_encode(array("status" => $status, 'data' => $data));
	}

	public function hapus_gdp_ksa($id = 0, $uri = NULL)
	{
		$this->db->delete('gdp_ksa', array('id' => $id));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('makro/gdp_ksa'));
	}

	// INFLASI

	public function inflasi()
	{
		$data['json_inflasi'] = json_encode($this->makro_model->get_inflasi());
		$data['view'] = 'visitor/makro/inflasi';
		$this->load->view('admin/layout', $data);
	}

	public function form_inflasi($id = null){
		$data['id'] = $id;
		if ($id) {
			$data['data'] = $this->db->from('inflasi')->order_by('tahun', 'desc')->get()->result();
		}
		$this->load->view('makro/form-inflasi', $data);
	}

	public function submit_inflasi($id = null){
		$data = $this->input->post('data');
		$is_exist = $this->db->select('*')->from('inflasi')->where('tahun', $data['tahun'])->get()->row();
		if ($id) {
			if ($data['tahun'] == '') {
				$msg = 'error|Tahun Inflasi belum di pilih';
			}
			else {
				if ($is_exist) {
					switch ($id) {
						case 'edit':
							$exec = $this->db->where('id', $is_exist->id)->update('inflasi', $data);
							if ($exec) {
								$msg = 'success|Data Inflasi berhasil di ubah';
							}
							else {
								$msg = 'error|Data Inflasi gagal di ubah';
							}
							
							break;
						
						default:
							$exec = $this->db->where('id', $is_exist->id)->delete('inflasi');
							if ($exec) {
								$msg = 'success|Data Inflasi berhasil di hapus';
							}
							else {
								$msg = 'error|Data Inflasi gagal di hapus';
							}
							break;
					}
				}
			}

		}
		else {
			if ($is_exist) {
				$msg = 'error|Data Inflasi pada tahun ' .$data['tahun']. ' sudah terdaftar';
			}
			else {
				$exec = $this->db->insert('inflasi', $data);

				if ($exec) {
					$msg = 'success|Data Inflasi berhasil di simpan';
				}
				else {
					$msg = 'error|Data Inflasi gagal di simpan';
				}
			}

		}

		$this->session->set_flashdata('message', $msg);
		redirect('makro/inflasi');
	}


	public function tambah_inflasi()
	{
		$data = array(
			'tahun' => $this->input->post('tahun'),
			'inflasi' => $this->input->post('inflasi'),
			'date' => date('Y-m-d : h:i:s'),
		);

		$id = $this->makro_model->tambah_inflasi($data);
		$status = true;

		$data = $this->makro_model->get_inflasi_by_id($id);

		echo json_encode(array("status" => $status, 'data' => $data));
	}

	public function hapus_inflasi($id = 0, $uri = NULL)
	{
		$this->db->delete('inflasi', array('id' => $id));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('makro/inflasi'));
	}

	// HARGA EMAS
	public function harga_emas()
	{
		$data['data'] = json_encode($this->makro_model->get_harga_emas());
		$data['view'] = 'visitor/makro/harga_emas';
		$this->load->view('admin/layout', $data);
	}

	public function tambah_harga_emas()
	{
		$data = array(
			'tahun' => $this->input->post('tahun'),
			'harga_emas' => $this->input->post('harga_emas'),
			'date' => date('Y-m-d : h:i:s'),
		);

		$id = $this->makro_model->tambah_harga_emas($data);
		$status = true;

		$data = $this->makro_model->get_harga_emas_by_id($id);

		echo json_encode(array("status" => $status, 'data' => $data));
	}

	public function hapus_harga_emas($id = 0, $uri = NULL)
	{
		$this->db->delete('harga_emas', array('id' => $id));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('makro/harga_emas'));
	}

	public function indeks_saham_syariah()
	{
		$data['json_saham'] = json_encode($this->makro_model->get_indeks_saham_syariah());
		$data['view'] = 'visitor/makro/indeks_saham_syariah';
		$this->load->view('admin/layout', $data);
	}

	public function form_indeks_saham_syariah($id = null){
		$data['id'] = $id;
		if ($id) {
			$data['data'] = $this->db->from('indeks_saham_syariah')->order_by('tahun', 'desc')->get()->result();
		}
		$this->load->view('makro/form-indeks-saham-syariah', $data);
	}

	public function submit_indeks_saham_syariah($id = null){
		$data = $this->input->post('data');
		$is_exist = $this->db->select('*')->from('indeks_saham_syariah')->where('tahun', $data['tahun'])->get()->row();
		if ($id) {
			if ($data['tahun'] == '') {
				$msg = 'error|Tahun Indeks Saham Syariah belum di pilih';
			}
			else {
				if ($is_exist) {
					switch ($id) {
						case 'edit':
							$exec = $this->db->where('id', $is_exist->id)->update('indeks_saham_syariah', $data);
							if ($exec) {
								$msg = 'success|Data Indeks Saham Syariah berhasil di ubah';
							}
							else {
								$msg = 'error|Data Indeks Saham Syariah gagal di ubah';
							}
							
							break;
						
						default:
							$exec = $this->db->where('id', $is_exist->id)->delete('indeks_saham_syariah');
							if ($exec) {
								$msg = 'success|Data Indeks Saham Syariah berhasil di hapus';
							}
							else {
								$msg = 'error|Data Indeks Saham Syariah gagal di hapus';
							}
							break;
					}
				}
			}

		}
		else {
			if ($is_exist) {
				$msg = 'error|Data Indeks Saham Syariah pada tahun ' .$data['tahun']. ' sudah terdaftar';
			}
			else {
				$exec = $this->db->insert('indeks_saham_syariah', $data);

				if ($exec) {
					$msg = 'success|Data Indeks Saham Syariah berhasil di simpan';
				}
				else {
					$msg = 'error|Data Indeks Saham Syariah gagal di simpan';
				}
			}

		}

		$this->session->set_flashdata('message', $msg);
		redirect('makro/indeks_saham_syariah');
	}

	public function tambah_indeks_saham_syariah()
	{
		$data = array(
			'tahun' => $this->input->post('tahun'),
			'indeks_saham_syariah' => $this->input->post('indeks_saham_syariah'),
			'date' => date('Y-m-d : h:i:s'),
		);

		$id = $this->makro_model->tambah_indeks_saham_syariah($data);
		$status = true;

		$data = $this->makro_model->get_indeks_saham_syariah_by_id($id);

		echo json_encode(array("status" => $status, 'data' => $data));
	}

	public function hapus_indeks_saham_syariah($id = 0, $uri = NULL)
	{
		$this->db->delete('indeks_saham_syariah', array('id' => $id));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('makro/indeks_saham_syariah'));
	}

	// harga avtur 

	public function harga_avtur()
	{
		$data['data'] = json_encode($this->makro_model->get_harga_avtur());
		$data['view'] = 'visitor/makro/harga_avtur';
		$this->load->view('admin/layout', $data);
	}

	public function tambah_harga_avtur()
	{
		$data = array(
			'tahun' => $this->input->post('tahun'),
			'harga_avtur' => $this->input->post('harga_avtur'),
			'date' => date('Y-m-d : h:i:s'),
		);

		$id = $this->makro_model->tambah_harga_avtur($data);
		$status = true;

		$data = $this->makro_model->get_harga_avtur_by_id($id);

		echo json_encode(array("status" => $status, 'data' => $data));
	}

	public function hapus_harga_avtur($id = 0, $uri = NULL)
	{
		$this->db->delete('harga_avtur', array('id' => $id));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('makro/harga_avtur'));
	}

	// SB LPS
	public function suku_bunga_lps()
	{
		$data['data'] = json_encode($this->makro_model->get_suku_bunga_lps());
		$data['view'] = 'visitor/makro/suku_bunga_lps';
		$this->load->view('admin/layout', $data);
	}

	public function form_suku_bunga_lps($id = null){
		$data['id'] = $id;
		if ($id) {
			$data['data'] = $this->db->from('suku_bunga_lps')->order_by('tahun', 'desc')->get()->result();
		}
		$this->load->view('makro/form-suku-bunga-lps', $data);
	}

	public function submit_suku_bunga_lps($id = null){
		$data = $this->input->post('data');
		$is_exist = $this->db->select('*')->from('suku_bunga_lps')->where('tahun', $data['tahun'])->get()->row();
		if ($id) {
			if ($data['tahun'] == '') {
				$msg = 'error|Tahun Suku Bunga LPS belum di pilih';
			}
			else {
				if ($is_exist) {
					switch ($id) {
						case 'edit':
							$exec = $this->db->where('id', $is_exist->id)->update('suku_bunga_lps', $data);
							if ($exec) {
								$msg = 'success|Data Suku Bunga LPS berhasil di ubah';
							}
							else {
								$msg = 'error|Data Suku Bunga LPS gagal di ubah';
							}
							
							break;
						
						default:
							$exec = $this->db->where('id', $is_exist->id)->delete('suku_bunga_lps');
							if ($exec) {
								$msg = 'success|Data Suku Bunga LPS berhasil di hapus';
							}
							else {
								$msg = 'error|Data Suku Bunga LPS gagal di hapus';
							}
							break;
					}
				}
			}

		}
		else {
			if ($is_exist) {
				$msg = 'error|Data Suku Bunga LPS pada tahun ' .$data['tahun']. ' sudah terdaftar';
			}
			else {
				$exec = $this->db->insert('suku_bunga_lps', $data);

				if ($exec) {
					$msg = 'success|Data Suku Bunga LPS berhasil di simpan';
				}
				else {
					$msg = 'error|Data Suku Bunga LPS gagal di simpan';
				}
			}

		}

		$this->session->set_flashdata('message', $msg);
		redirect('makro/suku_bunga_lps');
	}

	public function tambah_suku_bunga_lps()
	{
		$data = array(
			'tahun' => $this->input->post('tahun'),
			'suku_bunga_lps' => $this->input->post('suku_bunga_lps'),
			'date' => date('Y-m-d : h:i:s'),
		);

		$id = $this->makro_model->tambah_suku_bunga_lps($data);
		$status = true;

		$data = $this->makro_model->get_suku_bunga_lps_by_id($id);

		echo json_encode(array("status" => $status, 'data' => $data));
	}

	public function hapus_suku_bunga_lps($id = 0, $uri = NULL)
	{
		$this->db->delete('suku_bunga_lps', array('id' => $id));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('makro/suku_bunga_lps'));
	}

	// YIELD
	public function yield_sukuk_negara()
	{
		$data['data'] = json_encode($this->makro_model->get_yield_sukuk_negara());
		$data['view'] = 'visitor/makro/yield_sukuk_negara';
		$this->load->view('admin/layout', $data);
	}

	public function tambah_yield_sukuk_negara()
	{
		$data = array(
			'tahun' => $this->input->post('tahun'),
			'yield_sukuk_negara' => $this->input->post('yield_sukuk_negara'),
			'date' => date('Y-m-d : h:i:s'),
		);

		$id = $this->makro_model->tambah_yield_sukuk_negara($data);
		$status = true;

		$data = $this->makro_model->get_yield_sukuk_negara_by_id($id);

		echo json_encode(array("status" => $status, 'data' => $data));
	}

	public function hapus_yield_sukuk_negara($id = 0, $uri = NULL)
	{
		$this->db->delete('yield_sukuk_negara', array('id' => $id));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('makro/yield_sukuk_negara'));
	}

	// nilai manfaat regulasi
	public function laporan_industri_keuangan_syariah($kategori = 0)
	{

		$kategori = ($kategori != '') ? $kategori : '';


		$data['dokumen'] = $this->makro_model->get_laporan_industri_keuangan_syariah($kategori);

		$data['view'] = 'visitor/makro/laporan_industri_keuangan_syariah';
		$this->load->view('admin/layout', $data);
	}

	public function tambah_laporan_industri_keuangan_syariah()
	{
		if ($this->input->post('submit')) {

			$this->form_validation->set_rules('nama_laporan_industri_keuangan_syariah', 'Judul laporan_industri_keuangan_syariah', 'trim|required');
	
			if ($this->form_validation->run() == FALSE) {

				$data['view'] = 'makro/tambah_laporan_industri_keuangan_syariah';
				$this->load->view('admin/layout', $data);
			} else {

				$upload_path = './uploads/laporan_industri_keuangan_syariah';

				if (!is_dir($upload_path)) {
					mkdir($upload_path, 0777, TRUE);
				}

				$config = array(
					'upload_path' => $upload_path,
					'allowed_types' => "doc|docx|xls|xlsx|ppt|pptx|odt|rtf|jpg|png|pdf",
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config);
				$this->upload->do_upload('file_laporan_industri_keuangan_syariah');
				$laporan_industri_keuangan_syariah= $this->upload->data();


				$data = array(
					'nama_dokumen' => $this->input->post('nama_laporan_industri_keuangan_syariah'),
					'date' => date('Y-m-d'),
					'upload_by' => $this->session->userdata('user_id'),
					'file' => $upload_path . '/' . $laporan_industri_keuangan_syariah['file_name'],
				
				);

				$data = $this->security->xss_clean($data);
				$result = $this->makro_model->tambah_laporan_industri_keuangan_syariah($data);

				$dataLog = [
		            'user_id' => $this->session->userdata('user_id'),
		            'modul' => 'Laporan Industri Keuangan Syariah',
		            'action' => 'Import Data',
		            'data' => json_encode($data),
		        ];

		        write_log($dataLog);

				if ($result) {
					$this->session->set_flashdata('message', 'success|Dokumen telah ditambahkan!');
				}
				else {
					$this->session->set_flashdata('message', 'error|Dokumen gagal ditambahkan!');
				}
				redirect(base_url('makro/laporan_industri_keuangan_syariah'));
			}
		} else {


			$data['view'] = 'makro/tambah_laporan_industri_keuangan_syariah';
			$this->load->view('admin/layout', $data);
		}
	}



	public function hapus_laporan_industri_keuangan_syariah($id = 0, $uri = NULL)
	{
		$this->db->delete('laporan_industri_keuangan_syariah', array('id' => $id));
		$this->session->set_flashdata('message', 'success|Data berhasil dihapus!');
		redirect(base_url('makro/laporan_industri_keuangan_syariah'));
	}
} //class
