<?php if ($id): ?>
<form role="form" action="<?php echo base_url() ?>makro/submit_gdp_ksa/<?php echo $id ?>" method="POST">
<?php else: ?>
<form role="form" action="<?php echo base_url() ?>makro/submit_gdp_ksa" method="POST">
<?php endif ?>
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
	<div class="box-body">
		<?php if ($id): ?>
			<div class="form-group">
				<label >Tahun</label>
				<select class="form-control" name="data[tahun]" required>
					<option value="">-- Silahkan Pilih Tahun --</option>
					<?php foreach ($data as $row): ?>
						<option value="<?php echo $row->tahun ?>"><?php echo $row->tahun ?></option>
					<?php endforeach ?>
				</select>
			</div>
		<?php else: ?>
			<div class="form-group">
				<label >Tahun</label>
				<input type="number" class="form-control" name="data[tahun]" required>
			</div>
		<?php endif ?>
		
		<?php if (!$id || $id == 'edit'): ?>
			<div class="form-group">
				<label >Total GDP Kerajaan Saudi Arabia</label>
				<input type="text" class="form-control" name="data[gdp_ksa]" required>
			</div>
		<?php endif ?>
	</div>
	<button type="submit" style="display: none;" id="btn-submit">submit</button>
</form>

<script>
	function submit(){
		$('#btn-submit').click();
	}
</script>