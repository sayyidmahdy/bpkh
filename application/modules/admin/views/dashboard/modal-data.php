<form action="<?php echo base_url('admin/dashboard/submit_data') ?>" method="POST">
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
	<input type="hidden" name="year" value="<?php echo $year ?>">
	<?php if ($type == 'penempatan'): ?>
		<div class="box-body">
			<div class="form-group">
				<label >Setoran Awal</label>
				<input type="text" class="form-control" name="data[setoran_awal]" value="<?php echo ($data) ? $data->setoran_awal : 0 ?>" required>
			</div>

			<div class="form-group">
				<label >Nilai Manfaat</label>
				<input type="text" class="form-control" name="data[setoran_lunas]" value="<?php echo ($data) ? $data->setoran_lunas : 0 ?>" required>
			</div>

			<div class="form-group">
				<label >Setoran Lunas</label>
				<input type="text" class="form-control" name="data[nilai_manfaat]" value="<?php echo ($data) ? $data->nilai_manfaat : 0 ?>" required>
			</div>

			<div class="form-group">
				<label >DAU</label>
				<input type="text" class="form-control" name="data[dau]" value="<?php echo ($data) ? $data->dau : 0 ?>" required>
			</div>
		</div>
	<?php elseif($type == 'label_audit'): ?>
		<div class="box-body">
			<input type="hidden" name="type" value="label_audit">
			<div class="form-group">
				<label >Label Audit Information</label>
				<input type="text" class="form-control" name="data[label_audit]" value="<?php echo ($data) ? $data->label_audit : 0 ?>" required>
			</div>
		</div>
	<?php else: ?>
		<div class="box-body">
			<div class="form-group">
				<label >Setoran Awal</label>
				<input type="text" class="form-control" name="data[setoran_awal_inv]" value="<?php echo ($data) ? $data->setoran_awal_inv : 0 ?>" required>
			</div>

			<div class="form-group">
				<label >DAU</label>
				<input type="text" class="form-control" name="data[dau_inv]" value="<?php echo ($data) ? $data->dau_inv : 0 ?>" required>
			</div>
		</div>
	<?php endif ?>

	<button type="submit" id="btn-submit-data" class="hide"></button>

	<!-- BACKUP FORM WITH  ThausandSeperator-->
	<!-- <?php if ($type == 'penempatan'): ?>
		<div class="box-body">
			<div class="form-group">
				<label >Setoran Awal</label>
				<input type="text" class="form-control"  value="<?php echo ($data->setoran_awal) ? $data->setoran_awal : 0 ?>" required onkeyup="this.value = ThausandSeperator('HIDDEN_SETORAN_AWAL', this.value, 4)">
				<input type="hidden" name="data[setoran_awal]" id="HIDDEN_SETORAN_AWAL">
			</div>

			<div class="form-group">
				<label >Nilai Manfaat</label>
				<input type="text" class="form-control"  value="<?php echo ($data->setoran_lunas) ? $data->setoran_lunas : 0 ?>" required onkeyup="this.value = ThausandSeperator('HIDDEN_SETORAN_LUNAS', this.value, 4)">
				<input type="hidden" name="data[setoran_lunas]" id="HIDDEN_SETORAN_LUNAS">
			</div>

			<div class="form-group">
				<label >Setoran Lunas</label>
				<input type="text" class="form-control"  value="<?php echo ($data->nilai_manfaat) ? $data->nilai_manfaat : 0 ?>" required onkeyup="this.value = ThausandSeperator('HIDDEN_NILAI_MANFAAT', this.value, 4)">
				<input type="hidden" name="data[nilai_manfaat]" id="HIDDEN_NILAI_MANFAAT">
			</div>

			<div class="form-group">
				<label >DAU</label>
				<input type="text" class="form-control"  value="<?php echo ($data->dau) ? $data->dau : 0 ?>" required onkeyup="this.value = ThausandSeperator('HIDDEN_DAU', this.value, 4)">
				<input type="hidden" name="data[dau]" id="HIDDEN_DAU">
			</div>
		</div>
	<?php else: ?>
		<div class="box-body">
			<div class="form-group">
				<label >Setoran Awal</label>
				<input type="text" class="form-control" value="<?php echo ($data->setoran_awal_inv) ? $data->setoran_awal_inv : 0 ?>" required onkeyup="this.value = ThausandSeperator('HIDDEN_SETORAN_AWAL_INV', this.value, 4)">
				<input type="hidden" name="data[setoran_awal_inv]" id="HIDDEN_SETORAN_AWAL_INV">
			</div>

			<div class="form-group">
				<label >DAU</label>
				<input type="text" class="form-control" value="<?php echo ($data->dau_inv) ? $data->dau_inv : 0 ?>" required onkeyup="this.value = ThausandSeperator('HIDDEN_DAU_INV', this.value, 4)">
				<input type="hidden" name="data[dau_inv]" id="HIDDEN_DAU_INV">
			</div>
		</div>
	<?php endif ?> -->
</form>