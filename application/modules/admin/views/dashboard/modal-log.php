<table  class="table table-bordered table-striped datatabless">
	<thead>
		<tr>
			<th style="width: 5%">#</th>
			<th>Modul</th>
			<th>Action</th>
			<th>Log Time</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach ($log as $rowLog): ?>
			<tr>
				<td width="5%"><?php echo $no++ ?></td>
				<td><?php echo $rowLog->modul ?></td>
				<td><?php echo $rowLog->action ?></td>
				<td><?php echo $rowLog->created_at ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<script type="text/javascript">
   	$('.datatabless').DataTable({
      pageLength: 10,
   	});
</script>