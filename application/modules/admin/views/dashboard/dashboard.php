<section class="content-header">
	<h1>
		Dashboard 
	</h1>
	<ol class="breadcrumb">
		<?php dropdown_year_dashboard() ?>
	</ol>
</section>
<style type="text/css">
	.small-box h3 {
	    font-size: 25px !important;
	    font-weight: bold;
	    margin: 0 0 10px 0;
	    white-space: nowrap;
	    padding: 0;
	}
</style>
<section class="content mt-2">
	<div class="row">
		<?php if (isset($result['label_audit'])): ?>
			<h5 class="col-lg-12 text-right"><i><?php echo $result['label_audit'] ?></i></h5>
		<?php endif ?>
		<div class="col-lg-6 p-0">
			<div class="col-lg-12 col-xs-12 ">
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>Rp <?php echo (isset($total_dana) ? $total_dana : 0) ?></h3>
						<p>Total Dana Kelolaan Per <?php echo (isset($result['periode']) ? $result['periode'] . ' ' . $result['tahun'] : '31 Desember ' . $year) ?> </p>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="javascript:void(0)" onclick="scroll_to_div('pie_chart_penempatan')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-6 col-xs-12 ">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3><?php echo $total_penempatan ?></h3>
						<p style="font-size:13px">Total Penempatan Per <?php echo (isset($result['periode']) ? $result['periode'] . ' ' . $result['tahun'] : '31 Desember ' . $year) ?></p>
					</div>
					<div class="icon">
						<!-- <i class="ion ion-bag"></i> -->
					</div>
					<a href="<?= base_url('keuanganhaji/sebaran_dana_haji/'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-6 col-xs-12 ">
				
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3><?php echo $total_investasi ?></h3>
						<p style="font-size:13px">Total Investasi Per <?php echo (isset($result['periode']) ? $result['periode'] . ' ' . $result['tahun'] : '31 Desember ' . $year) ?></p>
					</div>
					<div class="icon">
						<!-- <i class="ion ion-bag"></i> -->
					</div>
					<a href="<?= base_url('keuanganhaji/sbssn_rupiah/'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<?php if (is_admin() == 1): ?>
				<div class="col-lg-6 col-xs-12 ">
					<button type="button" class="btn btn-lg btn-block btn-primary" onclick="modal_data('penempatan')">Update Penempatan</button>
				</div>
				<div class="col-lg-6 col-xs-12 ">
					<button type="button" class="btn btn-lg btn-block btn-primary" onclick="modal_data('investasi')">Update Investasi</button>
				</div>
				<div class="col-lg-12 col-xs-12 mt-4">
					<button type="button" class="btn btn-lg btn-block btn-warning" onclick="modal_data('label_audit')">Update Audit Information</button>
				</div>
				<div class="col-md-12 mt-4">
					<div class="box" style="min-height: 320px;">
						<div class="box-header with-border">
							<div class="row px-2">
								<div class="col-lg-10">
									<h3 class="box-title ">Log Activity</h3>
								</div>
								<div class="col-lg-2 text-right">
									<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-block" onclick="modal_log()">Detail</a>
								</div>
							</div>
						</div>
						<div class="box-body pt-0">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 5%">#</th>
										<th>Modul</th>
										<th>Action</th>
										<th>Log Time</th>
									</tr>
								</thead>
								<tbody>
									<?php if ($log): ?>
										<?php $no = 1; foreach ($log as $rowLog): ?>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $rowLog->modul ?></td>
												<td><?php echo $rowLog->action ?></td>
												<td><?php echo $rowLog->created_at ?></td>
											</tr>
										<?php endforeach ?>
									<?php else: ?>
										<tr>
											<td colspan="4" class="text-center">Data Not Found</td>
										</tr>
									<?php endif ?>
								</tbody>
							</table>
							<small><i>Showing the last 5 activities</i></small>
						</div>
					</div>
				</div>
			<?php endif ?>

			
			<div class="col-lg-6 hide">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>Rp. 51.079T</h3>
						<p>Setoran Awal</p>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="javascript:void(0)" onclick="scroll_to_div('setoran_awal')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-6 hide">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>Rp. 51.079T</h3>
						<p>Setoran Lunas</p>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="javascript:void(0)" onclick="scroll_to_div('setoran_lunas')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-6 hide">
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>Rp. 51.079T</h3>
						<p>Nilai Manfaat</p>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="javascript:void(0)" onclick="scroll_to_div('nilai_manfaat')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-6 hide">
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>Rp. 51.079T</h3>
						<p>DAU</p>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="javascript:void(0)" onclick="scroll_to_div('dau')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<?php if (is_admin() == 1): ?>
				<div class="box" style="min-height: 704px;">
					<div class="box-header with-border">
						<div class="row px-2">
							<div class="col-lg-12">
								<h3 class="box-title ">User Active</h3>
							</div>
						</div>
					</div>
					<div class="box-body pt-0">
						<table class="table table-bordered datatables">
							<thead>
								<tr>
									<th style="width: 5%">#</th>
									<th>Username</th>
									<th>Email</th>
									<th>Last Activity</th>
								</tr>
							</thead>
							<tbody>
								<?php $no = 1; foreach ($user_active as $rowUser): ?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $rowUser->username ?></td>
										<td><?php echo $rowUser->email ?></td>
										<td><?php echo $rowUser->last_activity ?></td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			<?php else: ?>
				<div class="box" style="min-height: 320px;">
					<div class="box-header with-border">
						<div class="row px-2">
							<div class="col-lg-10">
								<h3 class="box-title ">Log Activity</h3>
							</div>
							<div class="col-lg-2 text-right">
								<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-block" onclick="modal_log()">Detail</a>
							</div>
						</div>
					</div>
					<div class="box-body pt-0">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th style="width: 5%">#</th>
									<th>Modul</th>
									<th>Action</th>
									<th>Log Time</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($log): ?>
									<?php $no = 1; foreach ($log as $rowLog): ?>
										<tr>
											<td><?php echo $no++ ?></td>
											<td><?php echo $rowLog->modul ?></td>
											<td><?php echo $rowLog->action ?></td>
											<td><?php echo $rowLog->created_at ?></td>
										</tr>
									<?php endforeach ?>
								<?php else: ?>
									<tr>
										<td colspan="4" class="text-center">Data Not Found</td>
									</tr>
								<?php endif ?>
							</tbody>
						</table>
						<small><i>Showing the last 5 activities</i></small>
					</div>
				</div>
			<?php endif ?>
		</div>
	</div>

	<!-- <div class="row">
		<div class="col-lg-12">
			<h2>
				Penempatan
			</h2>
		</div>
	</div> -->

	<div class="row">
		<div class="col-lg-12" id="pie_chart_penempatan">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Pie Chart Data Penempatan</h3>
				</div>
				<div class="box-body">
					<div id="pie_penempatan"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-12" id="pie_chart_penempatan">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Pie Chart Data Investasi</h3>
				</div>
				<div class="box-body">
					<div id="pie_investasi"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-12" id="chart_total_penempatan">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Chart Data Penempatan</h3>
				</div>
				<div class="box-body">
					<div id="chart_penempatan"></div>
				</div>
			</div>
		</div>

		<div class="col-lg-12" id="chart_total_investasi">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Chart Data Investasi</h3>
				</div>
				<div class="box-body">
					<div id="chart_setoran_lunas"></div>
				</div>
			</div>
		</div>
		
	</div>		

	<div class="row hide">
		<div class="col-md-6">
			<!-- DONUT CHART -->
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Penempatan Rp<?=$result['penempatan']; ?></h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<canvas id="penempatan" style="height:200px"></canvas>
					<p class="text-center" style="margin-top:50px;">
						<a href="<?= base_url('keuanganhaji/sebaran_dana_haji/'); ?>" class="btn btn-md btn-warning">
						Lihat Porsi Penempatan</a>
					</p>
					
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		
		<!-- /.col (LEFT) -->
		<div class="col-md-6">
			<!-- BAR CHART -->
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Investasi Rp<?=$result['investasi']; ?></h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<div class="chart">
						<canvas id="investasi" style="height:230px"></canvas>
						<p class="text-center" style="margin-top:50px;">
							<a href="<?= base_url('keuanganhaji/sbssn_rupiah/'); ?>" class="btn btn-md btn-success">Lihat Porsi
							Investasi</a>
						</p>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
			
	<div class="row hide">
		<div class="col-lg-6" id="setoran_awal">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Setoran Awal</h3>
				</div>
				<div class="box-body">
					<div id="chart_setoran_awal"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-6" id="setoran_lunas">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Setoran Lunas</h3>
				</div>
				<div class="box-body">
					<div id="chart_setoran_lunas"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-6" id="nilai_manfaat">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Nilai Manfaat</h3>
				</div>
				<div class="box-body">
					<div id="chart_nilai_manfaat"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-6" id="dau">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">DAU</h3>
				</div>
				<div class="box-body">
					<div id="chart_dau"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="row hide">
		<div class="col-lg-12">
			<h2>
				Investasi
			</h2>
		</div>
	</div>

	
</section>

<!-- /.content -->

<!-- ChartJS 1.0.1 -->
<!-- <script src="<?= base_url() ?>public/plugins/chartjs/Chart.min.js"></script> -->
<!-- <script src="<?= base_url() ?>public/plugins/chartjs/utils.js"></script> -->
<!-- FastClick -->
<!-- <script src="<?= base_url() ?>public/plugins/fastclick/fastclick.js"></script> -->
<!-- page script -->

<script>
	$(function(){
	    $('#tahun').val('<?php echo $year ?>');
	    // $('#tahun').trigger('change');
	});

	function change_year(value){
		var is_admin = '<?php echo is_admin() ?>';
		if (is_admin == 1) {
			var url = base_url +'admin/dashboard/index/' + value;
		}
		else {
			var url = base_url +'visitor/dashboard/index/' + value;
		}
		window.location.href = url;
	}

	function modal_log(){
		var is_admin = '<?php echo is_admin() ?>'

		if (is_admin == 1) {
			var url = base_url + 'admin/dashboard/modal_log';
		}
		else {
			var url = base_url + 'visitor/dashboard/modal_log';
		}

		var footer = '<div class="row">\
						<div class="col-lg-9">&nbsp;</div>\
						<div class="col-lg-3"><button type="button" class="btn btn-primary btn-block px-4" onclick="submit()">Submit</button></div>\
					</div>';

		Modal('form_menu', 'Detail Log Activity', url, '', 'modal-lg', 'auto');
	}

	function modal_data(type){
		var year = '<?php echo $year ?>'
		var url = base_url + '/admin/dashboard/modal_data/' + type + '/' + year;

		var footer = '<div class="row">\
						<div class="col-lg-9">&nbsp;</div>\
						<div class="col-lg-3"><button type="button" class="btn btn-primary btn-block px-4" onclick="submit()">Submit</button></div>\
					</div>';

		Modal('form_menu', 'Update Data', url, footer, 'modal-md', 'auto');
	}

	function submit(){
		$('#btn-submit-data').click();
	}
</script>

<style>
	#chart_penempatan {
	  width: 100%;
	  height: 280px !important;
	}

	#chart_setoran_lunas {
	  width: 100%;
	  height: 280px !important;
	}

	#chart_nilai_manfaat {
	  width: 100%;
	  height: 280px !important;
	}

	#chart_dau {
	  width: 100%;
	  height: 280px !important;
	}

	#pie_penempatan {
	  width: 100%;
	  height: 380px !important;
	}

	#pie_investasi {
	  width: 100%;
	  height: 380px !important;
	}
</style>

<script>
	function scroll_to_div(id) {
		$('html, body').animate({
	        scrollTop: $("#"+id).offset().top
	    }, 2000);
	}
</script>

<!-- <script src="https://cdn.amcharts.com/lib/4/themes/frozen.js"></script> -->

<!-- PENEMPATAN -->
<script>
	am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_animated);
		// Themes end

		var chart = am4core.create("chart_penempatan", am4charts.XYChart);
		var data = [];
		var json = '<?php echo $chart_penempatan ?>';

		if (json) {
			var jsonData = [];
			$.each(JSON.parse(json), function(index, value){
				var years = value.year;

				if (years == 'Jan' || years == 'Mei' || years == 'Aug') {
					value.lineColor = chart.colors.next();
				}

				jsonData.push(value);
			});
		}
		else {
			var jsonData = [{
			  "year": "Jan",
			  "income": 0,
			  "lineColor": chart.colors.next()
			}, {
			  "year": "Feb",
			  "income": 0,
			}, {
			  "year": "Mar",
			  "income": 0,
			}, {
			  "year": "Apr",
			  "income": 0,
			}, {
			  "year": "Mai",
			  "income": 0,
			  "lineColor": chart.colors.next()
			}, {
			  "year": "Jun",
			  "income": 0
			}, {
			  "year": "Jul",
			  "income": 0
			}, {
			  "year": "Aug",
			  "income": 0,
			  "lineColor": chart.colors.next()
			}, {
			  "year": "Sep",
			  "income": 0
			}, {
			  "year": "Okt",
			  "income": 0
			}, {
			  "year": "Nov",
			  "income": 0
			}, {
			  "year": "Des",
			  "income": 0
			}];
		}

		chart.data = jsonData;

		var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.ticks.template.disabled = true;
		categoryAxis.renderer.line.opacity = 0;
		categoryAxis.renderer.grid.template.disabled = true;
		categoryAxis.renderer.minGridDistance = 40;
		categoryAxis.dataFields.category = "year";
		categoryAxis.startLocation = 0.4;
		categoryAxis.endLocation = 0.6;


		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.tooltip.disabled = true;
		valueAxis.renderer.line.opacity = 0;
		valueAxis.renderer.ticks.template.disabled = true;
		valueAxis.min = 0;

		var lineSeries = chart.series.push(new am4charts.LineSeries());
		lineSeries.dataFields.categoryX = "year";
		lineSeries.dataFields.valueY = "income";
		lineSeries.tooltipText = "income: {valueY.value}";
		lineSeries.fillOpacity = 0.5;
		lineSeries.strokeWidth = 3;
		lineSeries.propertyFields.stroke = "lineColor";
		lineSeries.propertyFields.fill = "lineColor";

		var bullet = lineSeries.bullets.push(new am4charts.CircleBullet());
		bullet.circle.radius = 6;
		bullet.circle.fill = am4core.color("#fff");
		bullet.circle.strokeWidth = 3;

		chart.cursor = new am4charts.XYCursor();
		chart.cursor.behavior = "panX";
		chart.cursor.lineX.opacity = 0;
		chart.cursor.lineY.opacity = 0;

		chart.scrollbarX = new am4core.Scrollbar();
		chart.scrollbarX.parent = chart.bottomAxesContainer;

	}); // end am4core.ready()
</script>

<!-- INVESTASI -->
<script>
	am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_dataviz);
		am4core.useTheme(am4themes_animated);
		// Themes end

		var chart = am4core.create("chart_setoran_lunas", am4charts.XYChart);

		var data = [];

		var json = '<?php echo $chart_investasi ?>';

		if (json) {
			var jsonData = [];
			$.each(JSON.parse(json), function(index, value){
				var years = value.year;

				if (years == 'Jan' || years == 'Mei' || years == 'Aug') {
					value.lineColor = chart.colors.next();
				}

				jsonData.push(value);
			});
		}
		else {
			var jsonData = [{
			  "year": "Jan",
			  "income": 0,
			  "lineColor": chart.colors.next()
			}, {
			  "year": "Feb",
			  "income": 0,
			}, {
			  "year": "Mar",
			  "income": 0,
			}, {
			  "year": "Apr",
			  "income": 0,
			}, {
			  "year": "Mai",
			  "income": 0,
			  "lineColor": chart.colors.next()
			}, {
			  "year": "Jun",
			  "income": 0
			}, {
			  "year": "Jul",
			  "income": 0
			}, {
			  "year": "Aug",
			  "income": 0,
			  "lineColor": chart.colors.next()
			}, {
			  "year": "Sep",
			  "income": 0
			}, {
			  "year": "Okt",
			  "income": 0
			}, {
			  "year": "Nov",
			  "income": 0
			}, {
			  "year": "Des",
			  "income": 0
			}];
		}

		chart.data = jsonData;

		var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.ticks.template.disabled = true;
		categoryAxis.renderer.line.opacity = 0;
		categoryAxis.renderer.grid.template.disabled = true;
		categoryAxis.renderer.minGridDistance = 40;
		categoryAxis.dataFields.category = "year";
		categoryAxis.startLocation = 0.4;
		categoryAxis.endLocation = 0.6;


		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.tooltip.disabled = true;
		valueAxis.renderer.line.opacity = 0;
		valueAxis.renderer.ticks.template.disabled = true;
		valueAxis.min = 0;

		var lineSeries = chart.series.push(new am4charts.LineSeries());
		lineSeries.dataFields.categoryX = "year";
		lineSeries.dataFields.valueY = "income";
		lineSeries.tooltipText = "income: {valueY.value}";
		lineSeries.fillOpacity = 0.5;
		lineSeries.strokeWidth = 3;
		lineSeries.propertyFields.stroke = "lineColor";
		lineSeries.propertyFields.fill = "lineColor";

		var bullet = lineSeries.bullets.push(new am4charts.CircleBullet());
		bullet.circle.radius = 6;
		bullet.circle.fill = am4core.color("#fff");
		bullet.circle.strokeWidth = 3;

		chart.cursor = new am4charts.XYCursor();
		chart.cursor.behavior = "panX";
		chart.cursor.lineX.opacity = 0;
		chart.cursor.lineY.opacity = 0;

		chart.scrollbarX = new am4core.Scrollbar();
		chart.scrollbarX.parent = chart.bottomAxesContainer;

	}); // end am4core.ready()
</script>




<!-- PIE CHART -->
<script>
	am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_kelly);
		// Themes end

		var chart = am4core.create("pie_penempatan", am4charts.PieChart3D);
		chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

		chart.legend = new am4charts.Legend();

		chart.data = [
		  {
		    country: "Setoran Awal (Rp<?php echo ($result['setoran_awal']) ? $result['setoran_awal'] : 0 ?>)",
		    litres: <?=$result['setoran_awal_per']; ?>
		  },
		  {
		    country: "Setoran Lunas (Rp<?php echo ($result['setoran_lunas']) ? $result['setoran_lunas'] : 0 ?>)",
		    litres: <?=$result['setoran_lunas_per']; ?>
		  },
		  {
		    country: "Nilai Manfaat (Rp<?php echo ($result['nilai_manfaat']) ? $result['nilai_manfaat'] : 0 ?>)",
		    litres: <?=$result['nilai_manfaat_per']; ?>
		  },
		  {
		    country: "DAU (Rp<?php echo ($result['dau']) ? $result['dau'] : 0 ?>)",
		    litres: <?=$result['dau_per']; ?>
		  },
		];

		var series = chart.series.push(new am4charts.PieSeries3D());
		series.dataFields.value = "litres";
		series.dataFields.category = "country";

	}); 

	am4core.ready(function() {

		// Themes begin
		// am4core.useTheme(am4themes_spiritedaway);
		// Themes end

		var chart = am4core.create("pie_investasi", am4charts.PieChart3D);
		chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

		chart.legend = new am4charts.Legend();

		chart.data = [
		  {
		    country: "Setoran Awal (Rp<?=$result['setoran_awal_inv']; ?>)",
		    litres: <?=$result['setoran_awal_inv_per']; ?>
		  },
		  {
		    country: "DAU (Rp<?=$result['dau_inv']; ?>)",
		    litres: <?=$result['dau_inv_per']; ?>
		  },
		];

		var series = chart.series.push(new am4charts.PieSeries3D());
		series.dataFields.value = "litres";
		series.dataFields.category = "country";
		series.colors.list = [
		    am4core.color("#FF6F91"),
		    am4core.color("#845EC2"),
		    am4core.color("#D65DB1"),
		    am4core.color("#FF9671"),
		    am4core.color("#FFC75F"),
		    am4core.color("#F9F871")
		];
	}); // end am4core.ready()
</script>

<!-- HTML -->
<script src="https://cdn.amcharts.com/lib/4/themes/dataviz.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/kelly.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/spiritedaway.js"></script>