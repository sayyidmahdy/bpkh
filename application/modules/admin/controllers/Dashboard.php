<?php defined('BASEPATH') or exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory; 
use PhpOffice\PhpSpreadsheet\Style\Alignment; 
class Dashboard extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/dashboard_model', 'dashboard_model');
		$this->load->model('dashboard_model');
		$this->load->library('excel');
	}

	public function index($tahun=0)
	{
		$user_id = $this->session->userdata('user_id');
		$data['year'] = ($tahun == 0) ? date('Y') : $tahun;
		$is_exist = $this->db->from('dashboard')->where('tahun', $data['year'])->get()->row();

		if (!$is_exist) {
			$arrData = [
				'penempatan' => 0,
				'setoran_awal' => 0,
				'setoran_awal_per' => 0,
				'setoran_lunas' => 0,
				'setoran_lunas_per' => 0,
				'nilai_manfaat' => 0,
				'nilai_manfaat_per' => 0,
				'dau' => 0,
				'dau_per' => 0,
				'setoran_awal_inv' => 0,
				'setoran_awal_inv_per' => 0,
				'dau_inv' => 0,
				'dau_inv_per' => 0,
				'investasi' => 0,
				'akumulasi_dau' => 0,
				'penempatan_dau' => 0,
				'penempatan_dau_per' => 0,
				'investasi_dau' => 0,
				'investasi_dau_per' => 0,
				'total' => 0,
				'tahun' => $data['year'],
				'periode' => '31 Desember',
			];

			$this->db->insert('dashboard', $arrData);
		}

		$query = $this->db->from('dashboard')->where('tahun', $data['year'])->get();
		$data['result'] = $query->row_array();

		$data['log'] = $this->db->from('trx_log')->where('user_id', $user_id)->limit('5')->order_by('id_log', 'desc')->get()->result();

		$sql_total_penempatan =  $this->db->select("januari, februari, maret, april, mei, juni, juli, agustus, september, oktober, november, desember")
											->from('sebaran_dana_haji2')
											->where('tahun', $data['year'])
											->where('bps_bpih', 'total')
											->get()
											->row();

		$sql_total_investasi = $this->db->select('nilai, bulan')
											->from('sbssn_rupiah2')
											->where('tahun', $data['year'])
											->where_not_in('instrumen', ['total'])
											->get()
											->result();

		$arrMonth = $this->db->from('md_month')->order_by('id_month', 'asc')->get()->result();

		$total_dana_haji = 0;
		foreach ($arrMonth as $key) {
				$get_dana_haji = $this->db->select($key->name .' as bulan')
										->from('sebaran_dana_haji2')
										->where('tahun', $data['year'])
										->where('bps_bpih', 'total')
										->get()
										->row();

				if ($get_dana_haji) {
					if (strlen($get_dana_haji->bulan) != 0) {
						$total_dana_haji = (int) str_replace(',', '', $get_dana_haji->bulan);
					}
				}
		}


		$sql_penempatan_produk = $this->db->select('jumlah')
										->from('posisi_penempatan_produk')
										->where('tahun', $data['year'])
										->order_by('bulan', 'desc')
										->limit(1)
										->get()
										->row();
		if (!$sql_penempatan_produk) {
			$penempatan_produk = 0;
		}
		else {
			$penempatan_produk = $sql_penempatan_produk->jumlah;
		}
		$total_penempatan_produk = (int) str_replace(',', '', $penempatan_produk);

		$total_data_penempatan = $total_dana_haji;
		// $total_data_penempatan = sum_data_by_month($sql_total_penempatan);
		
		$data['total_penempatan'] = formatCurrency(number_format($total_data_penempatan));
		$data['chart_penempatan'] = ($sql_total_penempatan) ? convert_month_to_array($sql_total_penempatan) : NULL;

		/*--------------------------CALCULATE INVESTASI--------------------------*/
		$total_investasi = 0;
		$total_Januari = 0;
        $total_Februari = 0;
        $total_Maret = 0;
        $total_April = 0;
        $total_Mei = 0;
        $total_Juni = 0;
        $total_Juli = 0;
        $total_Agustus = 0;
        $total_September = 0;
        $total_Oktober = 0;
        $total_November = 0;
        $total_Desember = 0;

		foreach ($sql_total_investasi as $rowInvest) {
			(float) $format_nilai = convert_str($rowInvest->nilai);
			$month = konversiBulanAngkaKeNama($rowInvest->bulan);
			$months = $rowInvest->bulan;
			
			$total_investasi += $format_nilai;
			
			if ($months == $rowInvest->bulan) {
				${'total_' . $month} += $format_nilai;
			}
		}

		$arrInvest = [
			['year' => 'Jan','income' => $total_Januari],
			['year' => 'Feb','income' => $total_Februari],
			['year' => 'Mar','income' => $total_Maret],
			['year' => 'Apr','income' => $total_April],
			['year' => 'Mei','income' => $total_Mei],
			['year' => 'Jun','income' => $total_Juni],
			['year' => 'Jul','income' => $total_Juli],
			['year' => 'Aug','income' => $total_Agustus],
			['year' => 'Sep','income' => $total_September],
			['year' => 'Okt','income' => $total_Oktober],
			['year' => 'Nov','income' => $total_November],
			['year' => 'Des','income' => $total_Desember],
		];

		// $total_investasi = formatCurrency(number_format($total_investasi, 2));
		// $total_investasi = $this->db->select('per_sumber_kas_haji')
		// 						->from('alokasi_investasi')
		// 						->where('tahun', $data['year'])
		// 						->order_by('bulan', 'desc')
		// 						->limit(1)
		// 						->get()
		// 						->row();

		$total_investasi = 0;
		foreach ($arrMonth as $key) {
				$get_alokasi_investasi = $this->db->select($key->name .' as bulan')
										->from('tr_alokasi_investasi')
										->where('tahun', $data['year'])
										->where('bidang', 'Per Sumber Kas')
										->get()
										->row();
				
				if ($get_alokasi_investasi) {
					if (strlen($get_alokasi_investasi->bulan) != 0) {
						$total_investasi = (int) str_replace(',', '', $get_alokasi_investasi->bulan);
					}
				}
		}

		if (!$total_investasi) {
			$total_investasi = 0;
		}
		else {
			$total_investasi = (int) str_replace(',', '', $total_investasi);
		}
		/*-------------------sementara untuk golive-------------------*/

		if ($data['year'] > 2019) {
			// code...
			$total_investasi = $total_investasi . '000000000';
			$total_investasi = (int)$total_investasi;
		// dd($total_investasi);
		}
		/*-------------------sementara untuk golive-------------------*/
		$total_investasi_format = number_format($total_investasi);
		$data['total_investasi'] = formatCurrency($total_investasi_format);
		$data['chart_investasi'] = (count($sql_total_investasi) > 0) ? json_encode($arrInvest) : NULL;


		$total_dana = $total_investasi + $total_data_penempatan;
		$data['total_dana'] = formatCurrency(number_format($total_dana, 2));
		/*--------------------------CALCULATE INVESTASI--------------------------*/
		
		$data['user_active'] = $this->db->from('ci_users')->where('last_activity > DATE_SUB(NOW(),INTERVAL 20 MINUTE)')->get()->result();

		$data['title'] = 'Dashboard Admin';
		$data['view'] = 'admin/dashboard/dashboard';
		
		$this->load->view('layout', $data);
	}

	public function users()
	{
		$data['view'] = 'admin/dashboard/dashboard_user';
		$this->load->view('layout', $data);
	}

	public function tambah()
	{

		if (isset($_POST['submit'])) {

			$upload_path = './uploads/excel/dashboard';

			if (!is_dir($upload_path)) {
				mkdir($upload_path, 0777, TRUE);
			}
			//$newName = "hrd-".date('Ymd-His');
			$config = array(
				'upload_path' => $upload_path,
				'allowed_types' => "xlsx",
				'overwrite' => FALSE,
			);

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$upload = $this->upload->data();


			if ($upload) { // Jika proses upload sukses			    	

				$excelreader = new Xlsx;
				$loadexcel = $excelreader->load('./uploads/excel/dashboard/' . $upload['file_name']); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				$data['sheet'] = $sheet;
				$data['file_excel'] = $upload['file_name'];


				$data['view'] = 'dashboard/tambah';
				$this->load->view('admin/layout', $data);
			} else {

				echo "gagal";
				$data['view'] = 'dashboard/tambah';
				$this->load->view('admin/layout', $data);
			}
		} else {

			$data['view'] = 'dashboard/tambah';
			$this->load->view('admin/layout', $data);
		}
    }
    
    public function import_dashboard($file_excel)
	{
		$excelreader = new Xlsx;
		$loadexcel = $excelreader->load('./uploads/excel/dashboard/' . $file_excel); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		$data = array(
			
			'periode' =>$sheet[1]['D'],
			'tahun' =>$sheet[1]['E'],

			'penempatan' => $sheet[2]['B'],
			'setoran_awal' =>$sheet[3]['B'],
			'setoran_awal_per' =>$sheet[3]['C'],
			'setoran_lunas' =>$sheet[4]['B'],
			'setoran_lunas_per' =>$sheet[4]['C'],
			'nilai_manfaat' =>$sheet[5]['B'],
			'nilai_manfaat_per' =>$sheet[5]['C'],
			'dau' =>$sheet[6]['B'],
			'dau_per' =>$sheet[6]['C'],

			'investasi' =>$sheet[7]['B'],
			'setoran_awal_inv' =>$sheet[8]['B'],
			'setoran_awal_inv_per' =>$sheet[8]['C'],
			'dau_inv' =>$sheet[9]['B'],
			'dau_inv_per' =>$sheet[9]['C'],
			'total' => $sheet[10]['B'],
		); 
		echo "<pre>";		
		print_r($data);
		echo "</pre>";
		
		// Panggil fungsi insert_dashboard
		$this->dashboard_model->tambah($data);

		redirect("admin/dashboard"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function show_php_info(){
		echo phpinfo();
	}

	public function hapus($tahun = 0){
		$this->db->delete('dashboard', array('tahun' => $tahun));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('admin/dashboard'));
	}

	public function modal_log(){
		$user_id = $this->session->userdata('user_id');
		$data['log'] = $this->db->from('trx_log')->where('user_id', $user_id)->order_by('id_log', 'desc')->get()->result();
		$this->load->view('admin/dashboard/modal-log', $data);
	}

	public function modal_data($type, $year){
		$user_id = $this->session->userdata('user_id');
		$data['data'] = $this->db->from('dashboard')->where('tahun', $year)->get()->row();
		$data['type'] = $type;
		$data['year'] = $year;
		$this->load->view('admin/dashboard/modal-data', $data);
	}

	public function submit_data(){
		$data = $this->input->post('data');
		$year = $this->input->post('year');
		$type = $this->input->post('year');
		$is_exist = $this->db->from('dashboard')->where('tahun', $year)->get()->row();
		
		if ($type) {
			$total_value = 0;
			foreach ($data as $key => $value) {
				$total_value += $this->replaceCurrency($value);
			}

			foreach ($data as $key => $value) {
				$setField = $key . '_per';
				$data[$setField] = $this->replaceCurrency($value) * 100 / $total_value;
			}
		}
		
		if ($is_exist) {
			$exec = $this->db->where('tahun', $year)->update('dashboard', $data);
		}
		else {
			$data['tahun'] = $year;
			$data['periode'] = '31 Desember';
			$exec = $this->db->insert('dashboard', $data);
		}
		
		if ($exec) {
			$msg = 'success|Data berhasil di simpan';
		}
		else {
			$msg = 'error|Data gagal di simpan';
		}

		$this->session->set_flashdata('message', $msg);

		if ($year == date('Y')) {
			redirect('admin/dashboard');
		}
		else {
			redirect('admin/dashboard/index/' . $year);
		}
	}

	function replaceCurrency($data){
		$data = strtoupper($data);
		$nominal = $data;
		if (strpos($data, 'M')) {
			$convert = str_replace('M', '000000000', $data);
			$comma = str_replace(',', '', $convert);
			$dot = str_replace(',', '', $comma);
			$nominal = str_replace(' ', '', $dot);
		}
		else if (strpos($data, 'T')) {
			$convert = str_replace('T', '000000000000', $data);
			$comma = str_replace(',', '', $convert);
			$dot = str_replace(',', '', $comma);
			$nominal = str_replace(' ', '', $dot);
		}

		return $nominal;
	}
}
