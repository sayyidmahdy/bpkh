<section class="content-header">
	<h1><i class="fa fa-user"></i> &nbsp; User Mangement </h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-lg-10">&nbsp;</div>
		<div class="col-lg-2">

			<button type="button" class="btn btn-primary btn-block" onclick="form_user()">Create User</button>
		</div>
	</div>
	<div class="box mt-4">
		<div class="box-body table-responsive">
			<table  class="table table-bordered table-striped datatabless">
				<thead>
					<tr>
						<th>Username</th>
						<th>Full Name</th>
						<th>Gender</th>
						<th>Email</th>
						<th>Phone Number</th>
						<th>Role</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($data as $row): ?>
						<tr>
							<td><?php echo $row->username ?></td>
							<td><?php echo $row->firstname ?></td>
							<td><?php echo $row->jenis_kelamin ?></td>
							<td><?php echo $row->email ?></td>
							<td><?php echo $row->mobile_no ?></td>
							<td><?php echo ($row->role == 1) ? 'Admin' : 'Visitor' ?></td>
							<td class="text-center">
								<a href="<?php echo base_url('user_management/form_menu/' . $row->id) ?>" class="btn btn-warning btn-xs px-4" ><i class="fa fa-bars"></i> Setting Menu</a>
								<button type="button" class="btn btn-primary btn-xs px-4" onclick="form_user('<?php echo $row->id ?>')"><i class="fa fa-edit"></i> Edit</button>
								<button type="button" class="btn btn-danger btn-xs px-4" onclick="deletes('<?php echo $row->id ?>')"><i class="fa fa-trash"></i> Delete</button>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<!-- <button id="buttons" type="submit" class="btn btn-primary" >submit</button> -->
		</div>
	</div>
</section>

<script>
	$(document).ready(function (){
	   	var table = $('.datatabless').DataTable({
	      pageLength: 10,
	   	});


		$('#buttons').on('click', function(e){
		   e.preventDefault();

		   var data = table.$('input[type="checkbox"]').serializeArray();

		   $.ajax({
		      url: base_url + 'user_management/test',
		      data: data
		   }).done(function(response){
		      console.log('Response', response);
		   });
		});
	});

	function form_user(id = ''){
		if (id.length == 0) {
			var url = base_url + '/user_management/form_user';
		}
		else {
			var url = base_url + '/user_management/form_user/' + id;
		}

		var footer = '<div class="row">\
						<div class="col-lg-9">&nbsp;</div>\
						<div class="col-lg-3"><button type="button" class="btn btn-primary btn-block px-4" onclick="submit()">Submit</button></div>\
					</div>';
		Modal('form_user', 'Form User', url, footer, '', 'auto');
	}

	function deletes(id){
		var url = base_url + '/user_management/delete/' + id;

		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.isConfirmed) {
				$.get(url, function(res){
					var data = JSON.parse(res);
					
					if (data.res == 'success'){
						window.location.reload();
					}
					else {
						Swal.fire('', data.msg, 'error');
					}
				});
			}
		})
	}

	function form_menu(id){
		var url = base_url + '/user_management/form_menu/' + id;

		var footer = '<div class="row">\
						<div class="col-lg-9">&nbsp;</div>\
						<div class="col-lg-3"><button type="button" class="btn btn-primary btn-block px-4" onclick="submit()">Submit</button></div>\
					</div>';

		Modal('form_menu', 'Setting Access Menu', url, footer, 'modal-lg', 'auto');
	}
</script>