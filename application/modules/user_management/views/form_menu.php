<section class="content-header">
	<h1><i class="fa fa-user"></i> &nbsp; Setting Access Menu </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">&nbsp;</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
			<button type="button" class="btn btn-danger btn-block" onclick="clear_all_menu()">Clear All Menu</button>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
			<button type="button" class="btn btn-warning btn-block" onclick="select_all_menu()">Select All Menu</button>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
			<button type="button" class="btn btn-primary btn-block" onclick="submit_menu()">Submit Data</button>
		</div>
	</div>
	<form action="<?php echo base_url('user_management/save_menu/' . $id) ?>" method="POST" class="row mt-4">
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
		<button type="submit" id="btn-submit" style="display: none;"></button>
		<?php foreach ($menu_hdr as $row_hdr): ?>
			<div class="col-lg-4">
				<div class="box">
					<div class="box-header pb-0 mb-0">
						<h5><b><?php echo $row_hdr->title ?></b></h5>
					</div>
					<div class="box-body py-0">
						<div class="form-group container">
							<?php if ($row_hdr->non_hdr == 'Y'): ?>
								<?php foreach ($menu_main as $row_main): ?>
									<?php if ($row_main->hdr_id == $row_hdr->id_hdr): ?>
										<?php foreach ($menu_sub as $row_sub): ?>
											<?php if ($row_sub->menu_id == $row_main->id_menu): ?>
												
													<div class="checkbox">
														<label for="<?php echo $row_sub->id_sub ?>">
															<input id="<?php echo $row_sub->id_sub ?>" type="checkbox" name="data[]" value="<?php echo $row_sub->id_sub ?>" <?php echo (in_array($row_sub->id_sub, $menu_user)) ? 'checked' : '' ?>>
															<?php echo $row_sub->sub_title ?>
														</label>
													</div>
												
											<?php endif ?>
										<?php endforeach ?>
									<?php endif ?>
								<?php endforeach ?>
								
							<?php else: ?>

								<?php foreach ($menu_main as $row_main): ?>
									<?php if ($row_main->hdr_id == $row_hdr->id_hdr): ?>

										<?php if ($row_main->is_sub == 'N'): ?>
											<?php foreach ($menu_sub as $row_sub): ?>
												<?php if ($row_sub->menu_id == $row_main->id_menu): ?>
													
														<div class="checkbox">
															<label for="<?php echo $row_sub->id_sub ?>">
																<input id="<?php echo $row_sub->id_sub ?>" type="checkbox" name="data[]" value="<?php echo $row_sub->id_sub ?>" <?php echo (in_array($row_sub->id_sub, $menu_user)) ? 'checked' : '' ?>>
																<?php echo $row_sub->sub_title ?>
															</label>
														</div>
													
												<?php endif ?>
											<?php endforeach ?>

										<?php else: ?>

											<li>
												<?php echo $row_main->title ?>
												<ul >
													
													<?php foreach ($menu_sub as $row_sub): ?>
														<?php if ($row_sub->menu_id == $row_main->id_menu): ?>
																<div class="checkbox">
																	<label for="<?php echo $row_sub->id_sub ?>">
																		<input id="<?php echo $row_sub->id_sub ?>" type="checkbox" name="data[]" value="<?php echo $row_sub->id_sub ?>" <?php echo (in_array($row_sub->id_sub, $menu_user)) ? 'checked' : '' ?>>
																		<?php echo $row_sub->sub_title ?>
																	</label>
																</div>
														<?php endif ?>
													<?php endforeach ?>
													
												</ul>
											</li>

										<?php endif ?>

									<?php endif ?>
								<?php endforeach ?>

							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</form>
</section>
<div class="container">
</div>

<script>
	function submit_menu(){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, save it!'
		}).then((result) => {
		  if (result.isConfirmed) {
		    $('#btn-submit').click();
		  }
		})
	}

	function select_all_menu(){
		$('input:checkbox').prop('checked',true);
	}

	function clear_all_menu(){
		$('input:checkbox').prop('checked', false);
	}
</script>