<?php if ($id): ?>
	<form role="form" action="<?php echo base_url() ?>user_management/submit/<?php echo $id ?>" method="POST">
<?php else: ?>
	<form role="form" action="<?php echo base_url() ?>user_management/submit" method="POST">
<?php endif ?>
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
	<div class="box-body">
		<div class="form-group">
			<label >Username</label>
			<input type="text" class="form-control" name="data[username]" value="<?php echo ($id) ? $data->username : '' ?>" required>
		</div>
		<div class="form-group">
			<label >Fullname</label>
			<input type="text" class="form-control" name="data[firstname]" value="<?php echo ($id) ? $data->firstname : '' ?>" required>
		</div>
		<?php if (!$id): ?>
			<div class="form-group">
				<label >Password</label>
				<input type="password" class="form-control" name="password" value="<?php echo ($id) ? $data->password : '' ?>" required>
			</div>
		<?php endif ?>
		<div class="form-group">
			<label >Gender</label>
			<select class="form-control" name="data[jenis_kelamin]" value="<?php echo ($id) ? $data->jenis_kelamin : '' ?>" required>
				<option value="Laki-laki">Laki-laki</option>
				<option value="Perempuan">Perempuan</option>
			</select>
		</div>
		<div class="form-group">
			<label >Email</label>
			<input type="email" class="form-control" name="data[email]" value="<?php echo ($id) ? $data->email : '' ?>" required>
		</div>
		<div class="form-group">
			<label >Phone Number</label>
			<input type="text" class="form-control" name="data[mobile_no]" value="<?php echo ($id) ? $data->mobile_no : '' ?>" required>
		</div>
		<div class="form-group">
			<label >Role</label>
			<select class="form-control" name="data[role]" required>
				<option value="1">Admin</option>
				<option value="2">Visitor</option>
			</select>
		</div>
	</div>
	<button type="submit" style="display: none;" id="btn-submit">submit</button>
</form>

<script>
	function submit(){
		$('#btn-submit').click();
	}
</script>