<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class User_management extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('excel');
	}

	public function index(){
		$data['data'] = $this->db->select('*')->from('ci_users')->order_by('username', 'asc')->get()->result();
		$data['view'] = 'index';
		$this->load->view('admin/layout', $data);
	}

	public function form_user($id = NULL){
		$data['id'] = $id;
		if ($id) {
			$data['data'] = $this->db->from('ci_users')->where('id', $id)->get()->row();
		}

		$this->load->view('form_user', $data);
	}

	public function submit($id = NULL){
		$data = $this->input->post('data');
		
		if ($id) {
			
			$exec = $this->db->where('id', $id)->update('ci_users', $data);
			if ($exec) {
				$msg = 'success|Data user berhasil di ubah';
			}
			else {
				$msg = 'error|Data user gagal di ubah';
			}
		}
		else {
			$data['password'] = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
			$data['is_verify'] = 1;
			$data['is_active'] = 1;

			$exec = $this->db->insert('ci_users', $data);

			if ($exec) {
				$msg = 'success|Data user berhasil di simpan';
			}
			else {
				$msg = 'error|Data user gagal di simpan';
			}
		}

		$this->session->set_flashdata('message', $msg);
		redirect('user_management');
	}

	public function delete($id){
		$exec = $this->db->where('id', $id)->delete('ci_users');

		if ($exec) {
			$data['msg'] = 'success|Data user berhasil di hapus';
			$data['res'] = 'success';
			$this->session->set_flashdata('message', $data['msg']);
		}
		else {
			$data['msg'] = 'Data user gagal di hapus';
			$data['res'] = 'fail';
		}

		echo json_encode($data);
	}

	public function form_menu($id){
		$data['id'] = $id;
		$user_id = $this->session->userdata('user_id');
		
		$data['menu_hdr'] = $this->db->select('a.*, COUNT(c.id_sub) AS total_sub')
								->from('menu_hdr a')
								->join('menu_main b', 'a.id_hdr = b.hdr_id')
								->join('menu_sub c', 'b.id_menu = c.menu_id')
								->group_by('title')
								->order_by('total_sub', 'asc')
								->get()
								->result();

		$data['menu_main'] = $this->db->select('b.*')
								->from('menu_hdr a')
								->join('menu_main b', 'a.id_hdr = b.hdr_id')
								->join('menu_sub c', 'b.id_menu = c.menu_id')
								->group_by('title')
								->order_by('id_menu', 'asc')
								->get()
								->result();

		$data['menu_sub'] = $this->db->select('c.*')
								->from('menu_hdr a')
								->join('menu_main b', 'a.id_hdr = b.hdr_id')
								->join('menu_sub c', 'b.id_menu = c.menu_id')
								->group_by('sub_title')
								->order_by('id_sub', 'asc')
								->get()
								->result();

		$menu_user = $this->db->select('c.id_sub')
								->from('menu_hdr a')
								->join('menu_main b', 'a.id_hdr = b.hdr_id')
								->join('menu_sub c', 'b.id_menu = c.menu_id')
								->join('menu_user d', 'c.id_sub = d.sub_menu_id')
								->where('d.user_id', $id)
								->group_by('sub_title')
								->order_by('id_sub', 'asc')
								->get()
								->result();

		$datas = [];
		foreach ($menu_user as $row) {
			$datas[] = $row->id_sub;
		}

		$data['menu_user'] = $datas;
		
		$data['view'] = 'form_menu';
		$this->load->view('admin/layout', $data);
	}

	public function save_menu($id){
		$data = $this->input->post('data');

		$delete_current_menu = $this->db->from('menu_user')->where('user_id', $id)->delete();

		$total_exec = 0;
		foreach ($data as $key => $value) {
			$arrData = [
				'user_id' => $id,
				'sub_menu_id' => $value,
			];

			$exec = $this->db->insert('menu_user', $arrData);
			$total_exec++;
		}
		
		$this->session->set_flashdata('message', 'success|Data menu berhasil di simpan');
		redirect('user_management');
	} 

	function test(){
		$data = $this->input->get();
		dd($data);
	}
}