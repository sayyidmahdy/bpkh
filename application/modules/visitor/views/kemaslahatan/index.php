<section class="content-header">
  <h1><i class="fa fa-file-pdf"></i> &nbsp; Laporan Realisasi Program Kemaslahatan
  </h1>
    
</section>

<section class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <?php if (is_admin() == 1): ?>
        <form class="box-body" id="form-search" action="<?php echo base_url('kemaslahatan/index') ?>" method="GET">
        <?php else: ?>
        <form class="box-body" id="form-search" action="<?php echo base_url('visitor/kemaslahatan/index') ?>" method="GET">
        <?php endif ?>
          <div class="<?php echo search_class() ?>">
            <div class="col-lg-3">
              <div class="form-group">
                <?php dropdown_year() ?>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
              <div class="form-group">
                <button class="btn btn-primary btn-block" onclick="search_data()" type="button">
                  <i class="fas fa-search"></i>
                  Seacrh
                </button>
              </div>
            </div>
          </div>
          <div class="<?php echo generate_class() ?>">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="form-group <?php echo form_group_class() ?>">
                <a href="javascript:void(0)" class="btn btn-primary btn-block" onclick="generate_data()"><i class="fas fa-file-excel"></i>&nbsp; Export Data ke Excel</a>
              </div>
            </div>
          </div>

          <?php if (is_admin() == 1): ?>
          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="form-group">
                  <a href="<?=base_url('kemaslahatan/tambah_kemaslahatan'); ?>" class="btn btn-warning btn-block"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a>
              </div>
            </div>
          </div>
          <?php endif ?>
        </form>
      </div>
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Bulan</th>
                <th>Nama Penerima</th>
                <th>Jenis</th>
                <th>Lokasi</th>
                <th>Kegiatan</th>
                <th>Asnaf</th>
                <th>Nilai</th>
                <th style="width:10%;text-align: center;">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($kemaslahatan as $row) { ?>
                <tr>
                  <td><?=konversiBulanAngkaKeNama($row['bulan']); ?></td>
                  <td><?=$row['nama_penerima']; ?></td>
                  <td><?=$row['jenis']; ?></td>
                  <td><?=$row['lokasi']; ?></td>
                  <td><?=$row['kegiatan']; ?></td>
                  <td><?=$row['asnaf']; ?></td>
                  <td class="text-right"><?=$row['nilai']; ?></td>
                  <td class="text-center;">     
                    <?php if (is_admin() == 1): ?>           
                    <a style="color:#fff;" title="Tambah Dokumentasi" class="btn btn-xs btn-info" href="<?=base_url('kemaslahatan/tambah_dokumentasi/'.$row['id']); ?>"> <i class="fas fa-plus"></i></a>
                    <a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger" data-href="<?=base_url('kemaslahatan/hapus_kemaslahatan/'.$row['id']).'/'.$thn; ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    <?php endif ?>
                    <a style="color:#fff;" title="Lihat Dokumentasi" class="btn btn-xs btn-info" href="<?=base_url('visitor/kemaslahatan/dokumentasi/'.$row['id']); ?>"> <i class="fas fa-image"></i></a>

                   
                  </td>
                 
              <?php  } ?>
            </tbody>           
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
  
  <!-- /.box -->
</section>  

<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus</h4>
      </div>
      <div class="modal-body">
        <p>Anda yakin ingin menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <a class="btn btn-danger btn-ok">Hapus</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>


<script type="text/javascript">
  $(function () {
    $("#table").DataTable();
  });
  

  $('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
  });

  $("#kemaslahatan").addClass('active');

  function search_data(){
      var tahun = $('#tahun option:selected').val();

      if (tahun == 0) {
        Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
        return false;
      }

      $('#form-search').submit();
    }

  $(function(){
    $('#tahun').val('<?php echo $selected_year ?>');
    $('#tahun').trigger('change');
  });

  function generate_data(){
    var tahun = $('#tahun option:selected').val();

    if (tahun == 0) {
        Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
        return false;
      }

    var url = '<?=base_url('visitor/kemaslahatan/export_kemaslahatan/'); ?>' + tahun;
    window.location.href = url;
  }
</script>