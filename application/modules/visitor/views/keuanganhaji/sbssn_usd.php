<section class="content-header">
	<h1><i class="fa fa-kaaba"></i> Laporan SBSSN USD </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<form class="box-body" id="form-search" action="<?php echo base_url('visitor/keuanganhaji/sbssn_usd') ?>" method="GET">
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="form-group">
								<?php dropdown_year() ?>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<select class="form-control select2" name="sukuk" id="sukuk">
									<option value="0">-- Silahkan Pilih Sukuk --</option>
									<option value="sbsn_rupiah"> Sukuk SBSN Rupiah </option>
									<option value="sbsn_usd"> Sukuk SBSN USD </option>
									<option value="sdhi_rupiah"> Sukuk SDHI Rupiah </option>
									<option value="korporasi"> Sukuk Korporasi </option>
								</select>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="form-group">
								<button class="btn btn-primary btn-block" onclick="search_dana_produk()" type="button">
								<i class="fas fa-search"></i>
								Seacrh
								</button>
							</div>
						</div>
					</div>
					<?php if (is_admin() == 1): ?>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right">
						<div class="col-lg-2">&nbsp;</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							<div class="form-group">
									<a href="<?=base_url('keuanganhaji/tambah_sbssn_usd'); ?>" class="btn btn-warning btn-block"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a>
							</div>
						</div>
					</div>
					<?php endif ?>
				</form>
			</div>
			<?php if ($sbssn_usd) { ?>

			<div class="box">

				<div class="box-body smy-form-body">
				<table id="table1" class="table table-striped table-bordered datatables">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th >Bulan</th>
							<th width="40%" class="text-center">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($sbssn_usd as $row) { ?>
						<tr>
							<td><?php echo $no++ ?></td>
							<td>Laporan SBSSN USD Bulan <?=konversiBulanAngkaKeNama($row['bulan']); ?></td>
							<td class="text-center">

								<a style="color:#fff;" title="Lihat Detail" class="btn btn-xs btn-info px-4"
									href="<?= base_url(url_validation() . $this->router->fetch_class() . '/detail_sbssn_usd/' . $row['bulan'] .'/' .$row['tahun']); ?>">
									<i class="fa fa-eye"></i> &nbsp; Detail
								</a>
								<?php if (is_admin() == 1): ?>
								<a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger px-4"
									data-href="<?= base_url($this->router->fetch_class() . '/hapus_sbssn_usd/' . $row['bulan'].'/' .$row['tahun']); ?>"
									data-toggle="modal" data-target="#confirm-delete">
									<i class="fa fa-trash-alt"></i> &nbsp; Hapus
								</a>
								<?php endif ?>
								<a href="<?= base_url('visitor/keuanganhaji/export_sbssn_usd/'. $row['bulan'] .'/' . $thn); ?>" class="btn btn-success btn-xs px-4"><i class="fas fa-file-excel">
									</i>&nbsp; Export Data ke Excel
								</a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>


				</div>
			</div>

			
			<?php } else {
				echo '<p class="alert alert-success"> Pilih tahun</p>';
			} ?>
		</div>
	</div>
</section>

<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				<p>Anda yakin ingin menghapus data ini?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function (e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});

</script>

<script>
	$("#alokasi_produk_investasi").addClass('active');
	$("#alokasi_produk_investasi .sukuk").addClass('active');

	function search_dana_produk(){
      var tahun = $('#tahun option:selected').val();
      var sukuk = $('#sukuk option:selected').val();
      var role = <?php echo is_admin() ?>;

      if (tahun == 0) {
        Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
        return false;
      }

      if (sukuk == 0) {
        Swal.fire('', 'Silahkah Pilih Jenis Sukuk Terlebih Dahulu', 'info');
        return false;
      }

      if (sukuk == 'korporasi') {
        var url = (role == 1) ? '<?php echo base_url('keuanganhaji/sukuk_korporasi') ?>' : '<?php echo base_url('visitor/keuanganhaji/sukuk_korporasi') ?>';
        $('#form-search').attr('action', url).submit();
      }
      else if (sukuk == 'sbsn_usd'){
        var url = (role == 1) ? '<?php echo base_url('keuanganhaji/sbssn_usd') ?>' : '<?php echo base_url('visitor/keuanganhaji/sbssn_usd') ?>';
        $('#form-search').attr('action', url).submit();
      }
      else if (sukuk == 'sdhi_rupiah'){
        var url = (role == 1) ? '<?php echo base_url('keuanganhaji/sdhi_rupiah') ?>' : '<?php echo base_url('visitor/keuanganhaji/sdhi_rupiah') ?>';
        $('#form-search').attr('action', url).submit();
      }
      else {
      	var url = (role == 1) ? '<?php echo base_url('keuanganhaji/sbssn_rupiah') ?>' : '<?php echo base_url('visitor/keuanganhaji/sbssn_rupiah') ?>';
        $('#form-search').attr('action', url).submit();	
      }
    }

    $(function(){
      $('#tahun').val('<?php echo $selected_year ?>');
      $('#tahun').trigger('change');

      $('#sukuk').val('sbsn_usd');
      $('#sukuk').trigger('change');
    });

</script>
