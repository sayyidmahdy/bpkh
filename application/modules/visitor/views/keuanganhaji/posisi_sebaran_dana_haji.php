<section class="content-header">
  <h1><i class="fa fa-kaaba"></i> &nbsp; Posisi Sebaran Dana Haji (Penempatan) pada BPS BPIH
  
  </h1>
</section>

<section class="content mt-4">
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <?php if (is_admin() == 1): ?>
        <form class="box-body" id="form-search" action="<?php echo base_url('keuanganhaji/sebaran_dana_haji') ?>" method="GET">
        <?php else: ?>
        <form class="box-body" id="form-search" action="<?php echo base_url('visitor/keuanganhaji/sebaran_dana_haji') ?>" method="GET">
        <?php endif ?>
          <div class="<?php echo search_class() ?>">
            <div class="<?php echo year_class() ?>">
              <div class="form-group">
                <?php dropdown_year() ?>
              </div>
            </div>
            <div class="col-lg-5">
              <div class="form-group">
                <select class="form-control select2" name="bank" id="bank">
                  <option value="all">Semua Bank</option>
                  <?php foreach ($dropdown_bank as $rowBank): ?>
                    <?php if ($rowBank['bps_bpih'] != "TOTAL"): ?>
                      
                      <option value="<?php echo $rowBank['bps_bpih'] ?>"><?php echo $rowBank['bps_bpih'] ?></option>

                    <?php endif ?>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
              <div class="form-group">
                <button class="btn btn-primary btn-block" onclick="search_dana_haji()" type="button">
                    <i class="fas fa-search"></i>
                    Seacrh
                </button>
              </div>
            </div>
          </div>

          <div class="<?php echo generate_class() ?>">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="form-group <?php echo form_group_class() ?>">
                <a href="javascript:void(0)" onclick="generate_dana_produk()" class="btn btn-primary btn-block"><i class="fas fa-file-excel"></i>&nbsp;  Export Data ke Excel</a>
              </div>
            </div>
          </div>

          <?php if (is_admin() == 1): ?>
          <div class="<?php echo create_class() ?>">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="form-group">
                  <a href="<?=base_url('keuanganhaji/tambah_sebaran_dana_haji'); ?>" class="btn btn-warning btn-block"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a> 
              </div>
            </div>
          </div>
          <?php endif ?>
        </form>
      </div>
      
      <div class="box">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12 text-right">
              <label><b><i>(dalam rupiah)</i></b></label>
            </div>
          </div>
          <div style="overflow-x: scroll; min-height:500px;">
            <?php if($sebaran_dana_haji) { ?>
            <table class="keuangan table table-striped table-bordered" style="width:2200px;min-width: 100%;">
              <thead>
                <tr class="text-center">
                  <th>BPS BPIH </th>
                  <th>Januari</th>
                  <th>Februari</th>
                  <th>Maret</th>
                  <th>April</th>
                  <th>Mei</th>
                  <th>Juni</th>
                  <th>Juli</th>
                  <th>Agustus</th>
                  <th>September</th>
                  <th>Oktober</th>
                  <th>November</th>
                  <th>Desember</th>
                </tr>
              </thead>
              <?php foreach ($sebaran_dana_haji as $row) {
              if($row['bps_bpih'] == "TOTAL") { ?>
              <tfoot>
                <tr class="total">
                  <td width="20%"><?=$row['bps_bpih']; ?></td>
                  <td style="text-align: right;"><?=$row['januari']; ?></td>
                  <td style="text-align: right;"><?=$row['februari']; ?></td>
                  <td style="text-align: right;"><?=$row['maret']; ?></td>
                  <td style="text-align: right;"><?=$row['april']; ?></td>
                  <td style="text-align: right;"><?=$row['mei']; ?></td>
                  <td style="text-align: right;"><?=$row['juni']; ?></td>
                  <td style="text-align: right;"><?=$row['juli']; ?></td>
                  <td style="text-align: right;"><?=$row['agustus']; ?></td>
                  <td style="text-align: right;"><?=$row['september']; ?></td>
                  <td style="text-align: right;"><?=$row['oktober']; ?></td>
                  <td style="text-align: right;"><?=$row['november']; ?></td>
                  <td style="text-align: right;"><?=$row['desember']; ?></td>
                </tr>
                <?php if (is_admin() == 1): ?>
                  <tr>
                    <td>HAPUS</td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/januari/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/februari/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/maret/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/april/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/mei/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/juni/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/juli/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/agustus/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/september/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/oktober/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/november/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_sebaran_dana_haji/desember/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                  </tr>
                <?php endif ?>
                
              </tfoot>
              <?php } else { ?>
              <tr>
                <td><?=$row['bps_bpih']; ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['januari']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['februari']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['maret']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['april']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['mei']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['juni']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['juli']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['agustus']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['september']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['oktober']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['november']); ?></td>
                <td style="text-align: right;"><?=formatCurrency_max_milyar($row['desember']); ?></td>
              </tr>
              <?php } // endif
              } //endforeach get bps bpih ?>
            </table>
            <?php } else { echo '<p class="alert alert-success"> Pilih tahun</p>';} ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus</h4>
      </div>
      <div class="modal-body">
        <p>Anda yakin ingin menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <a class="btn btn-danger btn-ok">Hapus</a>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  $('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
  });
</script>
<script>
  $("#alokasi_produk_perbankan").addClass('active');
  $("#alokasi_produk_perbankan .sebaran_dana_haji").addClass('active');

  function search_dana_haji(){
    var tahun = $('#tahun option:selected').val();
    var bank = $('#bank option:selected').val();

    if (tahun == 0) {
      Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
      return false;
    }

    $('#form-search').submit();
  }

  $(function(){
    $('#tahun').val('<?php echo $selected_year ?>');
    $('#tahun').trigger('change');

    $('#bank').val('<?php echo $selected_bank ?>');
    $('#bank').trigger('change');
  });

  function generate_dana_produk(){
    var tahun = $('#tahun option:selected').val();
    var bank = $('#bank option:selected').val();

    if (tahun == 0) {
        Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
        return false;
      }
      
    var url = '<?=base_url('visitor/keuanganhaji/export_sebaran_dana_haji/'); ?>' + tahun + '/' + bank;

    window.location.href = url;
  }
</script>