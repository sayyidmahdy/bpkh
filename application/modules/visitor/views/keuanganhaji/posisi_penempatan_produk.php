<section class="content-header">
	<h1><i class="fa fa-kaaba"></i> &nbsp; Dana Haji yang Ditempatkan</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<?php if (is_admin() == 1): ?>
				<form class="box-body" id="form-search" action="<?php echo base_url('keuanganhaji/posisi_penempatan_produk') ?>" method="GET">
					<?php else: ?>
					<form class="box-body" id="form-search" action="<?php echo base_url('visitor/keuanganhaji/posisi_penempatan_produk') ?>" method="GET">
						<?php endif ?>
						<div class="<?php echo search_class() ?>">
							<div class="<?php echo year_class() ?>">
								<div class="form-group">
									<?php dropdown_year() ?>
								</div>
							</div>
							
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
								<div class="form-group">
									<button class="btn btn-primary btn-block" onclick="search_dana_produk()" type="button">
									<i class="fas fa-search"></i>
									Seacrh
									</button>
								</div>
							</div>
						</div>
						<div class="<?php echo generate_class() ?>">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group <?php echo form_group_class() ?>">
									<a href="javascript:void(0)" class="btn btn-primary btn-block" onclick="generate_dana_produk()"><i class="fas fa-file-excel"></i>&nbsp; Export Data ke Excel</a>
								</div>
							</div>
						</div>
						<?php if (is_admin() == 1): ?>
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right p-0">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<a href="<?=base_url('keuanganhaji/tambah_posisi_penempatan_produk'); ?>" class="btn btn-warning btn-block"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a>
								</div>
							</div>
						</div>
						<?php endif ?>
					</form>
				</div>
				<div class="box">
					<div class="box-body">
						<div style="min-height:500px;">
							<?php if($posisi_penempatan_produk) { ?>
							<div class="box-keuangan">
								<div class="row">
						            <div class="col-md-12 text-right">
						              <label><b><i>(dalam rupiah)</i></b></label>
						            </div>
						          </div>
								<table class="keuangan table table-striped table-bordered">
									<thead>
										<tr class="text-center">
											<th style="width:200px !important;">Bulan</th>
											<th>Giro</th>
											<th>Tabungan</th>
											<th>Deposito</th>
											<th>Jumlah</th>
											<?php if (is_admin() == 1): ?>
											<th style="width: 5%"> &nbsp;</th>
											<?php endif ?>
										</tr>
									</thead>
									<?php foreach ($posisi_penempatan_produk as $row) { ?>
									<tr>
										<td><?=konversiBulanAngkaKeNama($row['bulan']); ?></td>
										<td style="text-align: right;"><?=$row['giro']; ?></td>
										<td style="text-align: right;"><?=$row['tabungan']; ?></td>
										<td style="text-align: right;"><?=$row['deposito']; ?></td>
										<td style="text-align: right;"><?=$row['jumlah']; ?></td>
										<?php if (is_admin() == 1): ?>
										<td  width="5%" style="text-align: center;">
											<a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger" data-href="<?=base_url('keuanganhaji/hapus_posisi_penempatan_produk/'.$row['id']); ?>" data-toggle="modal" data-target="#confirm-delete">
												<i class="fa fa-trash-alt"></i>
											</a>
										</td>
										<?php endif ?>
									</tr>
									<?php
									} //endforeach get bps bpih ?>
								</table>
							</div>
							<?php } else { echo '<p class="alert alert-success"> Pilih tahun</p>';} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script>
		$("#alokasi_produk_perbankan").addClass('active');
		$("#alokasi_produk_perbankan .posisi_penempatan_produk").addClass('active');
	</script>
	<!-- Modal -->
	<div id="confirm-delete" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Hapus</h4>
				</div>
				<div class="modal-body">
					<p>Anda yakin ingin menghapus data ini?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					<a class="btn btn-danger btn-ok">Hapus</a>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('#confirm-delete').on('show.bs.modal', function (e) {
			$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
		});
		function search_dana_produk(){
		var tahun = $('#tahun option:selected').val();
		if (tahun == 0) {
		Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
		return false;
		}
		$('#form-search').submit();
		}
		$(function(){
	$('#tahun').val('<?php echo $selected_year ?>');
	$('#tahun').trigger('change');
	});
	function generate_dana_produk(){
	var tahun = $('#tahun option:selected').val();
	if (tahun == 0) {
	Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
	return false;
	}
	var url = '<?=base_url('visitor/keuanganhaji/export_posisi_penempatan_produk/'); ?>' + tahun;
	window.location.href = url;
	}
	function modal_create(){
	var modal_url = base_url + '/keuanganhaji/tambah_posisi_penempatan_produk';
	
	var size = 'modal-md';
	var height = '40vh';
	var title = 'Tambah Dana Haji yang Ditempatkan';
	var button = '<button type="submit" class="btn btn-primary w-md" id="btn_submit_menu" onclick="submit_form_menu()">\
	<i class="icon-check2"></i> Save\
	</button>';
	Modal('modal-form-menu', title, modal_url, button, size, '');
	}
	</script>