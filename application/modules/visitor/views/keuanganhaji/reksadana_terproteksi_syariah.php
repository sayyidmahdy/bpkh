<section class="content-header">
  <h1><i class="fa fa-kaaba"></i> &nbsp; Reksadana Terproteksi Syariah  
  </h1>
  
</section>
<section class="content">
 
  <div class="row">
    <div class="col-md-12">
        <div class="box">
          <?php if (is_admin() == 1): ?>
          <form class="box-body" id="form-search" action="<?php echo base_url('keuanganhaji/reksadana_terproteksi_syariah') ?>" method="GET">
          <?php else: ?>
          <form class="box-body" id="form-search" action="<?php echo base_url('visitor/keuanganhaji/reksadana_terproteksi_syariah') ?>" method="GET">
          <?php endif ?>
            <div class="<?php echo search_class() ?>">
              <div class="<?php echo year_class() ?>">
                <div class="form-group">
                  <?php dropdown_year() ?>
                </div>
              </div>
              <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="form-group">
                  <select class="form-control select2" name="reksadana" id="reksadana">
                    <option value="0">-- Silahkan Pilih Reksadana --</option>
                    <option value="terproteksi"> Terproteksi Syariah </option>
                    <option value="pasar_uang"> Pasar Uang Syarian </option>

                  </select>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                  <button class="btn btn-primary btn-block" onclick="search_dana_produk()" type="button">
                    <i class="fas fa-search"></i>
                    Seacrh
                  </button>
                </div>
              </div>
            </div>
            <div class="<?php echo generate_class() ?>">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group <?php echo form_group_class() ?>">
                  <a href="javascript:void(0)" class="btn btn-primary btn-block" onclick="generate_dana_produk()"><i class="fas fa-file-excel"></i>&nbsp; Export Data ke Excel</a>
                </div>
              </div>
            </div>

            <?php if (is_admin() == 1): ?>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right p-0">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <a href="<?=base_url('keuanganhaji/tambah_reksadana_terproteksi_syariah'); ?>" class="btn btn-warning btn-block"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a> 
                </div>
              </div>
            </div>
            <?php endif ?>
          </form>
        </div>
        <div class="box">
          <div class="box-body">
            <div style="overflow-x: scroll; min-height:500px;">
              <?php if($reksadana_terproteksi_syariah) { ?>
              <p class="text-success">*) Dalam Miliar Rupiah</p>
              <table class="keuangan table table-striped table-bordered" style="width:2200px;min-width: 100%;">
                <thead>
                    <tr>
                      <th>Instrumen RDTS</th>
                      <th>Januari</th>
                      <th>Februari</th>
                      <th>Maret</th>
                      <th>April</th>
                      <th>Mei</th>
                      <th>Juni</th>
                      <th>Juli</th>
                      <th>Agustus</th>
                      <th>September</th>
                      <th>Oktober</th>
                      <th>November</th>
                      <th>Desember</th> 
                    </tr>
                  </thead>
                  <?php foreach ($reksadana_terproteksi_syariah as $row) { 

                  if($row['instrumen'] == "TOTAL") { ?>
                    <!-- <tfoot> -->
                      <tr class="total">
                        <td><?=$row['instrumen']; ?></td>
                        <td style="text-align: right;"><?=$row['januari']; ?></td>
                        <td style="text-align: right;"><?=$row['februari']; ?></td>
                        <td style="text-align: right;"><?=$row['maret']; ?></td>
                        <td style="text-align: right;"><?=$row['april']; ?></td>
                        <td style="text-align: right;"><?=$row['mei']; ?></td>
                        <td style="text-align: right;"><?=$row['juni']; ?></td>
                        <td style="text-align: right;"><?=$row['juli']; ?></td>
                        <td style="text-align: right;"><?=$row['agustus']; ?></td>
                        <td style="text-align: right;"><?=$row['september']; ?></td>
                        <td style="text-align: right;"><?=$row['oktober']; ?></td>
                        <td style="text-align: right;"><?=$row['november']; ?></td>
                        <td style="text-align: right;"><?=$row['desember']; ?></td>
                      </tr>
                    <!-- </tfoot> -->
                  <?php } else { ?>               
                    <tr>
                      <td><?=$row['instrumen']; ?></td>
                      <td style="text-align: right;"><?=$row['januari']; ?></td>
                      <td style="text-align: right;"><?=$row['februari']; ?></td>
                      <td style="text-align: right;"><?=$row['maret']; ?></td>
                      <td style="text-align: right;"><?=$row['april']; ?></td>
                      <td style="text-align: right;"><?=$row['mei']; ?></td>
                      <td style="text-align: right;"><?=$row['juni']; ?></td>
                      <td style="text-align: right;"><?=$row['juli']; ?></td>
                      <td style="text-align: right;"><?=$row['agustus']; ?></td>
                      <td style="text-align: right;"><?=$row['september']; ?></td>
                      <td style="text-align: right;"><?=$row['oktober']; ?></td>
                      <td style="text-align: right;"><?=$row['november']; ?></td>
                      <td style="text-align: right;"><?=$row['desember']; ?></td>               
                    </tr>
                  <?php } // endif

                  } //endforeach get bps bpih ?>
                  <?php if (is_admin() == 1): ?>
                    <tfoot>
                      <tr>
                        <td>HAPUS</td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/januari/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/februari/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/maret/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/april/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/mei/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/juni/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/juli/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/agustus/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/september/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/oktober/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/november/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                        <td style="text-align: center;">
                          <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('keuanganhaji/hapus_reksadana_terproteksi_syariah/desember/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                        </td>
                      </tr>
                    </tfoot>
                  <?php endif ?>
              </table>
              <?php } else { echo '<p class="alert alert-success"> Pilih tahun</p>';} ?>
            </div>
          </div>
        </div>
        
    </div>
  </div>  
</section> 
<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus</h4>
      </div>
      <div class="modal-body">
        <p>Anda yakin ingin menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <a class="btn btn-danger btn-ok">Hapus</a>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  $('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
  });
</script>
<script>
    $("#alokasi_produk_investasi").addClass('active');
    $("#alokasi_produk_investasi .reksadana").addClass('active');

    function search_dana_produk(){
      var tahun = $('#tahun option:selected').val();
      var reksadana = $('#reksadana option:selected').val();
      var role = <?php echo is_admin() ?>;

      if (tahun == 0) {
        Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
        return false;
      }

      if (reksadana == 0) {
        Swal.fire('', 'Silahkah Pilih Jenis Reksadana Terlebih Dahulu', 'info');
        return false;
      }

      if (reksadana == 'terproteksi') {
        var url = (role == 1) ? '<?php echo base_url('keuanganhaji/reksadana_terproteksi_syariah') ?>' : '<?php echo base_url('visitor/keuanganhaji/reksadana_terproteksi_syariah') ?>';
        $('#form-search').attr('action', url);
      }
      else {
        var url = (role == 1) ? '<?php echo base_url('keuanganhaji/reksadana_pasar_uang_syariah') ?>' : '<?php echo base_url('visitor/keuanganhaji/reksadana_pasar_uang_syariah') ?>';
        $('#form-search').attr('action', url);
      }

      $('#form-search').submit();
    }

    $(function(){
      $('#tahun').val('<?php echo $selected_year ?>');
      $('#tahun').trigger('change');

      $('#reksadana').val('terproteksi');
      $('#reksadana').trigger('change');
    });

    function generate_dana_produk(){
      var tahun = $('#tahun option:selected').val();

      if (tahun == 0) {
        Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
        return false;
      }

      var url = '<?=base_url('visitor/keuanganhaji/export_reksadana_terproteksi_syariah/'); ?>' + tahun;
      window.location.href = url;
    }
</script>





