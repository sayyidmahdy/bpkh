<section class="content-header">
	<h1><i class="fa fa-kaaba"></i> &nbsp; Nilai Manfaat Hasil Penempatan di BPS BPIH
	</h1>

</section>
<section class="content">

	<div class="row">
		<div class="col-md-12">

			<div class="box">
				<?php if (is_admin() == 1): ?>
				<form class="box-body" id="form-search" action="<?php echo base_url('nilaimanfaat/penempatan_di_bpsbpih') ?>" method="GET">
				<?php else: ?>
				<form class="box-body" id="form-search" action="<?php echo base_url('visitor/nilaimanfaat/penempatan_di_bpsbpih') ?>" method="GET">
				<?php endif ?>
					<div class="<?php echo search_class() ?>">
              <div class="<?php echo year_class() ?>">
							<div class="form-group">
								<?php dropdown_year() ?>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<select class="form-control select2" id="produk">
									<option value="0">-- Silahkan Pilih Produk --</option>
									<option value="dana_haji"> Dana Haji yang Ditempatkan </option>
									<option value="data_kelola"> Penempatan Dana Kelolaan di BPS BPIH </option>
								</select>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="form-group">
								<button class="btn btn-primary btn-block" onclick="search_dana_penempatan_di_bpsbpih()" type="button">
								<i class="fas fa-search"></i>
								Seacrh
								</button>
							</div>
						</div>
					</div>
					
					<div class="<?php echo generate_class() ?>">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group <?php echo form_group_class() ?>">
								<a href="javascript:void(0)" class="btn btn-primary btn-block" onclick="generate_dana_penempatan_di_bpsbpih()"><i class="fas fa-file-excel"></i>&nbsp; Export Data ke Excel</a>
							</div>
						</div>
					</div>

					<?php if (is_admin() == 1): ?>
					<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-right">
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
									<a href="<?=base_url('nilaimanfaat/tambah_nilai_manfaat_penempatan_di_bpsbpih'); ?>" class="btn btn-warning btn-block"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a>
							</div>
						</div>
					</div>
					<?php endif ?>
				</form>
			</div>

			<div class="box">
		    <div class="box-body">
		      <?php if($all_data) { ?>
		      <div class="row">
		        <div class="col-md-12 text-right">
		          <label><b><i>(Rp in Miliyar)</i></b></label>
		        </div>
		      </div>
		      <div style="overflow-x: scroll; min-height:500px;">
		        <table class="keuangan table table-striped table-bordered" style="width:2200px;min-width: 100%;">
		          <thead>
		            <tr class="text-center">
		              <th>Bidang</th>
		              <?php if (is_admin() == 1): ?>
		              <th>Hapus</th>
		              <?php endif ?>
		              <th>Januari</th>
		              <th>Februari</th>
		              <th>Maret</th>
		              <th>April</th>
		              <th>Mei</th>
		              <th>Juni</th>
		              <th>Juli</th>
		              <th>Agustus</th>
		              <th>September</th>
		              <th>Oktober</th>
		              <th>November</th>
		              <th>Desember</th>
		            </tr>
		          </thead>
		          <?php foreach ($all_data as $row) {
		          if(strtoupper($row->bidang) == "TOTAL") { ?>
		          <tfoot>
		            <tr class="total">
		              <td width="20%"><?=$row->bidang; ?></td>
		              <?php if (is_admin() == 1): ?>
		              <td  width="5%" style="text-align: center;">
		                <a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger" data-href="<?=base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih?id='.$row->id .'&tahun=' . $selected_year); ?>" data-toggle="modal" data-target="#confirm-delete">
		                  <i class="fa fa-trash-alt"></i>
		                </a>
		              </td>
		            <?php endif ?>
		              <td style="text-align: right;"><?=$row->januari; ?></td>
		              <td style="text-align: right;"><?=$row->februari; ?></td>
		              <td style="text-align: right;"><?=$row->maret; ?></td>
		              <td style="text-align: right;"><?=$row->april; ?></td>
		              <td style="text-align: right;"><?=$row->mei; ?></td>
		              <td style="text-align: right;"><?=$row->juni; ?></td>
		              <td style="text-align: right;"><?=$row->juli; ?></td>
		              <td style="text-align: right;"><?=$row->agustus; ?></td>
		              <td style="text-align: right;"><?=$row->september; ?></td>
		              <td style="text-align: right;"><?=$row->oktober; ?></td>
		              <td style="text-align: right;"><?=$row->november; ?></td>
		              <td style="text-align: right;"><?=$row->desember; ?></td>
		            </tr>
		          </tfoot>
		          <?php } else { ?>
		          <tr>
		            <td><?=$row->bidang; ?></td>
		            <?php if (is_admin() == 1): ?>
		              <td  width="5%" style="text-align: center;">
		                <a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger" data-href="<?=base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih?id='.$row->id .'&tahun=' . $selected_year); ?>" data-toggle="modal" data-target="#confirm-delete">
		                  <i class="fa fa-trash-alt"></i>
		                </a>
		              </td>
		            <?php endif ?>
		            <td style="text-align: right;"><?=$row->januari; ?></td>
		            <td style="text-align: right;"><?=$row->februari; ?></td>
		            <td style="text-align: right;"><?=$row->maret; ?></td>
		            <td style="text-align: right;"><?=$row->april; ?></td>
		            <td style="text-align: right;"><?=$row->mei; ?></td>
		            <td style="text-align: right;"><?=$row->juni; ?></td>
		            <td style="text-align: right;"><?=$row->juli; ?></td>
		            <td style="text-align: right;"><?=$row->agustus; ?></td>
		            <td style="text-align: right;"><?=$row->september; ?></td>
		            <td style="text-align: right;"><?=$row->oktober; ?></td>
		            <td style="text-align: right;"><?=$row->november; ?></td>
		            <td style="text-align: right;"><?=$row->desember; ?></td>
		          </tr>
		          <?php } // endif
		          } //endforeach get bps bpih ?>
		          <?php if (is_admin() == 1): ?>
		            <!-- <tfoot>
		              <tr>
		                <td>HAPUS</td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/januari/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/februari/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/maret/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/april/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/mei/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/juni/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/juli/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/agustus/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/september/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/oktober/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/november/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		                <td style="text-align: center;">
		                  <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('nilaimanfaat/hapus_nilai_manfaat_penempatan_di_bpsbpih/desember/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
		                </td>
		              </tr>
		            </tfoot> -->
		          <?php endif ?>
		        </table>
		      </div>
		      <?php } else { echo 'Data Not Found';} ?>
		    </div>
		  </div>
		</div>
	</div>
</section>


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				<p>Anda yakin ingin menghapus data ini?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<script>
	$("#nilai_manfaat").addClass('active');
	$("#nilai_manfaat .produk").addClass('active');

	function search_dana_penempatan_di_bpsbpih(){
      var tahun = $('#tahun option:selected').val();
      var produk = $('#produk option:selected').val();
      var role = <?php echo is_admin() ?>;

      if (tahun == 0) {
        Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
        return false;
      }

      if (produk == 0) {
        Swal.fire('', 'Silahkah Pilih Jenis produk Terlebih Dahulu', 'info');
        return false;
      }

      if (produk == 'dana_haji') {
        var url = (role == 1) ? '<?php echo base_url('nilaimanfaat/produk') ?>' : '<?php echo base_url('visitor/nilaimanfaat/produk') ?>';
        $('#form-search').attr('action', url).submit();
      }
      else {
      	var url = (role == 1) ? '<?php echo base_url('nilaimanfaat/penempatan_di_bpsbpih') ?>' : '<?php echo base_url('visitor/nilaimanfaat/penempatan_di_bpsbpih') ?>';
        $('#form-search').attr('action', url).submit();	
      }
    }

    $(function(){
      $('#tahun').val('<?php echo $selected_year ?>');
      $('#tahun').trigger('change');

      $('#produk').val('data_kelola');
      $('#produk').trigger('change');
    });

    function generate_dana_penempatan_di_bpsbpih(){
      var tahun = $('#tahun option:selected').val();

      if (tahun == 0) {
          Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
          return false;
        }

      var url = '<?=base_url('visitor/nilaimanfaat/export_nilai_manfaat_penempatan_di_bpsbpih/'); ?>' + tahun;
      window.location.href = url;
    }
</script>