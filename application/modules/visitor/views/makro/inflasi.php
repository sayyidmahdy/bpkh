<section class="content-header">
  <h1><i class="fa fa-file-pdf"></i> &nbsp; Inflasi Indonesia</h1>        
</section>

<style>
  .error{ color:red; } 
</style>

<section class="content" >
  <?php if (is_admin() == 1): ?>
  <div class="row">
    <div class="col-lg-6">&nbsp;</div>
    <div class="col-lg-6 col-md-2 col-sm-12 col-xs-12 text-right">
      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-danger btn-block" onclick="form_inflasi('delete')"><i class="fa fa-trash"></i>&nbsp;Delete Data</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-primary btn-block" onclick="form_inflasi('edit')"><i class="fa fa-edit"></i>&nbsp;Edit Data</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-warning btn-block" onclick="form_inflasi()"><i class="fa fa-plus"></i>&nbsp;Tambah Data</a>
        </div>
      </div>
    </div>
  </div>
  <?php endif ?>
  <div class="box">
    <div class="box-body table-responsive">
      <div id="chartdiv"></div>
    </div>
  </div>
</section>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
  $(function () {
    $("#table").DataTable();
  });
  
  $("#makro").addClass('active');
  $("#makro .inflasi").addClass('active');

  function form_inflasi(id = ''){
    if (id.length == 0) {
      var url = base_url + 'makro/form_inflasi/';
      var label = 'Tambah';
    }
    else {
      var url = base_url + 'makro/form_inflasi/' + id;
      var label = capitalizeFirstLetter(id);
    }

    var header = 'Form Edit Inflasi';
    var footer = '<div class="row">\
            <div class="col-lg-9">&nbsp;</div>\
            <div class="col-lg-3"><button type="button" class="btn btn-primary btn-block px-4" onclick="submit()">'+label+'</button></div>\
          </div>';
    Modal('form_user', header, url, footer, '', 'auto');
  }
</script>

<!-- Chart code -->
<script>
  am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_animated);
  // Themes end

  // Create chart instance
  var chart = am4core.create("chartdiv", am4charts.XYChart);
  chart.scrollbarX = new am4core.Scrollbar();

  // Add data
  chart.data = JSON.parse('<?php echo $json_inflasi ?>');


  // Create axes
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "tahun";
  categoryAxis.renderer.grid.template.location = 0;
  categoryAxis.renderer.minGridDistance = 30;
  categoryAxis.renderer.labels.template.horizontalCenter = "right";
  categoryAxis.renderer.labels.template.verticalCenter = "middle";
  categoryAxis.renderer.labels.template.rotation = 270;
  categoryAxis.tooltip.disabled = true;
  categoryAxis.renderer.minHeight = 110;

  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.minWidth = 50;

  // Create series
  var series = chart.series.push(new am4charts.ColumnSeries());
  series.sequencedInterpolation = true;
  series.dataFields.valueY = "inflasi";
  series.dataFields.categoryX = "tahun";
  series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
  series.columns.template.strokeWidth = 0;

  series.tooltip.pointerOrientation = "vertical";

  series.columns.template.column.cornerRadiusTopLeft = 10;
  series.columns.template.column.cornerRadiusTopRight = 10;
  series.columns.template.column.fillOpacity = 0.8;

  // on hover, make corner radiuses bigger
  var hoverState = series.columns.template.column.states.create("hover");
  hoverState.properties.cornerRadiusTopLeft = 0;
  hoverState.properties.cornerRadiusTopRight = 0;
  hoverState.properties.fillOpacity = 1;

  series.columns.template.adapter.add("fill", function(fill, target) {
    return chart.colors.getIndex(target.dataItem.index);
  });

  // Cursor
  chart.cursor = new am4charts.XYCursor();

  }); // end am4core.ready()
</script>

<!-- HTML -->