<section class="content-header">
  <h1><i class="fa fa-file-pdf"></i> &nbsp; Pertumbuhan GDP Indonesia </h1>        
</section>

<style>
  .error{ color:red; } 
  #chartdiv {
    width: 100%;
    height: 600px;
  }
</style>

<section class="content">
  <?php if (is_admin() == 1): ?>
  <div class="row">
    <div class="col-lg-6">&nbsp;</div>
    <div class="col-lg-6 col-md-2 col-sm-12 col-xs-12 text-right">
      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-danger btn-block" onclick="form_gdp_ina('delete')"><i class="fa fa-trash"></i>&nbsp;Delete Data</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-primary btn-block" onclick="form_gdp_ina('edit')"><i class="fa fa-edit"></i>&nbsp;Edit Data</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-warning btn-block" onclick="form_gdp_ina()"><i class="fa fa-plus"></i>&nbsp;Tambah Data</a>
        </div>
      </div>
    </div>
  </div>
  <?php endif ?>
  <div class="collapse" id="collapseExample">
    <div class="well">
      <?=form_open(base_url('makro/tambah_gdp_ina'),array('id'=>'form_gdp_ina')); ?>
     <!-- <input type="hidden" name="id" id="product_id">-->
      <div class="row">
        <div class="col-md-6"><input type="number" name="tahun" value="" placeholder="Tahun" class="form-control" id="tahun"></div>
        <div class="col-md-4"><input type="number" name="gdp_ina" value="" placeholder="GDP Indonesia in USD" class="form-control" id="gdp_ina"></div>
        <div class="col-md-2"><input type="submit" name="submit" value="Tambah Data" class="btn btn-success"></div>
      </div>
      <?=form_close(); ?>
      
    </div>
  </div>
  <div class="box">
    <div class="box-body table-responsive">
      <div id="chartdiv"></div>
    </div>
  </div>
</section>




<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
  $(function () {
    $("#table").DataTable();
  });
  $("#makro").addClass('active');
  $("#makro .gdp_ina").addClass('active');

  function form_gdp_ina(id = ''){
    if (id.length == 0) {
      var url = base_url + 'makro/form_gdp_ina/';
      var header = 'Form Tambah GDP Indonesia';
      var label = 'Tambah';
    }
    else {
      var url = base_url + 'makro/form_gdp_ina/' + id;
      var header = 'Form Edit GDP Indonesia';
      var label = capitalizeFirstLetter(id);
    }

    var footer = '<div class="row">\
            <div class="col-lg-9">&nbsp;</div>\
            <div class="col-lg-3"><button type="button" class="btn btn-primary btn-block px-4" onclick="submit()">'+label+'</button></div>\
          </div>';
    Modal('form_user', header, url, footer, '', 'auto');
  }
</script>

<!-- Chart code -->
<script>
  am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_animated);
  // Themes end

  // Create chart instance
  var chart = am4core.create("chartdiv", am4charts.XYChart);
  chart.scrollbarX = new am4core.Scrollbar();

  // Add data
  chart.data = JSON.parse('<?php echo $json_gdp_ina ?>');

  // chart.data = [{"tahun":"2000","gdp_ina":"4.80"},{"tahun":"2001","gdp_ina":"3.60"},{"tahun":"2002","gdp_ina":"4.50"},{"tahun":"2003","gdp_ina":"4.80"},{"tahun":"2004","gdp_ina":"5.00"},{"tahun":"2005","gdp_ina":"5.70"},{"tahun":"2006","gdp_ina":"5.50"},{"tahun":"2007","gdp_ina":"6.30"},{"tahun":"2008","gdp_ina":"6.00"},{"tahun":"2009","gdp_ina":"4.60"},{"tahun":"2010","gdp_ina":"6.20"},{"tahun":"2011","gdp_ina":"6.50"},{"tahun":"2012","gdp_ina":"5.87"},{"tahun":"2013","gdp_ina":"5.61"},{"tahun":"2014","gdp_ina":"5.01"},{"tahun":"2015","gdp_ina":"4.88"},{"tahun":"2016","gdp_ina":"5.03"},{"tahun":"2017","gdp_ina":"5.07"},{"tahun":"2018","gdp_ina":"5.17"},{"tahun":"2019","gdp_ina":"5.03"},{"tahun":"2020","gdp_ina":"-2.07"}];

  // Create axes
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "tahun";
  categoryAxis.renderer.grid.template.location = 0;
  categoryAxis.renderer.minGridDistance = 30;
  categoryAxis.renderer.labels.template.horizontalCenter = "right";
  categoryAxis.renderer.labels.template.verticalCenter = "middle";
  categoryAxis.renderer.labels.template.rotation = 270;
  categoryAxis.tooltip.disabled = true;
  categoryAxis.renderer.minHeight = 110;

  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.minWidth = 50;

  // Create series
  var series = chart.series.push(new am4charts.ColumnSeries());
  series.sequencedInterpolation = true;
  series.dataFields.valueY = "gdp_ina";
  series.dataFields.categoryX = "tahun";
  series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
  series.columns.template.strokeWidth = 0;

  series.tooltip.pointerOrientation = "vertical";

  series.columns.template.column.cornerRadiusTopLeft = 10;
  series.columns.template.column.cornerRadiusTopRight = 10;
  series.columns.template.column.fillOpacity = 0.8;

  // on hover, make corner radiuses bigger
  var hoverState = series.columns.template.column.states.create("hover");
  hoverState.properties.cornerRadiusTopLeft = 0;
  hoverState.properties.cornerRadiusTopRight = 0;
  hoverState.properties.fillOpacity = 1;

  series.columns.template.adapter.add("fill", function(fill, target) {
    return chart.colors.getIndex(target.dataItem.index);
  });

  // Cursor
  chart.cursor = new am4charts.XYCursor();

  }); // end am4core.ready()
</script>

<!-- HTML -->
