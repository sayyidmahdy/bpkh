<section class="content-header">
  <h1><i class="fa fa-file-pdf"></i> &nbsp; Pertumbuhan GDP Kerajaan Saudi Arabia</h1>        
</section>

<style>
  .error{ color:red; } 
  #chartdiv {
    width: 100%;
    height: 600px;
  }
</style>

<section class="content">
  <?php if (is_admin() == 1): ?>
  <div class="row">
    <div class="col-lg-6">&nbsp;</div>
    <div class="col-lg-6 col-md-2 col-sm-12 col-xs-12 text-right">
      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-danger btn-block" onclick="form_gdp_ksa('delete')"><i class="fa fa-trash"></i>&nbsp;Delete Data</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-primary btn-block" onclick="form_gdp_ksa('edit')"><i class="fa fa-edit"></i>&nbsp;Edit Data</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-warning btn-block" onclick="form_gdp_ksa()"><i class="fa fa-plus"></i>&nbsp;Tambah Data</a>
        </div>
      </div>
    </div>
  </div>
  <?php endif ?>
  <div class="box">
    <div class="box-body table-responsive">
      <div id="chartdiv"></div>
    </div>
  </div>
</section>



<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
  $(function () {
    $("#table").DataTable();
  });

  $("#makro").addClass('active');
  $("#makro .gdp_ksa").addClass('active');

  function form_gdp_ksa(id = ''){
    if (id.length == 0) {
      var url = base_url + 'makro/form_gdp_ksa/';
      var header = 'Form Tambah GDP Kerajaan Saudi Arabia';
      var label = 'Tambah';
    }
    else {
      var url = base_url + 'makro/form_gdp_ksa/' + id;
      var header = 'Form Edit GDP Kerajaan Saudi Arabia';
      var label = capitalizeFirstLetter(id);
    }

    var footer = '<div class="row">\
            <div class="col-lg-9">&nbsp;</div>\
            <div class="col-lg-3"><button type="button" class="btn btn-primary btn-block px-4" onclick="submit()">'+label+'</button></div>\
          </div>';
    Modal('form_user', header, url, footer, '', 'auto');
  }
</script>

<script>
  am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_animated);
  // Themes end

  // Create chart instance
  var chart = am4core.create("chartdiv", am4charts.XYChart);
  chart.scrollbarX = new am4core.Scrollbar();

  // Add data
  chart.data = JSON.parse('<?php echo $json_gdp_ksa ?>');

  // chart.data = [{"tahun":"2000","gdp_ksa":"4.80"},{"tahun":"2001","gdp_ksa":"3.60"},{"tahun":"2002","gdp_ksa":"4.50"},{"tahun":"2003","gdp_ksa":"4.80"},{"tahun":"2004","gdp_ksa":"5.00"},{"tahun":"2005","gdp_ksa":"5.70"},{"tahun":"2006","gdp_ksa":"5.50"},{"tahun":"2007","gdp_ksa":"6.30"},{"tahun":"2008","gdp_ksa":"6.00"},{"tahun":"2009","gdp_ksa":"4.60"},{"tahun":"2010","gdp_ksa":"6.20"},{"tahun":"2011","gdp_ksa":"6.50"},{"tahun":"2012","gdp_ksa":"5.87"},{"tahun":"2013","gdp_ksa":"5.61"},{"tahun":"2014","gdp_ksa":"5.01"},{"tahun":"2015","gdp_ksa":"4.88"},{"tahun":"2016","gdp_ksa":"5.03"},{"tahun":"2017","gdp_ksa":"5.07"},{"tahun":"2018","gdp_ksa":"5.17"},{"tahun":"2019","gdp_ksa":"5.03"},{"tahun":"2020","gdp_ksa":"-2.07"}];

  // Create axes
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "tahun";
  categoryAxis.renderer.grid.template.location = 0;
  categoryAxis.renderer.minGridDistance = 30;
  categoryAxis.renderer.labels.template.horizontalCenter = "right";
  categoryAxis.renderer.labels.template.verticalCenter = "middle";
  categoryAxis.renderer.labels.template.rotation = 270;
  categoryAxis.tooltip.disabled = true;
  categoryAxis.renderer.minHeight = 110;

  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.minWidth = 50;

  // Create series
  var series = chart.series.push(new am4charts.ColumnSeries());
  series.sequencedInterpolation = true;
  series.dataFields.valueY = "gdp_ksa";
  series.dataFields.categoryX = "tahun";
  series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
  series.columns.template.strokeWidth = 0;

  series.tooltip.pointerOrientation = "vertical";

  series.columns.template.column.cornerRadiusTopLeft = 10;
  series.columns.template.column.cornerRadiusTopRight = 10;
  series.columns.template.column.fillOpacity = 0.8;

  // on hover, make corner radiuses bigger
  var hoverState = series.columns.template.column.states.create("hover");
  hoverState.properties.cornerRadiusTopLeft = 0;
  hoverState.properties.cornerRadiusTopRight = 0;
  hoverState.properties.fillOpacity = 1;

  series.columns.template.adapter.add("fill", function(fill, target) {
    return chart.colors.getIndex(target.dataItem.index);
  });

  // Cursor
  chart.cursor = new am4charts.XYCursor();

  }); // end am4core.ready()
</script>


<script type="text/javascript">
  $(function () {
    $("#table").DataTable();
  });
    
  var SITEURL = '<?php echo base_url(); ?>';
  if ($("#form_gdp_ksa").length > 0) {
    $("#form_gdp_ksa").validate( {
      rules: {
        tahun: {
          required: true,
          minlength: 2
        },
        gdp_ksa: {
          required: true,
        }
      },
      messages: {
        tahun: {
          required: "Tahun wajib diisi",
          minlength: jQuery.validator.format("At least {0} characters required!")
        },
        gdp_ksa: {
          required: "GDP Kerajaan Saudi Arabia wajib diisi",
        }
      },
      submitHandler: function(form) {
        $.ajax({
          url: SITEURL + "makro/tambah_gdp_ksa",
          data: $('#form_gdp_ksa').serialize(),
          type:"post",
          dataType: 'json',
          success: function(res){
             var gdp_ksa = '<tr id="id_' + res.data.id + '"><td>' + res.data.tahun + '</td><td>' + res.data.gdp_ksa + '</td><td class="text-center"><a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger" data-href="' + SITEURL + '/makro/hapus_gdp_ksa/' + res.data.id + '" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a></td>';


              $('#table').prepend(gdp_ksa);          
              $('#table tr#id_' + res.data.id).addClass("success").delay(1000).queue(function(){
                $(this).removeClass("success", 1000).dequeue();
              });
            },
           error: function (data) {
                  console.log('Error:', data);
               }
        });
      }
    });  
  } //endif

  $('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
  });

  $("#makro").addClass('active');
  $("#makro .gdp_ksa").addClass('active');
</script>

<!-- Chart code -->
