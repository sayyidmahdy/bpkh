<section class="content-header">
  <h1><i class="fa fa-kaaba"></i> Alokasi Investasi BPKH 

  </h1>
 
</section>


<section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="box">
          <?php if (is_admin() == 1): ?>
          <form class="box-body" id="form-search" action="<?php echo base_url('alokasi/index') ?>" method="GET">
          <?php else: ?>
          <form class="box-body" id="form-search" action="<?php echo base_url('visitor/alokasi/index') ?>" method="GET">
          <?php endif ?>
            <div class="<?php echo search_class() ?>">
              <div class="<?php echo year_class() ?>">
                <div class="form-group">
                  <?php dropdown_year() ?>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                  <button class="btn btn-primary btn-block" onclick="search_data()" type="button">
                    <i class="fas fa-search"></i>
                    Seacrh
                  </button>
                </div>
              </div>
            </div>
            <div class="<?php echo generate_class() ?>">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group <?php echo form_group_class() ?>">
                  <a href="javascript:void(0)" class="btn btn-primary btn-block" onclick="generate_data()"><i class="fas fa-file-excel"></i>&nbsp; Export Data ke Excel</a>
                </div>
              </div>
            </div>

            <?php if (is_admin() == 1): ?>
            <div class="<?php echo create_class() ?>">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <a href="<?=base_url('alokasi/tambah_alokasi_investasi'); ?>" class="btn btn-warning btn-block"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a>
                </div>
              </div>
            </div>
            <?php endif ?>
          </form>
        </div>

        <div class="box">
        <div class="box-body">
          <?php if($all_data) { ?>
          <div class="row">
            <div class="col-md-12 text-right">
              <?php if ($selected_year > '2019'): ?>
              <label><b><i>(dalam miliar rupiah)</i></b></label>
              <?php else: ?>
              <label><b><i>(dalam rupiah)</i></b></label>
              <?php endif ?>
            </div>
          </div>
          <div style="overflow-x: scroll; min-height:500px;">
            <table class="keuangan table table-striped table-bordered" style="width:2200px;min-width: 100%;">
              <thead>
                <tr class="text-center">
                  <th>Bidang</th>
                  <?php if (is_admin() == 1): ?>
                  <th>Hapus</th>
                  <?php endif ?>
                  <th>Januari</th>
                  <th>Februari</th>
                  <th>Maret</th>
                  <th>April</th>
                  <th>Mei</th>
                  <th>Juni</th>
                  <th>Juli</th>
                  <th>Agustus</th>
                  <th>September</th>
                  <th>Oktober</th>
                  <th>November</th>
                  <th>Desember</th>
                </tr>
              </thead>
              <?php foreach ($all_data as $row) {
              if(strtoupper($row->bidang) == "TOTAL") { ?>
              <tfoot>
                <tr class="total">
                  <td width="20%"><?=$row->bidang; ?></td>
                  <?php if (is_admin() == 1): ?>
                  <td  width="5%" style="text-align: center;">
                    <a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger" data-href="<?=base_url('alokasi/hapus_alokasi_investasi?id='.$row->id .'&tahun=' . $selected_year); ?>" data-toggle="modal" data-target="#confirm-delete">
                      <i class="fa fa-trash-alt"></i>
                    </a>
                  </td>
                <?php endif ?>
                  <td style="text-align: right;"><?=$row->januari; ?></td>
                  <td style="text-align: right;"><?=$row->februari; ?></td>
                  <td style="text-align: right;"><?=$row->maret; ?></td>
                  <td style="text-align: right;"><?=$row->april; ?></td>
                  <td style="text-align: right;"><?=$row->mei; ?></td>
                  <td style="text-align: right;"><?=$row->juni; ?></td>
                  <td style="text-align: right;"><?=$row->juli; ?></td>
                  <td style="text-align: right;"><?=$row->agustus; ?></td>
                  <td style="text-align: right;"><?=$row->september; ?></td>
                  <td style="text-align: right;"><?=$row->oktober; ?></td>
                  <td style="text-align: right;"><?=$row->november; ?></td>
                  <td style="text-align: right;"><?=$row->desember; ?></td>
                </tr>
              </tfoot>
              <?php } else { ?>
              <tr>
                <td><?=$row->bidang; ?></td>
                <?php if (is_admin() == 1): ?>
                  <td  width="5%" style="text-align: center;">
                    <a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger" data-href="<?=base_url('alokasi/hapus_alokasi_investasi?id='.$row->id .'&tahun=' . $selected_year); ?>" data-toggle="modal" data-target="#confirm-delete">
                      <i class="fa fa-trash-alt"></i>
                    </a>
                  </td>
                <?php endif ?>
                <td style="text-align: right;"><?=$row->januari; ?></td>
                <td style="text-align: right;"><?=$row->februari; ?></td>
                <td style="text-align: right;"><?=$row->maret; ?></td>
                <td style="text-align: right;"><?=$row->april; ?></td>
                <td style="text-align: right;"><?=$row->mei; ?></td>
                <td style="text-align: right;"><?=$row->juni; ?></td>
                <td style="text-align: right;"><?=$row->juli; ?></td>
                <td style="text-align: right;"><?=$row->agustus; ?></td>
                <td style="text-align: right;"><?=$row->september; ?></td>
                <td style="text-align: right;"><?=$row->oktober; ?></td>
                <td style="text-align: right;"><?=$row->november; ?></td>
                <td style="text-align: right;"><?=$row->desember; ?></td>
              </tr>
              <?php } // endif
              } //endforeach get bps bpih ?>
              <?php if (is_admin() == 1): ?>
                <!-- <tfoot>
                  <tr>
                    <td>HAPUS</td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/januari/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/februari/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/maret/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/april/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/mei/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/juni/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/juli/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/agustus/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/september/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/oktober/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/november/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                    <td style="text-align: center;">
                      <a class="delete btn btn-xs btn-danger" data-href="<?php echo base_url('alokasi/hapus_alokasi_investasi/desember/'. $selected_year) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
                    </td>
                  </tr>
                </tfoot> -->
              <?php endif ?>
            </table>
          </div>
          <?php } else { echo 'Data Not Found';} ?>
        </div>
      </div>
        
      </div>
    </div>
</section>

<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus</h4>
      </div>
      <div class="modal-body">
        <p>Anda yakin ingin menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <a class="btn btn-danger btn-ok">Hapus</a>
      </div>
    </div>

  </div>
</div>

  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>

  <script>
    $("#alokasi").addClass('active');

    function search_data(){
        var tahun = $('#tahun option:selected').val();

        if (tahun == 0) {
          Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
          return false;
        }

        $('#form-search').submit();
      }

    $(function(){
      $('#tahun').val('<?php echo $selected_year ?>');
      $('#tahun').trigger('change');
    });

    function generate_data(){
      var tahun = $('#tahun option:selected').val();

      if (tahun == 0) {
          Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
          return false;
        }

      var url = '<?=base_url('visitor/alokasi/export_alokasi_investasi/'); ?>' + tahun;
      window.location.href = url;
    }
</script>






