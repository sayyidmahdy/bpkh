<section class="content-header">
	<h1><i class="fa fa-kaaba"></i> Perubahan Aset Neto 
	</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
	          <?php if (is_admin() == 1): ?>
	          <form class="box-body" id="form-search" action="<?php echo base_url('laporankeuangan/perubahan_asetneto') ?>" method="GET">
	          <?php else: ?>
	          <form class="box-body" id="form-search" action="<?php echo base_url('visitor/laporankeuangan/perubahan_asetneto') ?>" method="GET">
	          <?php endif ?>
	            <div class="<?php echo search_class() ?>">
	              <div class="col-lg-3">
	                <div class="form-group">
	                  <?php dropdown_year() ?>
	                </div>
	              </div>
	              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
	                <div class="form-group">
	                  <button class="btn btn-primary btn-block" onclick="search_data()" type="button">
	                    <i class="fas fa-search"></i>
	                    Seacrh
	                  </button>
	                </div>
	              </div>
	            </div>
	            <div class="<?php echo generate_class() ?>">
	              &nbsp;
	            </div>

	            <?php if (is_admin() == 1): ?>
	            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
	              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <div class="form-group">
	                    <a href="<?=base_url('laporankeuangan/tambah_perubahan_asetneto'); ?>" class="btn btn-warning btn-block"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a>
	                </div>
	              </div>
	            </div>
	            <?php endif ?>
	          </form>
	        </div>
			<?php if ($perubahan_asetneto) { ?>

				<div class="box">

					<div class="box-body mx-5">
						<table id="table1" class="table table-striped table-bordered datatables">
							<thead>
								<tr>
									<th>No</th>
									<th>Bulan</th>
									<th class="text-center">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php $no =1; foreach ($perubahan_asetneto as $row) { ?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td>Perubahan Aset Neto Bulan <?= konversiBulanAngkaKeNama($row['bulan']); ?></td>
										<td class="text-center">

											<a style="color:#fff;" title="Lihat Detail" class="btn btn-xs btn-info px-3" href="<?= base_url('visitor/'.$this->router->fetch_class() . '/detail_perubahan_asetneto/' . $row['bulan'] . '/' . $row['tahun']); ?>">
												<i class="fa fa-eye"></i>
												Detail
											</a>
											<a href="<?= base_url('laporankeuangan/export_perubahan_asetneto/' . $row['bulan'] . '/' . $thn); ?>" class="btn btn-warning btn-xs px-3">
												<i class="fas fa-file-excel"></i>&nbsp; Export Data ke Excel
											</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

					</div>
				</div>


			<?php } else {
				echo '<p class="alert alert-success"> Pilih tahun</p>';
			} ?>
		</div>
	</div>
</section>

<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				<p>Anda yakin ingin menghapus data ini?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<script>
	$("#perubahan_asetneto").addClass('active');
	function search_data(){
        var tahun = $('#tahun option:selected').val();

        if (tahun == 0) {
          Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
          return false;
        }

        $('#form-search').submit();
      }

    $(function(){
      $('#tahun').val('<?php echo $selected_year ?>');
      $('#tahun').trigger('change');
    });

    function generate_data(){
      var tahun = $('#tahun option:selected').val();

      if (tahun == 0) {
          Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
          return false;
        }

      var url = '<?=base_url('laporankeuangan/export_perubahan_asetneto/'); ?>' + tahun;
      window.location.href = url;
    }
</script>