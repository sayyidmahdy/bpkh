<section class="content-header">
	<h1><i class="fa fa-kaaba"></i> Laporan Realisasi Anggaran</h1>
</section>

<!-- <style type="text/css">
	.d-flex {
	    display: -webkit-box!important;
	    display: -ms-flexbox!important;
	    display: flex!important;
	}
	.flex-column {
    -webkit-box-orient: vertical!important;
    -webkit-box-direction: normal!important;
    -ms-flex-direction: column!important;
    flex-direction: column!important;
	}
	.pl-7, .px-7 {
	    padding-left: 1.75rem!important;
	}

	/*.btn.btn-primary {
	    color: #fff;
	    background-color: #3699ff;
	    border-color: #3699ff;
	}*/
	.font-size-sm {
	    font-size: .925rem;
	}
	.rounded-xl {
	    border-radius: 1.25rem!important;
	}
	.bg-gray-100 {
	    background-color: #f3f6f9!important;
	}
	.p-10 {
	    padding: 2.5rem!important;
	}
	.flex-grow-1 {
	    -webkit-box-flex: 1!important;
	    -ms-flex-positive: 1!important;
	    flex-grow: 1!important;
	}


	</style>
	<style type="text/css">
		.subContainer .cardNum:after, .subContainer .date:after, .subContainer .ccv:after {
		position: absolute;
		top: -25px;
		font-family: "Roboto", sans-serif;
		left: 0;
		color: rgba(142, 190, 255, 0.9);
		font-size: 13px;
	}

	.subContainer .date p, .subContainer .ccv p {
		padding: 0;
		margin: 0;
		font-size: 15px;
		font-family: "Montserrat", sans-serif;
		font-weight: lighter;
		text-align: center;
		padding: 4px 20px;
		padding-right: 25px;
	}

	* {
		font-family: "Roboto", sans-serif;
		box-sizing: border-box;
		padding: 0;
		margin: 0;
	}

	body {
		overflow: hidden;
	}

	.container {
		position: absolute;
		width: 600px;
		height: 400px;
		background: #FFF;
		box-shadow: 0px 0px 40px 1px #eee;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		display: grid;
		grid-template-columns: repeat(2, 1fr);
		justify-content: space-between;
		grid-template-areas: 'mGrid card';
		min-height: 0;
		min-width: 0;
		border-radius: 9px;
		z-index: 7;
	}

	.mGrid {
		grid-area: mGrid;
		display: grid;
		grid-template-rows: repeat(3, 1fr);
		overflow: hidden;
		justify-content: center;
		margin-top: 15px;
		margin-left: -10px;
	}

	.total {
		padding-top: 50px;
		align-self: end;
	}

	.total p {
		text-align: center;
		font-size: 15px;
		text-transform: uppercase;
		font-weight: 400;
		color: #808eab;
		letter-spacing: .15rem;
	}

	.total p:last-child {
		font-size: 2rem;
		font-weight: bold;
		color: #0b82f8;
		padding-top: 10px;
		font-family: "Roboto", sans-serif;
	}

	.detail {
		font-family: "Roboto", sans-serif;
		align-self: start;
		display: grid;
		margin-top: 32px;
		grid-template-rows: repeat(3, 1fr);
	}

	.detail p {
		text-align: center;
		font-size: 15px;
		text-transform: uppercase;
		font-weight: 400;
		color: #808eab;
		letter-spacing: .15rem;
		margin-bottom: 10px;
	}

	.detail ul {
		list-style-type: none;
		display: grid;
		grid-template-columns: repeat(2, 1fr);
		font-family: "Roboto", sans-serif;
		justify-content: center;
		align-content: start;
	}

	.detail ul li {
		display: inline-block;
		color: #515860;
		text-align: left;
		font-size: 15px;
	}

	.detail ul li:last-child {
		text-align: right;
		color: #0b82f8;
		font-family: "Roboto", sans-serif;
	}

	button {
		display: block;
		justify-self: center;
		align-self: center;
		font-family: "Open Sans", sans-serif;
		background: #ffffff;
		color: #0b82f8;
		border: 1px solid #0b82f8;
		text-transform: uppercase;
		font-size: .97rem;
		opacity: .8;
		font-weight: lighter;
		padding: 7px 40px;
		border-radius: 1.6rem;
		cursor: pointer;
		outline: none;
		transition: all .2s linear;
	}

	button:hover {
		transition: all .2s linear;
		background: #0b82f8;
		color: #FFF;
	}

	.subContainer {
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		height: 270px;
		width: 400px;
		border-radius: 9px;
		font-family: "Roboto", sans-serif;
		bottom: 0;
		color: #fff;
		min-height: 0;
		min-width: 0;
		background: #1f8fff;
		box-shadow: -10px 10px 60px 10px #ADB7C4;
		grid-area: card;
		display: grid;
		grid-template-columns: repeat(7, 1fr);
		grid-template-rows: repeat(3, 1fr);
		grid-template-areas: "name name  . . . visa visa "  "cardNum cardNum cardNum cardNum cardNum . ." " date date date date . ccv ccv ";
	}

	.subContainer p, .subContainer .visaCont, .subContainer .cardNum, .subContainer .date, .subContainer .ccv {
		margin-left: 35px;
		margin-top: -25px;
	}

	.subContainer p {
		padding-top: 25px;
		font-weight: 600;
		grid-area: name;
		margin-top: 0px;
	}

	.subContainer .visaCont {
		display: grid;
		grid-area: visa;
		overflow: hidden;
		margin: 0;
		justify-self: center;
		align-self: center;
	}

	.subContainer .visaCont svg {
		height: 50px;
		width: 100px;
	}

	.subContainer .cardNum {
		position: relative;
		grid-area: cardNum;
		background: #3799ff;
		align-self: center;
		padding: 7px 5px;
		font-size: 19px;
	}

	.subContainer .cardNum:after {
		content: 'Credit  Card  Number';
	}

	.subContainer .cardNum ul {
		list-style-type: none;
		text-align: center;
	}

	.subContainer .cardNum ul li {
		display: inline-block;
		font-family: "Montserrat", sans-serif;
		margin: 0 auto;
		letter-spacing: 1px;
	}

	.subContainer .cardNum ul li:last-child {
		text-align: right;
	}

	.subContainer .date {
		position: relative;
		grid-area: date;
		background: #3799ff;
		align-self: center;
		justify-self: left;
	}

	.subContainer .date:after {
		content: 'Expiration';
	}

	.subContainer .ccv {
		position: relative;
		grid-area: ccv;
		align-self: center;
		justify-self: left;
		background: #3799ff;
		margin-left: 0;
	}

	.subContainer .ccv:after {
		content: 'CCV';
	}

	.subContainer .ccv p {
		padding: 4px 12px;
		letter-spacing: 1px;
	}
</style> -->
<div class="container" style="display: none;">
	<div class="mGrid">
		<div class="total">
			<p>total</p>
			<p>$898</p>
		</div>
		<div class="detail">
			<p>detail</p>
			<ul>
				<li>iPad Pro</li>
				<li>$799</li>
			</ul>
			<ul>
				<li>Apple Pencil</li>
				<li>$99</li>
			</ul>
		</div><button>pay now</button>
		</div><!-- Small Side Card detail-->
		<div class="subContainer">
			<p>Carly Ling</p><!-- svg Logo-->
			<div class="visaCont"><svg class="visa" enable-background="new 0 0 291.764 291.764" version="1.1" viewbox="5 70 290 200" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
			<path class="svgcolor" d="m119.26 100.23l-14.643 91.122h23.405l14.634-91.122h-23.396zm70.598 37.118c-8.179-4.039-13.193-6.765-13.193-10.896 0.1-3.756 4.24-7.604 13.485-7.604 7.604-0.191 13.193 1.596 17.433 3.374l2.124 0.948 3.182-19.065c-4.623-1.787-11.953-3.756-21.007-3.756-23.113 0-39.388 12.017-39.489 29.204-0.191 12.683 11.652 19.721 20.515 23.943 9.054 4.331 12.136 7.139 12.136 10.987-0.1 5.908-7.321 8.634-14.059 8.634-9.336 0-14.351-1.404-21.964-4.696l-3.082-1.404-3.273 19.813c5.498 2.444 15.609 4.595 26.104 4.705 24.563 0 40.546-11.835 40.747-30.152 0.08-10.048-6.165-17.744-19.659-24.035zm83.034-36.836h-18.108c-5.58 0-9.82 1.605-12.236 7.331l-34.766 83.509h24.563l6.765-18.08h27.481l3.51 18.153h21.664l-18.873-90.913zm-26.97 54.514c0.474 0.046 9.428-29.514 9.428-29.514l7.13 29.514h-16.558zm-160.86-54.796l-22.931 61.909-2.498-12.209c-4.24-14.087-17.533-29.395-32.368-36.999l20.998 78.33h24.764l36.799-91.021h-24.764v-0.01z" fill="#FFF"></path>
		<path class="svgtipcolor" d="m51.916 111.98c-1.787-6.948-7.486-11.634-15.226-11.734h-36.316l-0.374 1.686c28.329 6.984 52.107 28.474 59.821 48.688l-7.905-38.64z" fill="#EFC75E"></path>
	</svg></div>
	<div class="cardNum">
		<ul>
			<li>4514</li>
			<li>6188</li>
			<li>1234</li>
			<li>5678</li>
		</ul>
	</div>
	<div class="date">
		<p class="datepara">January        2018</p>
	</div>
	<div class="ccv">
		<p>983</p>
	</div>
</div>
</div>
<div class="backlayer"></div>


<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
	          <?php if (is_admin() == 1): ?>
	          <form class="box-body" id="form-search" action="<?php echo base_url('laporankeuangan/realisasi_anggaran') ?>" method="GET">
	          <?php else: ?>
	          <form class="box-body" id="form-search" action="<?php echo base_url('visitor/laporankeuangan/realisasi_anggaran') ?>" method="GET">
	          <?php endif ?>
	            <div class="<?php echo search_class() ?>">
	              <div class="col-lg-3">
	                <div class="form-group">
	                  <?php dropdown_year() ?>
	                </div>
	              </div>
	              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
	                <div class="form-group">
	                  <button class="btn btn-primary btn-block" onclick="search_data()" type="button">
	                    <i class="fas fa-search"></i>
	                    Seacrh
	                  </button>
	                </div>
	              </div>
	            </div>
	            <div class="<?php echo generate_class() ?>">
	              &nbsp;
	            </div>

	            <?php if (is_admin() == 1): ?>
	            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right p-0">
	              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <div class="form-group">
	                    <a href="<?=base_url('laporankeuangan/tambah_realisasi_anggaran'); ?>" class="btn btn-warning btn-block"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a>
	                </div>
	              </div>
	            </div>
	            <?php endif ?>
	          </form>
	        </div>
			<?php if ($realisasi_anggaran) { ?>

			<div class="box">

				<div class="box-body mx-5">
				<table id="table1" class="table table-striped table-bordered datatables">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Bulan</th>
							<th class="text-center">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($realisasi_anggaran as $row) { ?>
						<tr>
							<td width="5%"><?php echo $no++ ?></td>
							<td>Laporan Realisasi Anggaran Bulan <?= konversiBulanAngkaKeNama($row['bulan']); ?></td>
							<td class="text-center">

								<a style="color:#fff;" title="Lihat Detail" class="btn btn-xs btn-primary px-4" href="<?= base_url(url_validation() . $this->router->fetch_class() . '/detail_realisasi_anggaran/' . $row['bulan'] .'/' .$row['tahun']); ?>">
									<i class="fa fa-eye"></i>&nbsp; Detail
								</a>
								<?php if (is_admin() == 1): ?>
								<a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger px-4" data-href="<?= base_url($this->router->fetch_class() . '/hapus_realisasi_anggaran/' . $row['bulan'].'/' .$row['tahun']); ?>" data-toggle="modal" data-target="#confirm-delete"> 
									<i class="fa fa-trash-alt"></i> &nbsp; Hapus
								</a>
								<?php endif ?>
								<a href="<?= base_url('visitor/laporankeuangan/export_realisasi_anggaran/'. $row['bulan'] .'/' . $thn); ?>" class="btn btn-warning btn-xs px-4">
									<i class="fas fa-file-excel"></i>&nbsp; Export Data ke Excel
								</a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>


				</div>
			</div>

			
			<?php } else {
				echo '<p class="alert alert-success"> Pilih tahun</p>';
			} ?>
		</div>
	</div>
</section>

<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				<p>Anda yakin ingin menghapus data ini?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function (e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});

</script>

<script>
	$("#realisasi_anggaran").addClass('active');
	function search_data(){
      var tahun = $('#tahun option:selected').val();

      if (tahun == 0) {
        Swal.fire('', 'Silahkah Pilih Tahun Terlebih Dahulu', 'info');
        return false;
      }

      $('#form-search').submit();
    }

  $(function(){
    $('#tahun').val('<?php echo $selected_year ?>');
    $('#tahun').trigger('change');
  });

</script>
