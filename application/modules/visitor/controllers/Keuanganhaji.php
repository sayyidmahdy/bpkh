<?php defined('BASEPATH') or exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory; 
use PhpOffice\PhpSpreadsheet\Style\Alignment; 
class Keuanganhaji extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('keuanganhaji/keuanganhaji_model', 'keuanganhaji_model');
		$this->load->model('keuanganhaji/alokasi_model', 'alokasi_model');
		$this->load->library('excel');
	}

	public function index()
	{
		$data['view'] = 'index';
		$this->load->view('admin/layout', $data);
	}

	public function total_dana_kelolaan_non_dau()
	{
		$data['view'] = 'total_dana_kelolaan_non_dau';
		$this->load->view('admin/layout', $data);
	}

	public function total_dana_kelolaan_dau()
	{
		$data['view'] = 'total_dana_kelolaan_dau';
		$this->load->view('admin/layout', $data);
	}

	// SEBARAN DANA HAJI
	public function porsi_penempatan_bps_bpih($tahun = 0)
	{
		$tahun = ($tahun != '') ? $tahun : date('Y');

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sebaran_dana_haji(); // untuk menu pilihan tahun
		$data['sebaran_dana_haji'] = $this->keuanganhaji_model->get_sebaran_dana_haji($tahun);

		$data['view'] = 'keuanganhaji/posisi_sebaran_dana_haji';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_porsi_penempatan_bps_bpih($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'porsi_penempatan_bps_bpih_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_porsi_penempatan_bps_bpih($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Porsi Penempatan di Bank BPS-BPIH Tahun " . $tahun)
			->setSubject("Porsi Penempatan di Bank BPS-BPIH Tahun " . $tahun)
			->setDescription("Porsi Penempatan di Bank BPS-BPIH Tahun " . $tahun)
			->setKeywords("Porsi Penempatan di Bank BPS-BPIH");



		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Porsi Penempatan di Bank BPS-BPIH Tahun " . $tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1


		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'BPS BPIH');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['bps_bpih']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['januari']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['februari']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['maret']);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['april']);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['mei']);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['juni']);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['juli']);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['agustus']);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['september']);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['oktober']);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['november']);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['desember']);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	// SEBARAN DANA HAJI
	public function sebaran_dana_haji($tahun = 0)
	{
		$year = $this->input->get('tahun');
		$bank = $this->input->get('bank');
		$last_year = $this->db->select('tahun')->from('sebaran_dana_haji2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		$year = ($year != '') ? $year : $last_year->tahun;
		$bank = ($bank != '') ? $bank : 'all';

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sebaran_dana_haji(); // untuk menu pilihan tahun
		$data['sebaran_dana_haji'] = $this->keuanganhaji_model->get_sebaran_dana_haji($year, $bank);

		$data['dropdown_bank'] = $this->keuanganhaji_model->get_sebaran_dana_haji($year, 'all');
		$data['selected_year'] = $year;
		$data['selected_bank'] = $bank;
		$data['view'] = 'visitor/keuanganhaji/posisi_sebaran_dana_haji';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_sebaran_dana_haji($tahun, $bank = NULL)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'posisi_sebaran_dana_haji_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';
		$sebaran = $this->keuanganhaji_model->get_sebaran_dana_haji($tahun, $bank);
		
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Posisi Sebaran Dana Haji Tahun " . $tahun)
			->setSubject("Posisi Sebaran Dana Haji Tahun " . $tahun)
			->setDescription("Posisi Sebaran Dana Haji Tahun " . $tahun)
			->setKeywords("Posisi Sebaran Dana Haji");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Posisi Sebaran Dana Haji Tahun " . $tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(dalam rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:N3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'BPS BPIH');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['bps_bpih']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['januari']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['februari']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['maret']);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['april']);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['mei']);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['juni']);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['juli']);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['agustus']);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['september']);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['oktober']);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['november']);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['desember']);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Posisi Sebaran Dana Haji" . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	//sbssn RUPIAH

	public function sdhi_rupiah($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$sukuk = $this->input->get('sukuk');
		$last_year = $this->db->select('tahun')->from('sdhi_rupiah2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		if ($last_year) {
			$year = ($year != '') ? $year : $last_year->tahun;
		}

		$data['thn'] = $year;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sdhi_rupiah();
		$data['sdhi_rupiah'] = $this->keuanganhaji_model->get_sdhi_rupiah($year);

		$data['selected_year'] = $year;
		$data['view'] = 'visitor/keuanganhaji/sdhi_rupiah';
		$this->load->view('admin/layout', $data);
	}
	public function detail_sdhi_rupiah($bulan=0, $tahun = 0)
	{
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sdhi_rupiah();
		$data['sdhi_rupiah'] = $this->keuanganhaji_model->get_detail_sdhi_rupiah($bulan, $tahun);
		$data['view'] = 'visitor/keuanganhaji/detail_sdhi_rupiah';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_sdhi_rupiah($bulan,$tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'laporan_sdhi_rupiah_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_detail_sdhi_rupiah($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan SDHI Rupiah Bulan ". $bulan. " " . $tahun)
			->setSubject("Laporan SDHI Rupiah Bulan ". $bulan. " " . $tahun)
			->setDescription("Laporan SDHI Rupiah Bulan ". $bulan. " " . $tahun)
			->setKeywords("Laporan SDHI Rupiah");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan SDHI Rupiah Bulan ". $bulan. " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('E3', "(dalam rupiah)");
		$excel->getActiveSheet()->getStyle('E3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('E3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('E3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT); // Set text center untuk kolom A1

		$excel->getActiveSheet()->SetCellValue('A4', 'No');
		$excel->getActiveSheet()->SetCellValue('B4', 'Instrumen');
		$excel->getActiveSheet()->SetCellValue('C4', 'Maturity');
		$excel->getActiveSheet()->SetCellValue('D4', 'Counterpart');
		$excel->getActiveSheet()->SetCellValue('E4', 'Nominal');
		
		$no=1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['instrumen']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['maturity']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['counterpart']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['nilai']);
		

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		$excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A13')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A14')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A15')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A18')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A19')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A22')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A23')->getFont()->setBold(TRUE);

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	//sbssn RUPIAH

	public function sbssn_rupiah($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$sukuk = $this->input->get('sukuk');
		$last_year = $this->db->select('tahun')->from('sbssn_rupiah2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		if ($last_year) {
			$year = ($year != '') ? $year : $last_year->tahun;
		}

		$data['thn'] = $year;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sbssn_rupiah();
		$data['sbssn_rupiah'] = $this->keuanganhaji_model->get_sbssn_rupiah($year);

		$data['selected_year'] = $year;
		$data['view'] = 'visitor/keuanganhaji/sbssn_rupiah';
		$this->load->view('admin/layout', $data);
	}
	public function detail_sbssn_rupiah($bulan=0, $tahun = 0)
	{
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sbssn_rupiah();
		$data['sbssn_rupiah'] = $this->keuanganhaji_model->get_detail_sbssn_rupiah($bulan, $tahun);
		$data['view'] = 'visitor/keuanganhaji/detail_sbssn_rupiah';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_sbssn_rupiah($bulan,$tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'laporan_sbssn_rupiah_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_detail_sbssn_rupiah($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan SBSSN Rupiah Bulan ". $bulan. " " . $tahun)
			->setSubject("Laporan SBSSN Rupiah Bulan ". $bulan. " " . $tahun)
			->setDescription("Laporan SBSSN Rupiah Bulan ". $bulan. " " . $tahun)
			->setKeywords("Laporan SBSSN Rupiah");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan SBSSN Rupiah Bulan ". $bulan. " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('E3', "(dalam rupiah)");
		$excel->getActiveSheet()->getStyle('E3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('E3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('E3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT); // Set text center untuk kolom A1

		$excel->getActiveSheet()->SetCellValue('A4', 'No');
		$excel->getActiveSheet()->SetCellValue('B4', 'Instrumen');
		$excel->getActiveSheet()->SetCellValue('C4', 'Maturity');
		$excel->getActiveSheet()->SetCellValue('D4', 'Counterpart');
		$excel->getActiveSheet()->SetCellValue('E4', 'Nominal');
		
		$no=1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['instrumen']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['maturity']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['counterpart']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['nilai']);
		

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		$excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A13')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A14')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A15')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A18')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A19')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A22')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A23')->getFont()->setBold(TRUE);

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	//sbssn USD

	public function sbssn_usd($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$sukuk = $this->input->get('sukuk');
		$last_year = $this->db->select('tahun')->from('sbssn_usd2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		if ($last_year) {
			$year = ($year != '') ? $year : $last_year->tahun;
		}

		$data['thn'] = $year;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sbssn_usd();
		$data['sbssn_usd'] = $this->keuanganhaji_model->get_sbssn_usd($year);

		$data['selected_year'] = $year;
		$data['view'] = 'visitor/keuanganhaji/sbssn_usd';
		$this->load->view('admin/layout', $data);
	}
	public function detail_sbssn_usd($bulan=0, $tahun = 0)
	{
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sbssn_usd();
		$data['sbssn_usd'] = $this->keuanganhaji_model->get_detail_sbssn_usd($bulan, $tahun);
		$data['view'] = 'visitor/keuanganhaji/detail_sbssn_usd';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_sbssn_usd($bulan,$tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'laporan_sbssn_usd_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_detail_sbssn_usd($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan SBSSN USD Bulan ". $bulan. " " . $tahun)
			->setSubject("Laporan SBSSN USD Bulan ". $bulan. " " . $tahun)
			->setDescription("Laporan SBSSN USD Bulan ". $bulan. " " . $tahun)
			->setKeywords("Laporan SBSSN USD");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan SBSSN USD Bulan ". $bulan. " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('E3', "USD($)");
		$excel->getActiveSheet()->getStyle('E3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('E3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('E3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT); // Set text center untuk kolom A1

		$excel->getActiveSheet()->SetCellValue('A4', 'No');
		$excel->getActiveSheet()->SetCellValue('B4', 'Instrumen');
		$excel->getActiveSheet()->SetCellValue('C4', 'Maturity');
		$excel->getActiveSheet()->SetCellValue('D4', 'Counterpart');
		$excel->getActiveSheet()->SetCellValue('E4', 'Nominal');
		
		$no=1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['instrumen']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['maturity']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['counterpart']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['nilai']);
		

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		$excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A13')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A14')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A15')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A18')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A19')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A22')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A23')->getFont()->setBold(TRUE);

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	//sukuk korporasi

	//sbssn RUPIAH

	public function sukuk_korporasi($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$sukuk = $this->input->get('sukuk');
		$last_year = $this->db->select('tahun')->from('sukuk_korporasi2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		if ($last_year) {
			$year = ($year != '') ? $year : $last_year->tahun;
		}

		$data['thn'] = $year;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sukuk_korporasi();
		$data['sukuk_korporasi'] = $this->keuanganhaji_model->get_sukuk_korporasi($year);

		$data['selected_year'] = $year;
		$data['view'] = 'visitor/keuanganhaji/sukuk_korporasi';
		$this->load->view('admin/layout', $data);
	}
	public function detail_sukuk_korporasi($bulan=0, $tahun = 0)
	{
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_sukuk_korporasi();
		$data['sukuk_korporasi'] = $this->keuanganhaji_model->get_detail_sukuk_korporasi($bulan, $tahun);
		$data['view'] = 'visitor/keuanganhaji/detail_sukuk_korporasi';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_sukuk_korporasi($bulan,$tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'laporan_sukuk_korporasi_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_detail_sukuk_korporasi($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan Sukuk Korporasi Bulan ". $bulan. " " . $tahun)
			->setSubject("Laporan Sukuk Korporasi Bulan ". $bulan. " " . $tahun)
			->setDescription("Laporan Sukuk Korporasi Bulan ". $bulan. " " . $tahun)
			->setKeywords("Laporan Sukuk Korporasi");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan Sukuk Korporasi Bulan ". $bulan. " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('E3', "(Rp in Milyar)");
		$excel->getActiveSheet()->getStyle('E3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('E3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('E3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT); // Set text center untuk kolom A1

		$excel->getActiveSheet()->SetCellValue('A4', 'No');
		$excel->getActiveSheet()->SetCellValue('B4', 'Instrumen');
		$excel->getActiveSheet()->SetCellValue('C4', 'Maturity');
		$excel->getActiveSheet()->SetCellValue('D4', 'Counterpart');
		$excel->getActiveSheet()->SetCellValue('E4', 'Nominal');
		
		$no=1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['instrumen']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['maturity']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['counterpart']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['nilai']);
		

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		$excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A13')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A14')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A15')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A18')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A19')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A22')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A23')->getFont()->setBold(TRUE);

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	// reksadana_terproteksi_syariah
	public function reksadana_terproteksi_syariah($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$reksadana = $this->input->get('reksadana');
		$last_year = $this->db->select('tahun')->from('reksadana_terproteksi_syariah')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		if ($last_year) {
			$year = ($year != '') ? $year : $last_year->tahun;
		}

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_reksadana_terproteksi_syariah(); // untuk menu pilihan tahun
		$data['reksadana_terproteksi_syariah'] = $this->keuanganhaji_model->get_reksadana_terproteksi_syariah($year);

		$data['selected_year'] = $year;

		$data['view'] = 'visitor/keuanganhaji/reksadana_terproteksi_syariah';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_reksadana_terproteksi_syariah($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'reksadana_terproteksi_syariah_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_reksadana_terproteksi_syariah($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Reksadana Terproteksi Syariah Tahun " . $tahun)
			->setSubject("Reksadana Terproteksi Syariah Tahun " . $tahun)
			->setDescription("Reksadana Terproteksi Syariah Tahun " . $tahun)
			->setKeywords("Reksadana Terproteksi Syariah");



		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Reksadana Terproteksi Syariah Tahun " . $tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(Dalam Milyar Rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:N3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'Instrumen RTDS');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['instrumen']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['januari']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['februari']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['maret']);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['april']);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['mei']);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['juni']);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['juli']);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['agustus']);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['september']);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['oktober']);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['november']);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['desember']);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);



			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("RTS" . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}
	// reksadana pasar uang syariah

	public function reksadana_pasar_uang_syariah($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$reksadana = $this->input->get('reksadana');
		$last_year = $this->db->select('tahun')->from('reksadana_pasar_uang_syariah')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		if ($last_year) {
			$year = ($year != '') ? $year : $last_year->tahun;
		}

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_reksadana_pasar_uang_syariah(); // untuk menu pilihan tahun
		$data['reksadana_pasar_uang_syariah'] = $this->keuanganhaji_model->get_reksadana_pasar_uang_syariah($year);

		$data['selected_year'] = $year;

		$data['view'] = 'visitor/keuanganhaji/reksadana_pasar_uang_syariah';
		$this->load->view('admin/layout', $data);
	}

	


	public function export_reksadana_pasar_uang_syariah($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'reksadana_pasar_uang_syariah_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_reksadana_pasar_uang_syariah($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Reksadana Pasar Uang Syariah Tahun " . $tahun)
			->setSubject("Reksadana Pasar Uang Syariah Tahun " . $tahun)
			->setDescription("Reksadana Pasar Uang Syariah Tahun " . $tahun)
			->setKeywords("Reksadana Pasar Uang Syariah");



		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Reksadana Pasar Uang Syariah Tahun " . $tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(Dalam Milyar Rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:N3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'Instrumen RTDS');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['instrumen']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['januari']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['februari']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['maret']);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['april']);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['mei']);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['juni']);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['juli']);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['agustus']);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['september']);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['oktober']);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['november']);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['desember']);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("RTS" . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}


	// rpenyertaan saham

	public function penyertaan_saham($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$bank = $this->input->get('bank');
		$last_year = $this->db->select('tahun')->from('tr_penyertaan_saham')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();
		
		$year = ($year != '') ? $year : $last_year->tahun;

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_penyertaan_saham(); // untuk menu pilihan tahun
		$data['arrMonth'] = data_month();

		$data['data_bidang'] = $this->keuanganhaji_model->get_bidang_penyertaan_saham($year);
		$data['all_data'] = $this->keuanganhaji_model->get_penyertaan_saham($year);

		$data['selected_year'] = $year;
		$data['view'] = 'visitor/keuanganhaji/penyertaan_saham';	
		$this->load->view('admin/layout', $data);
	}

	

	public function export_penyertaan_saham($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'penyertaan_saham_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$all_data = $this->keuanganhaji_model->get_penyertaan_saham($tahun, 'null');
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Penyertaan Saham Tahun " . $tahun)
			->setSubject("Penyertaan Saham Tahun " . $tahun)
			->setDescription("Penyertaan Saham Tahun " . $tahun)
			->setKeywords("Penyertaan Saham");



		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Penyertaan Saham Tahun " . $tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(dalam rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:N3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'BPS BPIH');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($all_data) + 4;
		foreach ($all_data as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->bidang);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->januari);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->februari);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->maret);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->april);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->mei);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element->juni);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element->juli);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element->agustus);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element->september);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element->oktober);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element->november);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element->desember);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Penyertaan Saham " . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	// investasi langsung

	public function investasi_langsung($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$bank = $this->input->get('bank');
		$last_year = $this->db->select('tahun')->from('tr_investasi_langsung')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		if ($last_year) {
			$year = ($year != '') ? $year : $last_year->tahun;
		}

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_investasi_langsung(); // untuk menu pilihan tahun
		$data['all_data'] = $this->keuanganhaji_model->get_investasi_langsung($year);

		$data['selected_year'] = $year;

		$data['view'] = 'visitor/keuanganhaji/investasi_langsung';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_investasi_langsung($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'investasi_langsung_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$all_data = $this->keuanganhaji_model->get_investasi_langsung($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Investasi Langsung Tahun " . $tahun)
			->setSubject("Investasi Langsung Tahun " . $tahun)
			->setDescription("Investasi Langsung Tahun " . $tahun)
			->setKeywords("Penyertaan Saham");



		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Investasi Langsung Tahun " . $tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(dalam rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:N3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'BPS BPIH');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($all_data) + 4;
		foreach ($all_data as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->bidang);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->januari);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->februari);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->maret);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->april);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->mei);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element->juni);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element->juli);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element->agustus);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element->september);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element->oktober);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element->november);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element->desember);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Investasi Langsung Th " . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	// investasi lainnya

	public function investasi_lainnya($tahun = 0)
	{
		$year = $this->input->get('tahun');
		$bank = $this->input->get('bank');
		$last_year = $this->db->select('tahun')->from('tr_investasi_lainnya')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		if ($last_year) {
			$year = ($year != '') ? $year : $last_year->tahun;
		}

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_investasi_lainnya(); // untuk menu pilihan tahun
		$data['all_data'] = $this->keuanganhaji_model->get_investasi_lainnya($year);

		$data['selected_year'] = $year;
		$data['view'] = 'visitor/keuanganhaji/investasi_lainnya';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_investasi_lainnya($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'investasi_lainnya_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$all_data = $this->keuanganhaji_model->get_investasi_lainnya($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Investasi Lainnya Tahun " . $tahun)
			->setSubject("Investasi Lainnya Tahun " . $tahun)
			->setDescription("Investasi Lainnya Tahun " . $tahun)
			->setKeywords("Investasi Lainnya");



		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Investasi Lainnya Tahun " . $tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(dalam rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:N3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'BPS BPIH');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($all_data) + 4;
		foreach ($all_data as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->bidang);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->januari);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->februari);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->maret);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->april);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->mei);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element->juni);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element->juli);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element->agustus);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element->september);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element->oktober);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element->november);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element->desember);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Investasi Lainnya Th " . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	

	// investasi lainnya

	public function emas($tahun = 0)
	{		

		$year = $this->input->get('tahun');
		$bank = $this->input->get('bank');
		$last_year = $this->db->select('tahun')->from('emas')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		if ($last_year) {
			$year = ($year != '') ? $year : $last_year->tahun;
		}

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_emas(); // untuk menu pilihan tahun
		$data['emas'] = $this->keuanganhaji_model->get_emas($year);

		$data['selected_year'] = $year;

		$data['view'] = 'visitor/keuanganhaji/emas';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_emas($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'emas_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_emas($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Emas Tahun " . $tahun)
			->setSubject("Emas Tahun " . $tahun)
			->setDescription("Emas Tahun " . $tahun)
			->setKeywords("Investasi Lainnya");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Emas Tahun " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(dalam rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:N3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1


		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'Nama Perusahaan');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['bps_bpih']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['januari']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['februari']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['maret']);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['april']);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['mei']);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['juni']);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['juli']);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['agustus']);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['september']);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['oktober']);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['november']);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['desember']);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);


			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Emas Thn " . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	// investasi lainnya

	public function akumulasi_kontribusi_bpsbpih($tahun = 0)
	{

		$tahun = ($tahun != '') ? $tahun : date('Y');

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_akumulasi_kontribusi_bpsbpih(); // untuk menu pilihan tahun
		$data['akumulasi_kontribusi_bpsbpih'] = $this->keuanganhaji_model->get_akumulasi_kontribusi_bpsbpih($tahun);

		$data['view'] = 'visitor/keuanganhaji/akumulasi_kontribusi_bpsbpih';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_akumulasi_kontribusi_bpsbpih($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'akumulasi_kontribusi_bpsbpih_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_akumulasi_kontribusi_bpsbpih($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Akumulasi Kontribusi BPS BPIH Tahun " . $tahun)
			->setSubject("Akumulasi Kontribusi BPS BPIH Tahun " . $tahun)
			->setDescription("Akumulasi Kontribusi BPS BPIH Tahun " . $tahun)
			->setKeywords("Investasi Lainnya");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Akumulasi Kontribusi BPS BPIH Tahun " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1


		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'BPS BPIH');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['bps_bpih']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['januari']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['februari']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['maret']);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['april']);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['mei']);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['juni']);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['juli']);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['agustus']);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['september']);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['oktober']);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['november']);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['desember']);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Kontribusi BPS BPIH Thn " . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}


	// posisi penempatan produk

	public function posisi_penempatan_produk($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$bank = $this->input->get('bank');
		$last_year = $this->db->select('tahun')->from('posisi_penempatan_produk')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();
		
		$year = ($year != '') ? $year : $last_year->tahun;

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_posisi_penempatan_produk(); // untuk menu pilihan tahun
		$data['posisi_penempatan_produk'] = $this->keuanganhaji_model->get_posisi_penempatan_produk($year);

		$data['selected_year'] = $year;
		$data['view'] = 'visitor/keuanganhaji/posisi_penempatan_produk';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_posisi_penempatan_produk($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'posisi_penempatan_produk_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_posisi_penempatan_produk($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Posisi Penempatan Produk Tahun " . $tahun)
			->setSubject("Posisi Penempatan Produk Tahun " . $tahun)
			->setDescription("Posisi Penempatan Produk Tahun " . $tahun)
			->setKeywords("Investasi Lainnya");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Posisi Penempatan Produk Tahun " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:F2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('F3', "(dalam rupiah)");
		$excel->getActiveSheet()->getStyle('F3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('F3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('F3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT); // Set text center untuk kolom A1


		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'Bulan');
		$excel->getActiveSheet()->SetCellValue('C4', 'Giro');
		$excel->getActiveSheet()->SetCellValue('D4', 'Tabungan');
		$excel->getActiveSheet()->SetCellValue('E4', 'Deposito');
		$excel->getActiveSheet()->SetCellValue('F4', 'Jumlah');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, konversiBulanAngkaKeNama($element['bulan']));
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['giro']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['tabungan']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['deposito']);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['jumlah']);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		/* 	for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			    $excel->getActiveSheet()->getStyle($i.$last_row)->applyFromArray($style_td_bold);
			} */
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Posisi Penempatan Thn " . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	public function porsi_investasi($tahun = 0)
	{
		$tahun = ($tahun != '') ? $tahun : date('Y');

		$data['thn'] = $tahun;
		$data['tahun'] = $this->alokasi_model->get_tahun_alokasi_investasi();
		$data['alokasi_investasi'] = $this->alokasi_model->get_alokasi_investasi($tahun);
		$data['view'] = 'Alokasi/index';
		$this->load->view('admin/layout', $data);
	}

	// SEBARAN DANA HAJI
	public function penempatan_dana_haji($tahun = 0)
	{

		$tahun = ($tahun != '') ? $tahun : date('Y');

		$data['thn'] = $tahun;
		$data['tahun'] = $this->keuanganhaji_model->get_tahun_penempatan_dana_haji(); // untuk menu pilihan tahun
		$data['penempatan_dana_haji'] = $this->keuanganhaji_model->get_penempatan_dana_haji($tahun);

		$data['view'] = 'keuanganhaji/penempatan_dana_haji';
		$this->load->view('admin/layout', $data);
	}

	

	public function export_penempatan_dana_haji($tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'penempatan_dana_haji_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->keuanganhaji_model->get_penempatan_dana_haji($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Posisi Sebaran Dana Haji Tahun " . $tahun)
			->setSubject("Posisi Sebaran Dana Haji Tahun " . $tahun)
			->setDescription("Posisi Sebaran Dana Haji Tahun " . $tahun)
			->setKeywords("Posisi Sebaran Dana Haji");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Posisi Sebaran Dana Haji Tahun " . $tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'BPS BPIH');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['bps_bpih']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['januari']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['februari']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['maret']);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['april']);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['mei']);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['juni']);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['juli']);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['agustus']);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['september']);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['oktober']);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['november']);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['desember']);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Posisi Sebaran Dana Haji" . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}
} //class
