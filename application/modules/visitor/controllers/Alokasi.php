<?php defined('BASEPATH') OR exit('No direct script access allowed');
	use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\IOFactory; 
	use PhpOffice\PhpSpreadsheet\Style\Alignment; 
	class Alokasi extends MY_Controller {
	//private $filename = "import_data";

	public function __construct(){
		parent::__construct();
		$this->load->library('excel');	
		$this->load->model('alokasi/alokasi_model', 'alokasi_model');
	}

	public function index( $tahun=0 ){;
		$year = $this->input->get('tahun');

		$last_year = $this->db->select('tahun')->from('tr_alokasi_investasi')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();	
		$year = ($year != '') ? $year : $last_year->tahun;

		$data['thn'] = $year;
		$data['tahun'] = $this->alokasi_model->get_tahun_alokasi_investasi();
		$data['all_data'] = $this->alokasi_model->get_alokasi_investasi($year);

		$data['selected_year'] = $year;
		$data['view'] = 'alokasi/index';
		$this->load->view('admin/layout', $data);
	}

	public function detail_alokasi_investasi($id=0){

		$data['alokasi_investasi'] = $this->alokasi_model->get_detail_alokasi_investasi($id);
		$data['view'] = 'alokasi/detail_alokasi_investasi';
		$this->load->view('admin/layout', $data);
	}

	
	public function export_alokasi_investasi($tahun){

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name
		
        $fileName = 'alokasi_investasi_'.$tahun.'-('. date('d-m-Y H-i-s', time()) .').xlsx';  

        $all_data = $this->alokasi_model->get_alokasi_investasi($tahun);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
         		->setLastModifiedBy('BPKH')
         		->setTitle(" Alokasi Investasi BPKH Tahun " .$tahun)
         		->setSubject(" Alokasi Investasi BPKH Tahun " .$tahun)
         		->setDescription(" Alokasi Investasi BPKH Tahun " .$tahun)
         		->setKeywords("Posisi Sebaran Dana Haji");            

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', " Alokasi Investasi BPKH Tahun " .$tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:N1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A2:N2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(dalam rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:N3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0);
		// set Header
		$excel->getActiveSheet()->SetCellValue('A4', 'No.');
		$excel->getActiveSheet()->SetCellValue('B4', 'BPS BPIH');
		$excel->getActiveSheet()->SetCellValue('C4', 'Januari');
		$excel->getActiveSheet()->SetCellValue('D4', 'Februari');
		$excel->getActiveSheet()->SetCellValue('E4', 'Maret');
		$excel->getActiveSheet()->SetCellValue('F4', 'April');
		$excel->getActiveSheet()->SetCellValue('G4', 'Mei');
		$excel->getActiveSheet()->SetCellValue('H4', 'Juni');
		$excel->getActiveSheet()->SetCellValue('I4', 'Juli');
		$excel->getActiveSheet()->SetCellValue('J4', 'Agustus');
		$excel->getActiveSheet()->SetCellValue('K4', 'September');
		$excel->getActiveSheet()->SetCellValue('L4', 'Oktober');
		$excel->getActiveSheet()->SetCellValue('M4', 'November');
		$excel->getActiveSheet()->SetCellValue('N4', 'Desember');

		//no 
		$no = 1;
		// set Row
		$rowCount = 5;
		$last_row = count($all_data) + 4;
		foreach ($all_data as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->bidang);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->januari);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->februari);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->maret);
			$excel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->april);
			$excel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->mei);
			$excel->getActiveSheet()->SetCellValue('H' . $rowCount, $element->juni);
			$excel->getActiveSheet()->SetCellValue('I' . $rowCount, $element->juli);
			$excel->getActiveSheet()->SetCellValue('J' . $rowCount, $element->agustus);
			$excel->getActiveSheet()->SetCellValue('K' . $rowCount, $element->september);
			$excel->getActiveSheet()->SetCellValue('L' . $rowCount, $element->oktober);
			$excel->getActiveSheet()->SetCellValue('M' . $rowCount, $element->november);
			$excel->getActiveSheet()->SetCellValue('N' . $rowCount, $element->desember);

			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}


		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		$excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(TRUE); 
		$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(TRUE); 
		$excel->getActiveSheet()->getStyle('A12')->getFont()->setBold(TRUE); 
       
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
		    $excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

        // Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Posisi Sebaran Dana Haji" . $tahun);
		$excel->setActiveSheetIndex(0);

		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
   		// download file
        header("Content-Type: application/vnd.ms-excel");
        redirect('./uploads/excel/'.$fileName);

	}

} //class

