<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/dashboard_model', 'dashboard_model');

	}

	public function index($tahun=0)
	{
		$user_id = $this->session->userdata('user_id');
		$data['year'] = ($tahun == 0) ? date('Y') : $tahun;
		$is_exist = $this->db->from('dashboard')->where('tahun', $data['year'])->get()->row();

		if (!$is_exist) {
			$arrData = [
				'penempatan' => 0,
				'setoran_awal' => 0,
				'setoran_awal_per' => 0,
				'setoran_lunas' => 0,
				'setoran_lunas_per' => 0,
				'nilai_manfaat' => 0,
				'nilai_manfaat_per' => 0,
				'dau' => 0,
				'dau_per' => 0,
				'setoran_awal_inv' => 0,
				'setoran_awal_inv_per' => 0,
				'dau_inv' => 0,
				'dau_inv_per' => 0,
				'investasi' => 0,
				'akumulasi_dau' => 0,
				'penempatan_dau' => 0,
				'penempatan_dau_per' => 0,
				'investasi_dau' => 0,
				'investasi_dau_per' => 0,
				'total' => 0,
				'tahun' => $data['year'],
				'periode' => '31 Desember',
			];

			$this->db->insert('dashboard', $arrData);
		}

		$query = $this->db->from('dashboard')->where('tahun', $data['year'])->get();
		$data['result'] = $query->row_array();

		$data['log'] = $this->db->from('trx_log')->where('user_id', $user_id)->limit('5')->order_by('id_log', 'desc')->get()->result();

		$sql_total_penempatan =  $this->db->select("januari, februari, maret, april, mei, juni, juli, agustus, september, oktober, november, desember")
											->from('sebaran_dana_haji2')
											->where('tahun', $data['year'])
											->where('bps_bpih', 'total')
											->get()
											->row();

		$sql_total_investasi = $this->db->select('nilai, bulan')
											->from('sbssn_rupiah2')
											->where('tahun', $data['year'])
											->where_not_in('instrumen', ['total'])
											->get()
											->result();

		$arrMonth = $this->db->from('md_month')->order_by('id_month', 'asc')->get()->result();

		$total_dana_haji = 0;
		foreach ($arrMonth as $key) {
				$get_dana_haji = $this->db->select($key->name .' as bulan')
										->from('sebaran_dana_haji2')
										->where('tahun', $data['year'])
										->where('bps_bpih', 'total')
										->get()
										->row();

				if ($get_dana_haji) {
					if (strlen($get_dana_haji->bulan) != 0) {
						$total_dana_haji = (int) str_replace(',', '', $get_dana_haji->bulan);
					}
				}
		}


		$sql_penempatan_produk = $this->db->select('jumlah')
										->from('posisi_penempatan_produk')
										->where('tahun', $data['year'])
										->order_by('bulan', 'desc')
										->limit(1)
										->get()
										->row();
		if (!$sql_penempatan_produk) {
			$penempatan_produk = 0;
		}
		else {
			$penempatan_produk = $sql_penempatan_produk->jumlah;
		}
		$total_penempatan_produk = (int) str_replace(',', '', $penempatan_produk);

		$total_data_penempatan = $total_dana_haji;
		// $total_data_penempatan = sum_data_by_month($sql_total_penempatan);
		
		$data['total_penempatan'] = formatCurrency(number_format($total_data_penempatan));
		$data['chart_penempatan'] = ($sql_total_penempatan) ? convert_month_to_array($sql_total_penempatan) : NULL;

		/*--------------------------CALCULATE INVESTASI--------------------------*/
		$total_investasi = 0;
		$total_Januari = 0;
        $total_Februari = 0;
        $total_Maret = 0;
        $total_April = 0;
        $total_Mei = 0;
        $total_Juni = 0;
        $total_Juli = 0;
        $total_Agustus = 0;
        $total_September = 0;
        $total_Oktober = 0;
        $total_November = 0;
        $total_Desember = 0;

		foreach ($sql_total_investasi as $rowInvest) {
			(float) $format_nilai = convert_str($rowInvest->nilai);
			$month = konversiBulanAngkaKeNama($rowInvest->bulan);
			$months = $rowInvest->bulan;
			
			$total_investasi += $format_nilai;
			
			if ($months == $rowInvest->bulan) {
				${'total_' . $month} += $format_nilai;
			}
		}

		$arrInvest = [
			['year' => 'Jan','income' => $total_Januari],
			['year' => 'Feb','income' => $total_Februari],
			['year' => 'Mar','income' => $total_Maret],
			['year' => 'Apr','income' => $total_April],
			['year' => 'Mei','income' => $total_Mei],
			['year' => 'Jun','income' => $total_Juni],
			['year' => 'Jul','income' => $total_Juli],
			['year' => 'Aug','income' => $total_Agustus],
			['year' => 'Sep','income' => $total_September],
			['year' => 'Okt','income' => $total_Oktober],
			['year' => 'Nov','income' => $total_November],
			['year' => 'Des','income' => $total_Desember],
		];

		// $total_investasi = formatCurrency(number_format($total_investasi, 2));
		// $total_investasi = $this->db->select('per_sumber_kas_haji')
		// 						->from('alokasi_investasi')
		// 						->where('tahun', $data['year'])
		// 						->order_by('bulan', 'desc')
		// 						->limit(1)
		// 						->get()
		// 						->row();

		$total_investasi = 0;
		foreach ($arrMonth as $key) {
				$get_alokasi_investasi = $this->db->select($key->name .' as bulan')
										->from('tr_alokasi_investasi')
										->where('tahun', $data['year'])
										->where('bidang', 'Per Sumber Kas')
										->get()
										->row();
				
				if ($get_alokasi_investasi) {
					if (strlen($get_alokasi_investasi->bulan) != 0) {
						$total_investasi = (int) str_replace(',', '', $get_alokasi_investasi->bulan);
					}
				}
		}

		if (!$total_investasi) {
			$total_investasi = 0;
		}
		else {
			$total_investasi = (int) str_replace(',', '', $total_investasi);
		}
		/*-------------------sementara untuk golive-------------------*/

		if ($data['year'] > 2019) {
			// code...
			$total_investasi = $total_investasi . '000000000';
			$total_investasi = (int)$total_investasi;
		// dd($total_investasi);
		}
		/*-------------------sementara untuk golive-------------------*/
		$total_investasi_format = number_format($total_investasi);
		$data['total_investasi'] = formatCurrency($total_investasi_format);
		$data['chart_investasi'] = (count($sql_total_investasi) > 0) ? json_encode($arrInvest) : NULL;


		$total_dana = $total_investasi + $total_data_penempatan;
		$data['total_dana'] = formatCurrency(number_format($total_dana, 2));
		/*--------------------------CALCULATE INVESTASI--------------------------*/
		
		$data['user_active'] = $this->db->from('ci_users')->where('last_activity > DATE_SUB(NOW(),INTERVAL 20 MINUTE)')->get()->result();

		$data['title'] = 'Dashboard Admin';
		$data['view'] = 'admin/dashboard/dashboard';
		
		$this->load->view('admin/layout', $data);
	}


	public function modal_log(){
		$user_id = $this->session->userdata('user_id');
		$data['log'] = $this->db->from('trx_log')->where('user_id', $user_id)->order_by('id_log', 'desc')->get()->result();
		$this->load->view('admin/dashboard/modal-log', $data);
	}
}
