<!DOCTYPE html>
<html lang="en">
    <head>
          <title><?=isset($title)?$title:'Login - SI BPKH' ?></title>
          <!-- Tell the browser to be responsive to screen width -->
          <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
          <!-- Bootstrap 3.3.6 -->
          <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
          <!-- Theme style -->
          <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/AdminLTE.min.css">
           <!-- Custom CSS -->
          <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/style.css">
           <!-- jQuery 2.2.3 -->
          <script src="<?= base_url() ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
          <link rel="stylesheet" href="<?= base_url() ?>public/css/sweetalert2.min.css">        
       
    </head>
    <body id="login-form">
        <div class="container">
            <div class="row">
                
                <div class="col-md-4 col-md-offset-4 text-center">
                    <div class="login-title">
                         <h3><img width="200" height="" src="<?= base_url() ?>public/dist/img/logo-bpkh-w.png" alt="SIKIMAS BMT"></h3>
                    </div>
                    <?php if(isset($msg) || validation_errors() !== ''): ?>
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                        <?= validation_errors();?>
                        <?= isset($msg)? $msg: ''; ?>
                    </div>
                    <?php endif; ?>
                    <?php if($this->session->flashdata('warning')): ?>
                          <div class="alert alert-warning">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                          <?=$this->session->flashdata('warning')?>
                        </div>
                    <?php endif; ?>
                    <?php if($this->session->flashdata('success')): ?>
                          <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?=$this->session->flashdata('success')?>
                          </div>
                    <?php endif; ?>
                    <div class="form-box">
                        <div class="caption">
                            <h3></h3>
                        </div>
                        <form action="<?php echo base_url('auth/register_submit') ?>" method="POST" class="login-form" id="form-register" >
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                            <div class="input-group">
                                <input type="text" name="username" id="usernames" class="form-control" placeholder="Username" autocomplete="off" required>
                                <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First Name" autocomplete="off" required>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Email" autocomplete="off" required>
                                <input type="password" name="password" id="passwords" class="form-control" placeholder="Password" autocomplete="off" required onchange="password_cek(this.id, 'Password')">
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm" autocomplete="off" required onchange="password_cek(this.id, 'Password Confirmation')">

                                <select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                                
                                <button type="button" class="form-control btn-success" style="margin-top:1rem;" onclick="register_validation()">Submit</button>
                                <a href="<?php echo base_url('auth/login') ?>" class="form-control btn-warning" style="margin-top: 1rem;">Login</a>
                                <button type="submit" class="hide" id="btn-register-submit"></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url() ?>public/dist/js/app.min.js"></script>
    <script src="<?= base_url() ?>public/js/sweetalert2.min.js"></script>
    <script type="text/javascript">
        function show_form(id, target){
            $('#'+id).hide();
            $('#'+target).show();
        }

        function register_validation(){
            var username = $('#usernames').val();
            var email = $('#email').val();
            var password = $('#passwords').val();
            var confirm_password = $('#confirm_password').val();

            if (password != confirm_password) {
                Swal.fire('Oops!', 'Password Confirmation not match!','error');
            }
            else {
                $('#btn-register-submit').click();
            }
        }

        function password_cek(str, type){

            if (str.length < 8) {
                return [
                    $('#info_cek').text(type + ' minimal 8 karakter')];
              
            } else if (str.length > 32) {
                return [
                    Swal.fire('', type + ' maksimal 32 karakter', 'error')];
            } else if (str.search(/\d/) == -1) {
                return [
                    Swal.fire('', type + ' harus mengandung angka', 'error')];
            } else if (str.search(/[a-z]/) == -1) {
                return [
                    Swal.fire('', type + ' harus mengandung huruf kecil', 'error')];
            } else if (str.search(/[A-Z]/) == -1) {
                return [
                    Swal.fire('', type + ' harus mengandung huruf kapital', 'error')];
            }else if (str.search(/[\!\@\#\$\%\^\&\*\(\)\-\=\_\+\.\,\<\>\;\:\/\?]/) == -1) {
                return [
                    Swal.fire('', type + ' harus mengandung spesial karakter', 'error')];
            }
            return [
                $('#password_input').removeClass('has-danger'),
                $('#info_cek').empty()];
        }
    </script>
</html>