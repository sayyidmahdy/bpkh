<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<script
		src="https://kit.fontawesome.com/64d58efce2.js"
		crossorigin="anonymous"
		></script>
		<link rel="stylesheet" href="<?php echo base_url('public/login/style.css') ?>" />
		<link rel="stylesheet" href="<?= base_url() ?>public/css/sweetalert2.min.css">   
		<title>BPKH - Statistik Haji</title>
		<style type="text/css">
			.alert {
			  padding: 20px;
			  background-color: #f44336;
			  color: white;
			  opacity: 1;
			  transition: opacity 0.6s;
			  margin-bottom: 15px;
			}

			.alert.success {background-color: #04AA6D;}
			.alert.info {background-color: #2196F3;}
			.alert.warning {background-color: #ff9800;}

			.closebtn {
			  margin-left: 15px;
			  color: white;
			  font-weight: bold;
			  float: right;
			  font-size: 22px;
			  line-height: 20px;
			  cursor: pointer;
			  transition: 0.3s;
			}

			.closebtn:hover {
			  color: black;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="forms-container">
				<div class="signin-signup">
					<form action="<?php echo base_url('auth/login') ?>" method="POST" class="sign-in-form">
						<?php if(isset($msg) || validation_errors() !== ''): ?>
	                    <div class="alert warning">
						  <span class="closebtn">&times;</span>  
						  <?= validation_errors();?>
	                        <?= isset($msg)? $msg: ''; ?>
						</div>
	                    <?php endif; ?>
	                    <?php if($this->session->flashdata('warning')): ?>
	                        <div class="alert warning">
							  <span class="closebtn">&times;</span>  
							  <?=$this->session->flashdata('warning')?>
							</div>
	                    <?php endif; ?>
	                    <?php if($this->session->flashdata('success')): ?>
	                    	<div class="alert success">
							  <span class="closebtn">&times;</span>  
							  <?=$this->session->flashdata('success')?>
							</div>
	                    <?php endif; ?>
						
						<!-- <img src="https://bpkh.go.id/cdn/uploads/2020/06/logo-bpkh-iso.png" class="logo" alt="" /> -->
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

						<h2 class="title">Sign in</h2>
						<div class="input-field">
							<i class="fas fa-user"></i>
							<input type="text" placeholder="Username" name="username" />
						</div>
						<div class="input-field">
							<i class="fas fa-lock"></i>
							<input type="password" placeholder="Password" name="password" />
						</div>
						<!-- <input type="submit" value="Login" class="btn solid" /> -->
						<input type="submit" name="submit" id="submit" class="btn solid" value="Login">
						<!-- <p class="social-text">Or Sign in with social platforms</p>
						<div class="social-media">
							<a href="#" class="social-icon">
								<i class="fab fa-facebook-f"></i>
							</a>
							<a href="#" class="social-icon">
								<i class="fab fa-twitter"></i>
							</a>
							<a href="#" class="social-icon">
								<i class="fab fa-google"></i>
							</a>
							<a href="#" class="social-icon">
								<i class="fab fa-linkedin-in"></i>
							</a>
						</div> -->
					</form>
					<form action="<?php echo base_url('auth/register_submit') ?>" method="POST" class="sign-up-form" id="form-register">
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
						<!-- <img src="https://bpkh.go.id/cdn/uploads/2020/06/logo-bpkh-iso.png" class="logo mt-5" alt="" /> -->
						<h2 class="title">Sign up</h2>
						<div class="input-field">
							<i class="fas fa-user"></i>
							<input type="text" placeholder="Username" name="username" id="usernames"/>
						</div>
						<div class="input-field">
							<i class="fas fa-user"></i>
							<input type="text" placeholder="First Name" name="firstname" id="firstname"/>
						</div>
						<div class="input-field">
							<i class="fas fa-envelope"></i>
							<input type="email" placeholder="Email" name="email" id="email" />
						</div>
						<div class="input-field">
							<i class="fas fa-lock"></i>
							<input type="password" placeholder="Password" name="password" id="passwords" placeholder="Password" autocomplete="off" required />
						</div>
						<div class="input-field">
							<i class="fas fa-lock"></i>
							<input type="password" placeholder="Password Confirmation" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm" autocomplete="off" required />
						</div>
						<button type="button" class="btn" style="margin-top:1rem;" onclick="register_validation()">Sign up</button>
						<button type="submit" style="display:none;" id="btn-register-submit"></button>
						<!-- <p class="social-text">Or Sign up with social platforms</p>
						<div class="social-media">
							<a href="#" class="social-icon">
								<i class="fab fa-facebook-f"></i>
							</a>
							<a href="#" class="social-icon">
								<i class="fab fa-twitter"></i>
							</a>
							<a href="#" class="social-icon">
								<i class="fab fa-google"></i>
							</a>
							<a href="#" class="social-icon">
								<i class="fab fa-linkedin-in"></i>
							</a>
						</div> -->
					</form>
				</div>
			</div>
			<div class="panels-container">
				<div class="panel left-panel">
					<div class="content">
						<img src="<?php echo base_url('public/dist/img/logo-bpkh-w.png') ?>" class="logo mb-3">
						<h3>New here ?</h3>
						<p>
							Please register an account to be able to enter the application
						</p>
						<button type="button" class="btn transparent" id="sign-up-btn">
						Sign up
						</button>
					</div>
					<!-- <img src="<?php echo base_url('public/login/img/Eva.png') ?>" class="image" alt="" style="width: 35%;" /> -->
					<img src="<?php echo base_url('public/login/img/mina.png') ?>" class="image" alt="" style="width: 35%;" />
					<!-- <img src="<?php echo base_url('public/login/img/register.svg') ?>" class="image" alt="" /> -->
				</div>
				<div class="panel right-panel">
					<div class="content">
						<img src="<?php echo base_url('public/dist/img/logo-bpkh-w.png') ?>" class="logo mb-3">
						<h3>One of us ?</h3>
						<p>
							Please login to be able to access the application
						</p>
						<button class="btn transparent" id="sign-in-btn">
						Sign in
						</button>
					</div>
					<!-- <img src="<?php echo base_url('public/login/img/log.svg') ?>" class="image" alt="" /> -->
					<img src="<?php echo base_url('public/login/img/Eva.png') ?>" class="image" alt="" style="width: 35%;" />
				</div>
			</div>
		</div>
		<script src="<?php echo base_url('public/login/app.js') ?>"></script>
	</body>
</html>
<!-- Bootstrap 3.3.6 -->
    <script src="<?= base_url() ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url() ?>public/dist/js/app.min.js"></script>
    <script src="<?= base_url() ?>public/js/sweetalert2.min.js"></script>
    <script src="<?= base_url() ?>public/js/numeral.min.js"></script>
    <script type="text/javascript">
        function show_form(id, target){
            $('#'+id).hide();
            $('#'+target).show();
        }

        function register_validation(){
            var username = $('#usernames').val();
            var email = $('#email').val();
            var password = $('#passwords').val();
            var confirm_password = $('#confirm_password').val();

            if (password != confirm_password) {
                Swal.fire('Oops!', 'Password Confirmation not match!','error');
            }
            else {
                $('#btn-register-submit').click();
            }
        }

        function password_cek(str, type){

            if (str.length < 8) {
                return [
                    $('#info_cek').text(type + ' minimal 8 karakter')];
              
            } else if (str.length > 32) {
                return [
                    Swal.fire('', type + ' maksimal 32 karakter', 'error')];
            } else if (str.search(/\d/) == -1) {
                return [
                    Swal.fire('', type + ' harus mengandung angka', 'error')];
            } else if (str.search(/[a-z]/) == -1) {
                return [
                    Swal.fire('', type + ' harus mengandung huruf kecil', 'error')];
            } else if (str.search(/[A-Z]/) == -1) {
                return [
                    Swal.fire('', type + ' harus mengandung huruf kapital', 'error')];
            }else if (str.search(/[\!\@\#\$\%\^\&\*\(\)\-\=\_\+\.\,\<\>\;\:\/\?]/) == -1) {
                return [
                    Swal.fire('', type + ' harus mengandung spesial karakter', 'error')];
            }
            return [
                $('#password_input').removeClass('has-danger'),
                $('#info_cek').empty()];
        }

        // $(function(){
        // 	var data = numeral('960.93').format('0,0.[00]');
        // 	alert(data);
        // });
    </script>
    <script>
var close = document.getElementsByClassName("closebtn");
var i;

for (i = 0; i < close.length; i++) {
  close[i].onclick = function(){
    var div = this.parentElement;
    div.style.opacity = "0";
    setTimeout(function(){ div.style.display = "none"; }, 600);
  }
}
</script>