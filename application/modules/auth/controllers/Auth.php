<?php defined('BASEPATH') OR exit('No direct script access allowed');
	class Auth extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library('mailer');
			$this->load->model('auth_model', 'auth_model');
			$this->load->helper('email_helper');
		}
		//--------------------------------------------------------------
		public function index(){
			if($this->session->has_userdata('is_login')) {
				
				if($this->session->userdata('is_admin') == 1) {
					redirect('admin/dashboard');
				} else {
					redirect('visitor/dashboard');	
				}

			} else{
				redirect('auth/login');
			}
		}
		//--------------------------------------------------------------
		public function login(){

			if($this->input->post('submit')){
				$this->form_validation->set_rules('username', 'Username', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$this->load->view('auth/index');
				}
				else {
					$data = array(
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password')
					);

					$is_user_bpkh = strpos($this->input->post('username'), '@bpkh');

					if ($is_user_bpkh) {
						// $ws_response = 'success';
						$ws_response = $this->login_web_service($this->input->post('username'), $this->input->post('password'));
						
						if ($ws_response == 'success') {
							$username = $this->input->post('username');
							$data_bpkh = [
								'username' => $username,
								'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
								'firstname' => $username,
								'jenis_kelamin' => '',
								'mobile_no' => '',
								'role' => '2',
								'email' => $username,
							];

							
							$is_exist = $this->db->from('ci_users')->where('username', $username)->get()->row();

							if ($is_exist) {
								$user_id = $is_exist->id;
							}
							else {
								$setup_menu = $this->db->from('menu_sub')->where_not_in('menu_id', [21, 22])->get()->result();
								$user_id = $this->auth_model->save_user($data_bpkh);
								
								foreach ($setup_menu as $rowMenu) {
									$dataMenu['user_id'] = $user_id;
									$dataMenu['sub_menu_id'] = $rowMenu->id_sub;

									$this->db->insert('menu_user', $dataMenu);
								}
							}
							
							$result = $this->db->from('ci_users')->where('id', $user_id)->get()->row();

							$user_data = array(
								'user_id' => $result->id,
								'username' => $result->username,
								'role' => $result->role,
								'is_login' => TRUE,
								'modul' => $result->modul,										
							);
							
							$this->session->set_userdata($user_data);

							if($this->session->userdata('role')==1) {
								redirect(base_url('admin/dashboard'), 'refresh'); 
							}
							else if($this->session->userdata('role')==2) {
								redirect(base_url('visitor/dashboard'), 'refresh');
							}
						}
						else {
							$this->session->set_flashdata('warning', 'Login Failed: Please check your BPKH email or password');
							redirect(base_url('auth/login'));
							exit;
						}
					}

					$result = $this->auth_model->login($data);

					if($result){
						if($result['is_verify'] == 0){
				    		$this->session->set_flashdata('warning', 'Please verify your email address!');
							redirect(base_url('auth/login'));
							exit;
				    	}
				    	
						$user_data = array(
							'user_id' => $result['id'],
							'username' => $result['username'],
							'role' => $result['role'],
							'is_login' => TRUE,
							'modul' => $result['modul'],										
						);

						$this->session->set_userdata($user_data);
						if($this->session->userdata('role')==1) {
							redirect(base_url('admin/dashboard'), 'refresh'); 
						}
						else if($this->session->userdata('role')==2) {
							redirect(base_url('visitor/dashboard'), 'refresh');
						}							
					}
					else{
						$data['msg'] = 'Invalid Username or Password!';
						$this->load->view('auth/index', $data);
					}
				}
			}
			else{
				$this->load->view('auth/index');
			}
		}	

		

		//----------------------------------------------------------	
		public function verify(){
			$verification_id = $this->uri->segment(3);
			$result = $this->auth_model->email_verification($verification_id);
			if($result){
				$this->session->set_flashdata('success', 'Your email has been verified, you can now login.');	
				redirect(base_url('auth/login'));
			}
			else{
				$this->session->set_flashdata('success', 'The url is either invalid or you already have activated your account.');	
				redirect(base_url('auth/login'));
			}	
		}
		//--------------------------------------------------		
		public function forgot_password(){
			if($this->input->post('submit')){
				//checking server side validation
				$this->form_validation->set_rules('email', 'Email', 'valid_email|trim|required');
				if ($this->form_validation->run() === FALSE) {
					$this->load->view('auth/forget_password');
					return;
				}
				$email = $this->input->post('email');
				$response = $this->auth_model->check_user_mail($email);
				if($response){
					$rand_no = rand(0,1000);
					$pwd_reset_code = md5($rand_no.$response['id']);
					$this->auth_model->update_reset_code($pwd_reset_code, $response['id']);
					// --- sending email
					$name = $response['firstname'];
					$email = $response['email'];
					$reset_link = base_url('auth/reset_password/'.$pwd_reset_code);
					$body = $this->mailer->Tpl_PwdResetLink($name,$reset_link);

					
					$to = $email;
					$subject = 'Reset your password';
					$message =  $body ;
					$email = sendEmail($to, $subject, $message, $file = '' , $cc = '');
					
					if($email == "success"){
						$this->session->set_flashdata('success', 'We have sent instructions for resetting your password to your email');

						redirect(base_url('auth/forgot_password'));
					}
					else{
						$this->session->set_flashdata('error', 'There is the problem on your email');
						redirect(base_url('auth/forgot_password'));
					}
				}
				else{
					$this->session->set_flashdata('error', 'The Email that you provided are invalid');
					redirect(base_url('auth/forgot_password'));
				}
			}
			else{
				$data['title'] = 'Forget Password';
				$this->load->view('auth/forget_password',$data);	
			}
		}
		//----------------------------------------------------------------		
		public function reset_password($id=0){
			// check the activation code in database
			if($this->input->post('submit')){
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
				$this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required|matches[password]');

				if ($this->form_validation->run() == FALSE) {
					$result = false;
					$data['reset_code'] = $id;
					$data['title'] = 'Reseat Password';
					$this->load->view('auth/reset_password',$data);
				}   
				else{
					$new_password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
					$this->auth_model->reset_password($id, $new_password);
					$this->session->set_flashdata('success','New password has been Updated successfully.Please login below');
					redirect(base_url('auth/login'));
				}
			}
			else{
				$result = $this->auth_model->check_password_reset_code($id);
				if($result){
					$data['reset_code'] = $id;
					$data['title'] = 'Reseat Password';
					$this->load->view('auth/reset_password',$data);
				}
				else{
					$this->session->set_flashdata('error','Password Reset Code is either invalid or expired.');
					redirect(base_url('auth/forgot_password'));
				}
			}
		}
			
		public function logout(){
			$this->session->sess_destroy();
			redirect(base_url('auth/login'), 'refresh');
		}

		public function update_last_activity(){
			date_default_timezone_set('Asia/Jakarta');
			$user = $this->session->userdata('user_id');
			$data['last_activity'] = date('Y-m-d H:i:s');
			$this->db->where('id', $user)->update('ci_users', $data);

			echo 'success';
		}

		public function register(){
			$this->load->view('auth/register');
		}
		public function register_submit(){
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('confirm_password', 'confirm_password', 'trim|required');
			$this->form_validation->set_rules('firstname', 'firstname', 'trim|required');
			$this->form_validation->set_rules('email', 'email', 'trim|required');
			$data = [];


			if ($this->form_validation->run() == FALSE) {
				$this->load->view('auth/index', $data);
			}
			else {
				$username = $this->input->post('username');
				$firstname = $this->input->post('firstname');
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				$confirm_password = $this->input->post('confirm_password');

				$check_user = $this->db->from('ci_users')->where('username', $username)->get()->row();
				$check_email = $this->db->from('ci_users')->where('email', $email)->get()->row();

				if ($password != $confirm_password) {
					$this->session->set_flashdata('warning', 'Password and Confirm Password not match!');
					redirect(base_url('auth/login'));
				}
				else if ($check_user) {
					$this->session->set_flashdata('warning', 'Username already exists! please change to another username');
					redirect(base_url('auth/login'));
				}
				else if ($check_email) {
					$this->session->set_flashdata('warning', 'Email already exists! please change to another email');
					redirect(base_url('auth/login'));
				}
				else {
					$data = [
						'username' => $username,
						'firstname' => $firstname,
						'email' => $email,
						'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
						'role' => 2,
						'is_active' => 0,
						'is_verify' => 0,
						'is_admin' => 0,
					];

					$exec = $this->db->insert('ci_users', $data);
					
					if ($exec) {
						$insert_id = $this->db->insert_id();
						$this->send_mail($insert_id, $email);
						$this->session->set_flashdata('success', 'Account has been created successfully, please check your email for verification');
					}
					else {
						$this->session->set_flashdata('warning', 'Account registration failed');
					}

					redirect(base_url('auth/login'));
				}
				
			}
		}

		public function send_mail($id, $mail_to){
			$subject = 'Email Verification';
			$data['data'] = $this->db->from('ci_users')->where('id', $id)->get()->row();
			$message =  $this->load->view('auth/email', $data, TRUE);
			
			$email = sendEmail($mail_to, $subject, $message, $file = '' , $cc = '');
			
			return $email;
		}

		public function login_web_service($username, $password){
			$ldapconfig['host'] = 'ldap.bpkh.go.id';//CHANGE THIS TO THE CORRECT LDAP SERVER
			$ldapconfig['port'] = '389';
			$ldapconfig['basedn'] = 'dc=bpkh,dc=go,dc=id';//CHANGE THIS TO THE CORRECT BASE DN
			$ldapconfig['usersdn'] = 'ou=people';//CHANGE THIS TO THE CORRECT USER OU/CN
			$ds = ldap_connect($ldapconfig['host'], $ldapconfig['port']);
			
			ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
			ldap_set_option($ds, LDAP_OPT_NETWORK_TIMEOUT, 10);

			$email = explode('@', $username);
			$new_email = $email[0];

			$dn="uid=".$new_email.",".$ldapconfig['usersdn'].",".$ldapconfig['basedn'];
			if ($bind=ldap_bind($ds, $dn, $password)) {
				// echo("Login correct with username (".$_PO ST['username'].")");//REPLACE THIS WITH THE CORRECT FUNCTION LIKE A REDIRECT;
				$return = 'success';
			} else {
				$return = "Login Failed: Please check your username or password";
			}

			$data = [
				'email' => $username,
				'password' => password_hash($password, PASSWORD_BCRYPT),
				'response' => $return
			];

			$data_log['data'] = json_encode($data);

			$this->db->insert('sso_log', $data_log);
			return $return;
		}

		public function verify_account($id){
			$check_user = $this->db->from('ci_users')->where('id', $id)->where('is_verify', 1)->get()->result();

			if (count($check_user) == 0 ) {
				$data = ['is_verify' => 1];
				$update_status = $this->db->where('id', $id)->update('ci_users', $data);

				$setup_menu = $this->db->from('menu_sub')->where_not_in('menu_id', [21, 22])->get()->result();
				
				foreach ($setup_menu as $rowMenu) {
					$dataMenu['user_id'] = $id;
					$dataMenu['sub_menu_id'] = $rowMenu->id_sub;

					$this->db->insert('menu_user', $dataMenu);
				}
			}
			// $this->session->set_flashdata('success', 'Email has been verified, please wait until we activate your account');

			$this->session->set_flashdata('success', 'Email has been verified, please login to use this application');
			redirect(base_url('auth/login'));
		}
	}  // end class


?>