<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class Laporankeuangan extends Admin_Controller
{
	//private $filename = "import_data";

	public function __construct()
	{
		parent::__construct();
		$this->load->library('excel');

		$this->load->model('laporankeuangan_model', 'laporankeuangan_model');
	}

	public function index()
	{
		$data['view'] = 'index';
		$this->load->view('admin/layout', $data);
	}



	public function neraca($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$last_year = $this->db->select('tahun')->from('neraca2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		$year = ($year != '') ? $year : $last_year->tahun;

		$data['thn'] = $year;
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_neraca();
		$data['neraca'] = $this->laporankeuangan_model->get_neraca($year);
		
		$data['selected_year'] = $year;
		$data['view'] = 'neraca';
		$this->load->view('admin/layout', $data);
	}
	public function detail_neraca($bulan = 0, $tahun = 0)
	{
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_neraca();
		$data['neraca'] = $this->laporankeuangan_model->get_detail_neraca($bulan, $tahun);
		$data['view'] = 'detail_neraca';
		$this->load->view('admin/layout', $data);
	}

	public function tambah_neraca()
	{

		if (isset($_POST['submit'])) {

			$upload_path = './uploads/excel/laporankeuangan';

			if (!is_dir($upload_path)) {
				mkdir($upload_path, 0777, TRUE);
			}
			//$newName = "hrd-".date('Ymd-His');
			$config = array(
				'upload_path' => $upload_path,
				'allowed_types' => "xlsx",
				'overwrite' => FALSE,
			);

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$upload = $this->upload->data();


			if ($upload) { // Jika proses upload sukses			    	

				$excelreader = new Xlsx;
				$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $upload['file_name']); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				$data['sheet'] = $sheet;
				$data['file_excel'] = $upload['file_name'];


				$data['view'] = 'tambah_neraca';
				$this->load->view('admin/layout', $data);
			} else {

				echo "gagal";
				$data['view'] = 'tambah_neraca';
				$this->load->view('admin/layout', $data);
			}
		} else {

			$data['view'] = 'tambah_neraca';
			$this->load->view('admin/layout', $data);
		}
	}

	public function import_neraca($file_excel)
	{

		$excelreader = new Xlsx;
		$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $file_excel); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		$data = transposeData($sheet);

		$data2 = array();
		$arrDataLog = array();

		$numrow = 1;
		foreach ($sheet as $row) {

			if ($numrow > 1 && $row['A'] != '') {
				// Kita push (add) array data ke variabel data
				$month = konversi_bulan_ke_angka($data["B"][1]);

				$arrData = array(
					'bidang' => $row['A'],
					'target' => $row['B'],
					'bulan' => $month,
					'tahun' => $data["C"][1],
					'updated_at' => date('Y-m-d H:i:s'),
				);

				$query = $this->db->from('neraca2')->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["C"][1])->get()->result();

				if (count($query) > 0) {
					$exec = $this->db->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["C"][1])->update('neraca2', $arrData);
				}
				else {
					array_push($data2, $arrData);
				}
				array_push($arrDataLog, $arrData);
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		
		if (count($data2) > 0) {
			$exec = $this->laporankeuangan_model->insert_neraca($data2);
		}

		if ($exec) {
            $msg = 'success|Data berhasil disimpan.';
        } else {
            $msg = 'error|Data gagal disimpan.';
        }

        $dataLog = [
            'user_id' => $this->session->userdata('user_id'),
            'modul' => 'Neraca',
            'action' => 'Import Data',
            'data' => json_encode($arrDataLog),
        ];

        write_log($dataLog);

        $this->session->set_flashdata('message', $msg);
		redirect("laporankeuangan/neraca"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function hapus_neraca($bulan = 0, $tahun = 0, $uri = NULL)
	{
		$this->db->delete('neraca2', array('bulan' => $bulan, 'tahun' => $tahun));
		$exec = ($this->db->affected_rows() < 1) ? false : true;
		
		if ($exec) {
            $msg = 'success|Data berhasil dihapus.';
        } else {
            $msg = 'error|Data gagal dihapus.';
        }

        $this->session->set_flashdata('message', $msg);
		redirect(base_url('laporankeuangan/neraca/' . $tahun));
	}

	public function export_neraca($bulan, $tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');
		$style_td_bold_no_bg = $this->excel->style('style_td_bold_no_bg');

		// create file name

		$fileName = 'Neraca_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->laporankeuangan_model->get_detail_neraca($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setSubject("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setDescription("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setKeywords("Laporan Operasional Bulanan");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Neraca  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:B1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:B2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(Dalam Rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:B3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1


		$excel->getActiveSheet()->SetCellValue('A4', 'Uraian');
		$excel->getActiveSheet()->SetCellValue('B4', konversiBulanAngkaKeNama($bulan));

		$excel->getActiveSheet()->SetCellValue('A5', '');
		$excel->getActiveSheet()->SetCellValue('B5', '');

		$no = 1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {

			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['bidang']);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['target']);


			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('A' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}


		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}

		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '8')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '12')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '15')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '16')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '18')->applyFromArray($style_td_bold_no_bg);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	public function lap_bulanan($tahun = 0)
	{

		$year = $this->input->get('tahun');

		$last_year = $this->db->select('tahun')->from('lap_bulanan2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();	
		$year = ($year != '') ? $year : $last_year->tahun;

		$data['thn'] = $year;
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_lap_bulanan();
		$data['lap_bulanan'] = $this->laporankeuangan_model->get_lap_bulanan($year);

		$data['selected_year'] = $year;
		$data['view'] = 'visitor/laporankeuangan/lap_bulanan';
		$this->load->view('admin/layout', $data);
	}
	public function detail_lap_bulanan($bulan = 0, $tahun = 0)
	{
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_lap_bulanan();
		$data['lap_bulanan'] = $this->laporankeuangan_model->get_detail_lap_bulanan($bulan, $tahun);
		$data['view'] = 'detail_lap_bulanan';
		$this->load->view('admin/layout', $data);
	}

	public function tambah_lap_bulanan()
	{

		if (isset($_POST['submit'])) {

			$upload_path = './uploads/excel/laporankeuangan';

			if (!is_dir($upload_path)) {
				mkdir($upload_path, 0777, TRUE);
			}
			//$newName = "hrd-".date('Ymd-His');
			$config = array(
				'upload_path' => $upload_path,
				'allowed_types' => "xlsx",
				'overwrite' => FALSE,
			);

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$upload = $this->upload->data();


			if ($upload) { // Jika proses upload sukses			    	

				$excelreader = new Xlsx;
				$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $upload['file_name']); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				$data['sheet'] = $sheet;
				$data['file_excel'] = $upload['file_name'];


				$data['view'] = 'tambah_lap_bulanan';
				$this->load->view('admin/layout', $data);
			} else {

				echo "gagal";
				$data['view'] = 'tambah_lap_bulanan';
				$this->load->view('admin/layout', $data);
			}
		} else {

			$data['view'] = 'tambah_lap_bulanan';
			$this->load->view('admin/layout', $data);
		}
	}

	public function import_lap_bulanan($file_excel)
	{

		$excelreader = new Xlsx;
		$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $file_excel); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		$data = transposeData($sheet);

		$data2 = array();
		$arrDataLog = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			if ($numrow > 1  && $row['A'] != '') {
				// Kita push (add) array data ke variabel data
				$month = konversi_bulan_ke_angka($data["B"][1]);

				$arrData = array(
					'bidang' => $row['A'],
					'target' => $row['B'],
					'bulan' => $month,
					'tahun' => $data["C"][1],
				);

				$query = $this->db->from('lap_bulanan2')->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["C"][1])->get()->result();

				if (count($query) > 0) {
					$exec = $this->db->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["C"][1])->update('lap_bulanan2', $arrData);
				}
				else {
					array_push($data2, $arrData);
				}
				array_push($arrDataLog, $arrData);
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		if (count($data2) > 0) {
			$exec = $this->laporankeuangan_model->insert_lap_bulanan($data2);
		}

		if ($exec) {
            $msg = 'success|Data berhasil disimpan.';
        } else {
            $msg = 'error|Data gagal disimpan.';
        }

        $dataLog = [
            'user_id' => $this->session->userdata('user_id'),
            'modul' => 'Laporan Operasional Bulanan',
            'action' => 'Import Data',
            'data' => json_encode($data2),
        ];

        write_log($dataLog);

        $this->session->set_flashdata('message', $msg);
		redirect("laporankeuangan/lap_bulanan"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function hapus_lap_bulanan($bulan = 0, $tahun = 0, $uri = NULL)
	{
		$this->db->delete('lap_bulanan2', array('bulan' => $bulan, 'tahun' => $tahun));
		$exec = ($this->db->affected_rows() < 1) ? false : true;
		
		if ($exec) {
            $msg = 'success|Data berhasil dihapus.';
        } else {
            $msg = 'error|Data gagal dihapus.';
        }

        $this->session->set_flashdata('message', $msg);
		redirect(base_url('laporankeuangan/lap_bulanan/' . $tahun));
	}

	public function export_lap_bulanan($bulan, $tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');
		$style_td_bold_no_bg = $this->excel->style('style_td_bold_no_bg');

		// create file name

		$fileName = 'laporan_operasional_bulanan' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->laporankeuangan_model->get_detail_lap_bulanan($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setSubject("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setDescription("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setKeywords("Laporan Operasional Bulanan");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:B1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:B2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(Dalam Rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:B3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1


		$excel->getActiveSheet()->SetCellValue('A4', 'Uraian');
		$excel->getActiveSheet()->SetCellValue('B4', konversiBulanAngkaKeNama($bulan));

		$excel->getActiveSheet()->SetCellValue('A5', '');
		$excel->getActiveSheet()->SetCellValue('B5', '');

		$no = 1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {

			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['bidang']);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['target']);


			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('A' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}


		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}

		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '8')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '12')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '15')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '16')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '18')->applyFromArray($style_td_bold_no_bg);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	public function lap_akumulasi($tahun = 0)
	{

		$year = $this->input->get('tahun');

		$last_year = $this->db->select('tahun')->from('lap_akumulasi2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();	
		$year = ($year != '') ? $year : $last_year->tahun;

		$data['thn'] = $year;
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_lap_akumulasi();
		$data['lap_akumulasi'] = $this->laporankeuangan_model->get_lap_akumulasi($year);

		$data['selected_year'] = $year;
		$data['view'] = 'visitor/laporankeuangan/lap_akumulasi';
		$this->load->view('admin/layout', $data);
	}
	public function detail_lap_akumulasi($bulan = 0, $tahun = 0)
	{
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_lap_akumulasi();
		$data['lap_akumulasi'] = $this->laporankeuangan_model->get_detail_lap_akumulasi($bulan, $tahun);
		$data['view'] = 'detail_lap_akumulasi';
		$this->load->view('admin/layout', $data);
	}

	public function tambah_lap_akumulasi()
	{

		if (isset($_POST['submit'])) {

			$upload_path = './uploads/excel/laporankeuangan';

			if (!is_dir($upload_path)) {
				mkdir($upload_path, 0777, TRUE);
			}
			//$newName = "hrd-".date('Ymd-His');
			$config = array(
				'upload_path' => $upload_path,
				'allowed_types' => "xlsx",
				'overwrite' => FALSE,
			);

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$upload = $this->upload->data();


			if ($upload) { // Jika proses upload sukses			    	

				$excelreader = new Xlsx;
				$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $upload['file_name']); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				$data['sheet'] = $sheet;
				$data['file_excel'] = $upload['file_name'];


				$data['view'] = 'tambah_lap_akumulasi';
				$this->load->view('admin/layout', $data);
			} else {

				echo "gagal";
				$data['view'] = 'tambah_lap_akumulasi';
				$this->load->view('admin/layout', $data);
			}
		} else {

			$data['view'] = 'tambah_lap_akumulasi';
			$this->load->view('admin/layout', $data);
		}
	}

	public function import_lap_akumulasi($file_excel)
	{


		$excelreader = new Xlsx;
		$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $file_excel); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		$data = transposeData($sheet);

		$data2 = array();
		$arrDataLog = array();

		$numrow = 1;

		foreach ($sheet as $row) {
			if ($numrow > 1 && $row['A'] != '') {
				// Kita push (add) array data ke variabel data
				$month = konversi_bulan_ke_angka($data["B"][1]);

				$arrData = array(
					'bidang' => $row['A'],
					'target' => $row['B'],
					'bulan' => $month,
					'tahun' => $data["C"][1],
				);

				$query = $this->db->from('lap_akumulasi2')->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["C"][1])->get()->result();

				if (count($query) > 0) {
					$exec = $this->db->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["C"][1])->update('lap_akumulasi2', $arrData);
				}
				else {
					array_push($data2, $arrData);
				}
				array_push($arrDataLog, $arrData);
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		if (count($data2) > 0) {
			$exec = $this->laporankeuangan_model->insert_lap_akumulasi($data2);
		}

		if ($exec) {
            $msg = 'success|Data berhasil disimpan.';
        } else {
            $msg = 'error|Data gagal disimpan.';
        }

		$dataLog = [
            'user_id' => $this->session->userdata('user_id'),
            'modul' => 'Laporan Operasional Akumulasi',
            'action' => 'Import Data',
            'data' => json_encode($arrDataLog),
        ];

        write_log($dataLog);

		redirect("laporankeuangan/lap_akumulasi"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function hapus_lap_akumulasi($bulan = 0, $tahun = 0, $uri = NULL)
	{
		$this->db->delete('lap_akumulasi2', array('bulan' => $bulan, 'tahun' => $tahun));
		$exec = ($this->db->affected_rows() < 1) ? false : true;
		
		if ($exec) {
            $msg = 'success|Data berhasil dihapus.';
        } else {
            $msg = 'error|Data gagal dihapus.';
        }

        $this->session->set_flashdata('message', $msg);
		redirect(base_url('laporankeuangan/lap_akumulasi/' . $tahun));
	}

	public function export_lap_akumulasi($bulan, $tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');
		$style_td_bold_no_bg = $this->excel->style('style_td_bold_no_bg');

		// create file name

		$fileName = 'laporan_operasional_akumulasi_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->laporankeuangan_model->get_detail_lap_akumulasi($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan Operasional Akumulasi  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setSubject("Laporan Operasional Akumulasi  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setDescription("Laporan Operasional Akumulasi  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setKeywords("Laporan Operasional Akumulasi");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan Operasional Akumulasi  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:B1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:B2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(Dalam Rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:B3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1


		$excel->getActiveSheet()->SetCellValue('A4', 'Uraian');
		$excel->getActiveSheet()->SetCellValue('B4', konversiBulanAngkaKeNama($bulan));

		$excel->getActiveSheet()->SetCellValue('A5', '');
		$excel->getActiveSheet()->SetCellValue('B5', '');

		$no = 1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {

			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['bidang']);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['target']);


			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('A' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}


		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}

		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '8')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '12')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '15')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '16')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '18')->applyFromArray($style_td_bold_no_bg);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	public function perubahan_asetneto($tahun = 0)
	{
		$year = $this->input->get('tahun');

		$last_year = $this->db->select('tahun')->from('perubahan_asetneto2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();	
		$year = ($year != '') ? $year : $last_year->tahun;

		$data['thn'] = $year;
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_perubahan_asetneto();
		$data['perubahan_asetneto'] = $this->laporankeuangan_model->get_perubahan_asetneto($year);
		$data['data_bidang'] = $this->laporankeuangan_model->get_bidang_perubahan_asetneto($year);
		$data['all_data'] = $this->laporankeuangan_model->get_data_perubahan_asetneto($year);

		$data['selected_year'] = $year;
		$data['view'] = 'laporankeuangan/perubahan_asetneto';
		
		$this->load->view('admin/layout', $data);
	}
	public function detail_perubahan_asetneto($bulan = 0, $tahun = 0)
	{
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_perubahan_asetneto();
		$data['perubahan_asetneto'] = $this->laporankeuangan_model->get_detail_perubahan_asetneto($bulan, $tahun);
		$data['view'] = 'detail_perubahan_asetneto';
		$this->load->view('admin/layout', $data);
	}

	public function tambah_perubahan_asetneto()
	{

		if (isset($_POST['submit'])) {

			$upload_path = './uploads/excel/laporankeuangan';

			if (!is_dir($upload_path)) {
				mkdir($upload_path, 0777, TRUE);
			}
			//$newName = "hrd-".date('Ymd-His');
			$config = array(
				'upload_path' => $upload_path,
				'allowed_types' => "xlsx",
				'overwrite' => FALSE,
			);

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$upload = $this->upload->data();


			if ($upload) { // Jika proses upload sukses			    	

				$excelreader = new Xlsx;
				$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $upload['file_name']); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				$data['sheet'] = $sheet;
				$data['file_excel'] = $upload['file_name'];


				$data['view'] = 'tambah_perubahan_asetneto';
				$this->load->view('admin/layout', $data);
			} else {

				echo "gagal";
				$data['view'] = 'tambah_perubahan_asetneto';
				$this->load->view('admin/layout', $data);
			}
		} else {

			$data['view'] = 'tambah_perubahan_asetneto';
			$this->load->view('admin/layout', $data);
		}
	}

	public function import_perubahan_asetneto($file_excel)
	{

		$excelreader = new Xlsx;
		$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $file_excel); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		$data = transposeData($sheet);

		$data2 = array();
		$arrDataLog = array();

		$numrow = 1;

		foreach ($sheet as $row) {
			if ($numrow > 1 && $row['A'] != '') {
				// Kita push (add) array data ke variabel data
				$month = konversi_bulan_ke_angka($data["B"][1]);

				$arrData = array(
					'bidang' => $row['A'],
					'target' => $row['B'],
					'bulan' => $month,
					'tahun' => $data["C"][1],
				);

				$query = $this->db->from('perubahan_asetneto2')->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["C"][1])->get()->result();

				if (count($query) > 0) {
					$exec = $this->db->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["C"][1])->update('perubahan_asetneto2', $arrData);
				}
				else {
					array_push($data2, $arrData);
				}
				array_push($arrDataLog, $arrData);
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		if (count($data2) > 0) {
			$exec = $this->laporankeuangan_model->insert_perubahan_asetneto($data2);
		}

		if ($exec) {
            $msg = 'success|Data berhasil disimpan.';
        } else {
            $msg = 'error|Data gagal disimpan.';
        }

        $dataLog = [
            'user_id' => $this->session->userdata('user_id'),
            'modul' => 'Perubahan Aset Neto',
            'action' => 'Import Data',
            'data' => json_encode($arrDataLog),
        ];

        write_log($dataLog);

        $this->session->set_flashdata('message', $msg);

		redirect("laporankeuangan/perubahan_asetneto"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function hapus_perubahan_asetneto($bulan = 0, $tahun = 0, $uri = NULL)
	{
		$this->db->delete('perubahan_asetneto2', array('bulan' => $bulan, 'tahun' => $tahun));
		$exec = ($this->db->affected_rows() < 1) ? false : true;
		
		if ($exec) {
            $msg = 'success|Data berhasil dihapus.';
        } else {
            $msg = 'error|Data gagal dihapus.';
        }

        $this->session->set_flashdata('message', $msg);
		redirect(base_url('laporankeuangan/perubahan_asetneto/' . $tahun));
	}

	public function export_perubahan_asetneto($bulan, $tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');
		$style_td_bold_no_bg = $this->excel->style('style_td_bold_no_bg');

		// create file name

		$fileName = 'laporan_perubahan_aset_neto_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->laporankeuangan_model->get_detail_perubahan_asetneto($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setSubject("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setDescription("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setKeywords("Laporan Operasional Bulanan");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan Perubahan Aset Neto  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:B1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:B2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(Dalam Rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:B3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1


		$excel->getActiveSheet()->SetCellValue('A4', 'Uraian');
		$excel->getActiveSheet()->SetCellValue('B4', konversiBulanAngkaKeNama($bulan));

		$excel->getActiveSheet()->SetCellValue('A5', '');
		$excel->getActiveSheet()->SetCellValue('B5', '');

		$no = 1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {

			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['bidang']);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['target']);


			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('A' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}


		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}

		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '8')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '12')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '15')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '16')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '18')->applyFromArray($style_td_bold_no_bg);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}

	public function realisasi_anggaran($tahun = 0)
	{

		$year = $this->input->get('tahun');
		$last_year = $this->db->select('tahun')->from('realisasi_anggaran2')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();

		$year = ($year != '') ? $year : $last_year->tahun;

		$data['thn'] = $year;
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_realisasi_anggaran();
		$data['realisasi_anggaran'] = $this->laporankeuangan_model->get_realisasi_anggaran($year);
		
		$data['selected_year'] = $year;
		$data['view'] = 'visitor/laporankeuangan/realisasi_anggaran';
		$this->load->view('admin/layout', $data);
	}
	public function detail_realisasi_anggaran($bulan = 0, $tahun = 0)
	{
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_realisasi_anggaran();
		$data['realisasi_anggaran'] = $this->laporankeuangan_model->get_detail_realisasi_anggaran($bulan, $tahun);
		$data['view'] = 'visitor/laporankeuangan/detail_realisasi_anggaran';
		$this->load->view('admin/layout', $data);
	}

	public function tambah_realisasi_anggaran()
	{

		if (isset($_POST['submit'])) {

			$upload_path = './uploads/excel/bpih';

			if (!is_dir($upload_path)) {
				mkdir($upload_path, 0777, TRUE);
			}
			//$newName = "hrd-".date('Ymd-His');
			$config = array(
				'upload_path' => $upload_path,
				'allowed_types' => "xlsx",
				'overwrite' => FALSE,
			);

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$upload = $this->upload->data();


			if ($upload) { // Jika proses upload sukses			    	

				$excelreader = new Xlsx;
				$loadexcel = $excelreader->load('./uploads/excel/bpih/' . $upload['file_name']); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				$data['sheet'] = $sheet;
				$data['file_excel'] = $upload['file_name'];


				$data['view'] = 'tambah_realisasi_anggaran';
				$this->load->view('admin/layout', $data);
			} else {

				echo "gagal";
				$data['view'] = 'tambah_realisasi_anggaran';
				$this->load->view('admin/layout', $data);
			}
		} else {

			$data['view'] = 'tambah_realisasi_anggaran';
			$this->load->view('admin/layout', $data);
		}
	}

	public function import_realisasi_anggaran($file_excel)
	{


		$excelreader = new Xlsx;
		$loadexcel = $excelreader->load('./uploads/excel/bpih/' . $file_excel); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		$data = transposeData($sheet);

		$data2 = array();
		$arrDataLog = array();

		$numrow = 1;
		foreach ($sheet as $row) {

			if ($numrow > 1) {
				$month = konversi_bulan_ke_angka($data["E"][1]);
				$query = $this->db->from('realisasi_anggaran2')->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["F"][1])->get()->result();
				
				$arrData = array(
					'bidang' => $row['A'],
					'target' => $row['B'],
					'realisasi' => $row['C'],
					'persentase' => $row['D'],
					'bulan' => $month,
					'tahun' => $data["F"][1],
					'upload_by' => $this->session->userdata('user_id'),
				);

				if (count($query) > 0) {
					$exec = $this->db->where('bidang', $row['A'])->where('bulan', $month)->where('tahun', $data["F"][1])->update('realisasi_anggaran2', $arrData);
				}
				else {
					array_push($data2, $arrData);
				}

				array_push($arrDataLog, $data2);
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		if (count($data2) > 0) {
			$exec = $this->laporankeuangan_model->insert_realisasi_anggaran($data2);
		}
		
		if ($exec) {
            $msg = 'success|Data berhasil disimpan.';
        } else {
            $msg = 'error|Data gagal disimpan.';
        }

        $dataLog = [
            'user_id' => $this->session->userdata('user_id'),
            'modul' => 'Laporan Realisasi Anggaran',
            'action' => 'Import Data',
            'data' => json_encode($arrDataLog),
        ];

        write_log($dataLog);

        $this->session->set_flashdata('message', $msg);
		redirect("laporankeuangan/realisasi_anggaran"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function hapus_realisasi_anggaran($bulan = 0, $tahun = 0, $uri = NULL)
	{
		$this->db->delete('realisasi_anggaran2', array('bulan' => $bulan, 'tahun' => $tahun));
		$exec = ($this->db->affected_rows() < 1) ? false : true;
		
		if ($exec) {
            $msg = 'success|Data berhasil dihapus.';
        } else {
            $msg = 'error|Data gagal dihapus.';
        }

        $this->session->set_flashdata('message', $msg);
		redirect(base_url('laporankeuangan/realisasi_anggaran/' . $tahun));
	}

	public function export_realisasi_anggaran($bulan, $tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');

		// create file name

		$fileName = 'laporan_realisasi_anggaran_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->laporankeuangan_model->get_detail_realisasi_anggaran($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan Realisasi Anggaran Bulan " . $bulan . " " . $tahun)
			->setSubject("Laporan Realisasi Anggaran Bulan " . $bulan . " " . $tahun)
			->setDescription("Laporan Realisasi Anggaran Bulan " . $bulan . " " . $tahun)
			->setKeywords("Laporan Operasional Akumulasi");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan Realisasi Anggaran Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->getActiveSheet()->SetCellValue('A4', 'No');
		$excel->getActiveSheet()->SetCellValue('B4', 'Bidang');
		$excel->getActiveSheet()->SetCellValue('C4', 'Target');
		$excel->getActiveSheet()->SetCellValue('D4', 'Realisasi');
		$excel->getActiveSheet()->SetCellValue('E4', 'Persentase');

		$no = 1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {
			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['bidang']);
			$excel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['target']);
			$excel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['realisasi']);
			$excel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['persentase']);


			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}
		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		$excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A13')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A14')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A15')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A18')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A19')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A22')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A23')->getFont()->setBold(TRUE);

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}


	public function lap_arus_kas($tahun = 0)
	{

		// $tahun = ($tahun != '') ? $tahun : date('Y');
		// $data['thn'] = $tahun;
		// $data['tahun'] = $this->laporankeuangan_model->get_tahun_lap_arus_kas();
		// $data['lap_arus_kas'] = $this->laporankeuangan_model->get_lap_arus_kas($tahun);
		// $data['view'] = 'lap_arus_kas';

		$year = $this->input->get('tahun');
		$bank = $this->input->get('bank');
		$last_year = $this->db->select('tahun')->from('tr_lap_arus_kas')->group_by('tahun')->order_by('tahun', 'desc')->get()->row();
		
		$year = ($year != '') ? $year : $last_year->tahun;

		$data['thn'] = $tahun;
		$data['tahun'] = $this->laporankeuangan_model->get_tahun_lap_arus_kas(); // untuk menu pilihan tahun
		$data['month'] = $this->laporankeuangan_model->get_month_lap_arus_kas($year); // untuk menu pilihan tahun
		$data['lap_arus_kas'] = $this->laporankeuangan_model->get_lap_arus_kas($year);
		
		$data['selected_year'] = $year;
		$data['view'] = 'visitor/laporankeuangan/lap_arus_kas';

		$this->load->view('admin/layout', $data);
	}

	public function detail_lap_arus_kas($bulan = 0, $tahun = 0)
	{
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;
		$data['lap_arus_kas'] = $this->laporankeuangan_model->get_detail_lap_arus_kas($bulan, $tahun);
		$data['view'] = 'detail_lap_arus_kas';

		$this->load->view('admin/layout', $data);
	}

	public function tambah_lap_arus_kas()
	{

		if (isset($_POST['submit'])) {

			$upload_path = './uploads/excel/laporankeuangan';

			if (!is_dir($upload_path)) {
				mkdir($upload_path, 0777, TRUE);
			}
			//$newName = "hrd-".date('Ymd-His');
			$config = array(
				'upload_path' => $upload_path,
				'allowed_types' => "xlsx",
				'overwrite' => FALSE,
			);

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$upload = $this->upload->data();


			if ($upload) { // Jika proses upload sukses

				$excelreader = new Xlsx;
				$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $upload['file_name']); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				$data['sheet'] = $sheet;
				$data['file_excel'] = $upload['file_name'];


				$data['view'] = 'tambah_lap_arus_kas';
				$this->load->view('admin/layout', $data);
			} else {

				echo "gagal";
				$data['view'] = 'tambah_lap_arus_kas';
				$this->load->view('admin/layout', $data);
			}
		} else {

			$data['view'] = 'tambah_lap_arus_kas';
			$this->load->view('admin/layout', $data);
		}
	}

	public function hapus_lap_arus_kas_by_id($id, $tahun)
	{
		$this->db->where('id_kas', $id)->delete('tr_lap_arus_kas');

		$exec = ($this->db->affected_rows() < 1) ? false : true;
		
		if ($exec) {
            $msg = 'success|Data berhasil dihapus.';
        } else {
            $msg = 'error|Data gagal dihapus.';
        }

        $this->session->set_flashdata('message', $msg);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function hapus_lap_arus_kas($bulan, $tahun)
	{
		$this->db->where('bulan', $bulan)->where('tahun', $tahun)->delete('tr_lap_arus_kas');

		$exec = ($this->db->affected_rows() < 1) ? false : true;
		
		if ($exec) {
            $msg = 'success|Data berhasil dihapus.';
        } else {
            $msg = 'error|Data gagal dihapus.';
        }

        $this->session->set_flashdata('message', $msg);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function import_lap_arus_kas($file_excel)
	{

		$excelreader = new Xlsx;
		$loadexcel = $excelreader->load('./uploads/excel/laporankeuangan/' . $file_excel); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		$data = transposeData($sheet);
		$month = konversi_bulan_ke_angka($data['B'][1]);

		$arrDataLog = array();

		$numrow = 0;

		foreach ($sheet as $row) {
			if ($numrow > 0 && $row['A'] != '') {

				$dataquery = array(
					'title' => $row['A'],
					'amount' => $row['B'],
					'bulan' => $month,
					'tahun' => $data["C"][1],
					'order' => $numrow++,
				);

				$check = $this->db->from('tr_lap_arus_kas')
									->where('bulan', $month)
									->where('tahun', $data["C"][1])
									->where('title', $row['A'])
									->get()
									->result();
				if (count($check) > 0) {
					$exec = $this->db->where('bulan', $month)
							->where('tahun', $data["C"][1])
							->where('title', $row['A'])
							->update('tr_lap_arus_kas', $dataquery);
				}
				else {
					$exec = $this->db->insert('tr_lap_arus_kas', $dataquery);
				}
			}
			else {
				$numrow++;
			}
		}

        if ($this->db->affected_rows() > 0) {
            $msg = 'success|Data berhasil disimpan.';
        } else {
            $msg = 'error|Data gagal disimpan.';
        }

        $dataLog = [
            'user_id' => $this->session->userdata('user_id'),
            'modul' => 'Laporan Arus Kas',
            'action' => 'Import Data',
            'data' => json_encode($dataquery),
        ];

        write_log($dataLog);

        $this->session->set_flashdata('message', $msg);
		redirect("laporankeuangan/lap_arus_kas"); // Redirect ke halaman awal (ke controller siswa fungsi index)


	}

	public function export_lap_arus_kas($bulan, $tahun)
	{

		// ambil style untuk table dari library Excel.php
		$style_header = $this->excel->style('style_header');
		$style_td = $this->excel->style('style_td');
		$style_td_left = $this->excel->style('style_td_left');
		$style_td_bold = $this->excel->style('style_td_bold');
		$style_td_bold_no_bg = $this->excel->style('style_td_bold_no_bg');

		// create file name

		$fileName = 'Lap_arus_kas_' . $tahun . '-(' . date('d-m-Y H-i-s', time()) . ').xlsx';

		$sebaran = $this->laporankeuangan_model->get_detail_lap_arus_kas($bulan, $tahun);
		$maxcolumn = konversiAngkaKeHuruf(count($sebaran) + 1);
		$excel = new Spreadsheet;

		// Settingan awal file excel
		$excel->getProperties()->setCreator('BPKH')
			->setLastModifiedBy('BPKH')
			->setTitle("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setSubject("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setDescription("Laporan Operasional Bulanan  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun)
			->setKeywords("Laporan Operasional Bulanan");

		//judul baris ke 1
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan Arus Kas  Bulan " . konversiBulanAngkaKeNama($bulan) . " " . $tahun); // 
		$excel->getActiveSheet()->mergeCells('A1:B1'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		//sub judul baris ke 2
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "Badan Pengelola Keuangan Haji Republik Indonesia");
		$excel->getActiveSheet()->mergeCells('A2:B2'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "(Dalam Rupiah)");
		$excel->getActiveSheet()->mergeCells('A3:B3'); // Set Merge Cell pada kolom A1 sampai F1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(10); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1


		$excel->getActiveSheet()->SetCellValue('A4', 'Uraian');
		$excel->getActiveSheet()->SetCellValue('B4', konversiBulanAngkaKeNama($bulan));

		$excel->getActiveSheet()->SetCellValue('A5', '');
		$excel->getActiveSheet()->SetCellValue('B5', '');

		$no = 1;
		$rowCount = 5;
		$last_row = count($sebaran) + 4;
		foreach ($sebaran as $element) {

			$excel->getActiveSheet()->SetCellValue('A' . $rowCount, $element->title);
			$excel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->amount);


			//stile column No
			// $excel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($style_td);

			//header style lainnya
			for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
				$excel->getActiveSheet()->getStyle($i . $rowCount)->applyFromArray($style_td);
			}

			//style column BPS/BPIH
			$excel->getActiveSheet()->getStyle('A' . $rowCount)->applyFromArray($style_td_left);

			$rowCount++;
			$no++;
		}
		//header style
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '4')->applyFromArray($style_header);
		}


		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . $last_row)->applyFromArray($style_td_bold);
		}

		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '8')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '12')->applyFromArray($style_td_bold_no_bg);
		}
		// last row style    		
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getStyle($i . '15')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '16')->applyFromArray($style_td_bold_no_bg);
			$excel->getActiveSheet()->getStyle($i . '18')->applyFromArray($style_td_bold_no_bg);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}

		//auto column width
		for ($i = 'A'; $i <=  $excel->getActiveSheet()->getHighestColumn(); $i++) {
			$excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
		}


		$objWriter = IOFactory::createWriter($excel, "Xlsx");
		$objWriter->save('./uploads/excel/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect('./uploads/excel/' . $fileName);
	}
} //class
