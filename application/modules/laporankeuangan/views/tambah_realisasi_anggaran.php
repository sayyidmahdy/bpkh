<section class="content-header">
  <h1><i class="fa fa-kaaba"></i> &nbsp; Tambah Laporan Realisasi Anggaran </h1>        
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body px-5 mx-5 smy-form-body">
  
        <?php 

        if(isset($_POST['submit'])) {

           // Buat sebuah tag form untuk proses import data ke database
          echo form_open_multipart(base_url('laporankeuangan/import_realisasi_anggaran/'.$file_excel), 'class="form-horizontal"' );

          echo "<p class='alert alert-warning'>Data bulan ".$sheet['1']['B']." " . $sheet['1']['C'] . "</p>";
          echo "<table class='table table-bordered table-striped'>
         
          <tr>
            <th>Uraian</th>
            <th>Anggaran</th>
            <th>Realisasi</th>            
            <th>% Realisasi</th>            
          </tr>";
          
          $numrow = 1;
          $kosong = 0;
          
          // Lakukan perulangan dari data yang ada di excel
          // $sheet adalah variabel yang dikirim dari controller
          $bulan = $sheet['1']['B'];

          foreach($sheet as $row){ 
            // Ambil data pada excel sesuai Kolom            

            $bps_bpih=$row['A']; 
            $jumlah=$row['B']; 
            $bulan = $bulan;
           
           
            if($numrow > 1){            
              
              echo "<tr>";            
              echo "<td>".$bps_bpih."</td>"; 
              echo "<td>".$jumlah."</td>";
              echo "<td>".$row['C']."</td>";
              echo "<td>".$row['D']."</td>";
            
              echo "</tr>";
            }
            
            $numrow++; // Tambah 1 setiap kali looping
          }
          
          echo "</table>";
          
          // Cek apakah variabel kosong lebih dari 0
          // Jika lebih dari 0, berarti ada data yang masih kosong
          if($kosong > 0){
          ?>  
            <script>
            $(document).ready(function(){
              // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
              $("#jumlah_kosong").html('<?php echo $kosong; ?>');
              
              $("#kosong").show(); // Munculkan alert validasi kosong
            });
            </script>
          <?php
          }else{ // Jika semua data sudah diisi
            echo "<hr>";
            
            ?>
            <hr>
            <div class="row">
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <button class='btn btn-primary btn-block' type='submit' name='import'>Import</button>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <a href="<?php echo base_url("nilaimanfaat/penempatan_di_bpsbpih") ?>" class="btn btn-default btn-block">Cancel</a>
                  </div>
                </div>
              </div>

            </div>

            <?php  
          }
          
          echo form_close(); 

        } else {

          echo form_open_multipart(base_url('laporankeuangan/tambah_realisasi_anggaran'), 'class="form-horizontal"' )?> 
            <div class="form-group">
             <p class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i><strong> Panduan Import Data</strong></p>

              <ol class="panduan-pengisian">
                <li>Ekstensi File yang didukung hanya .xlsx</li>
                <li>Data yang diimport harus mengikuti template yang sudah disediakan. </li>
                <li>Kolom Tahun wajib diisi</li>
                <li>Data yang dapat diimport hanya data satu bulan</li>
                <li>Format Tahun : 2020, dst</li>               
                <li>Format bulan  : Januari, Februari, Maret, dst</li>               
              </ol>
            </div>
        
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                <input type="file" name="file" class="form-control" required>
              </div>
            </div>
            <div class="row mt-5">
              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text-right">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <a href="<?=base_url('public/template-excel/lapkeuanganhaji/lap_realisasi_anggaran.xlsx'); ?>" class="btn btn-primary btn-block"><i class="fas fa-file-excel"></i> Unduh Template Excel</a>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right btn-block"><i class="fas fa-upload"></i> Upload</button>
                    <input type="hidden" name="submit" value="Upload" >
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <a href="<?php echo base_url('laporankeuangan/realisasi_anggaran') ?>" class="btn btn-default btn-block">Cancel</a>
                  </div>
                </div>
              </div>

          <?php echo form_close();  
        }
        ?>

         
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>  
</section> 


<script>
    $("#realisasi_anggaran").addClass('active');
</script>



  