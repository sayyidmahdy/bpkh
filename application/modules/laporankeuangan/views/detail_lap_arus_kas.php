<?php //get las URI
    $last = $this->uri->total_segments();
	$tahun = $this->uri->segment($last);	 
	$bulan = $this->uri->segment($last-1);	 
  ?>
<section class="content-header">
	<h1><i class="fa fa-kaaba"></i> Laporan Arus Kas
	</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12 ">
			<div class="row mb-4">
				<div class="col-lg-10">&nbsp;</div>
				<div class="col-lg-2">
					<a href="<?= base_url(url_validation().'laporankeuangan/lap_arus_kas/'.$tahun); ?>" class="btn btn-warning btn-sm btn btn-primary btn-block"><i
					class="fas fa-chevron-left"></i>&nbsp; Kembali</a>
				</div>
			</div>
			<div class="box">
				<div class="box-body">
					<?php if ($lap_arus_kas) { ?>

					<h4>Data <?=konversiBulanAngkaKeNama($bulan)?> <?=$tahun?></h4>
					<div class="row">
			            <div class="col-md-12 text-right">
			              <label><b><i>(dalam rupiah)</i></b></label>
			            </div>
			          </div>
					<table id="table1" class="table table-striped table-bordered">
						<tr>
							<th>No</th>
							<th>Uraian</th>
							<th class="text-center">Instrumen</th>
							<?php if (is_admin() == 1): ?>
								<th class="text-center">Aksi</th>
							<?php endif ?>
						</tr>
						<?php $no=1; foreach ($lap_arus_kas as $row) { ?>
							<?php if ($row->amount == ''): ?>
								<tr class="success text-bold">
									<td><?php echo $no++ ?></td>
									<td colspan="<?php echo (is_admin() == 1) ? '3': '2' ?>"><?php echo $row->title ?></td>
								</tr>
							<?php else: ?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?= $row->title; ?></td>
									<td style="text-align: right;"><?= $row->amount; ?></td>
									<?php if (is_admin() == 1): ?>
										<td class="text-center">
											<a style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger px-4" data-href="<?= base_url(url_validation() . $this->router->fetch_class() . '/hapus_lap_arus_kas_by_id/' . $row->id_kas . '/' . $tahun); ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i> Hapus</a>
										</td>
									<?php endif ?>
								</tr>
							<?php endif ?>
						<?php  } ?>
					</table>
				</div>
			</div>

			<?php } else {
				echo '<p class="alert alert-success"> Data Not Found</p>';
			} ?>
		</div>
	</div>
</section>

<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				<p>Anda yakin ingin menghapus data ini?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function (e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});

</script>

<script>
	$("#lap_arus_kas").addClass('active');

</script>
