<?php
	class Nilaimanfaat_model extends CI_Model{	
		// PER INSTRUMEN

		public function get_tahun_per_instrumen(){
			
			$this->db->select('tahun');
			$this->db->from('per_instrumen');			
			$this->db->where(array('tahun !=' => '0', 'tahun !=' => ''));
			$this->db->order_by('tahun', 'ASC');
			$this->db->group_by('tahun');

			$query = $this->db->get();

			return $result = $query->result_array();
			
		}

		public function get_per_instrumen_export($tahun){
			$this->db->select('*');
			$this->db->from('per_instrumen');
			$this->db->where('tahun', $tahun);
			$this->db->order_by('tahun', 'ASC');
    		$query = $this->db->get(); 
    		return $result = $query->result_array();
  		}

		public function get_per_instrumen($tahun){
			$this->db->select('*');
			$this->db->from('tr_per_instrumen');
			$this->db->where('tahun', $tahun);
			$query = $this->db->get();

			return $result = $query->result();
  		}
  		public function get_detail_per_instrumen($id){
			$this->db->select('*');
			$this->db->from('per_instrumen');
			$this->db->where('id_per_instrumen', $id);
    		$query = $this->db->get(); 
    		return $result = $query->row_array();
  		}		  
		  
		public function insert_per_instrumen($data){
		    $this->db->insert_batch('tr_per_instrumen', $data);
		}

		//NILAI MANFAAT PER BPS BPIH
		public function get_tahun_nilai_manfaat_penempatan_di_bpsbpih(){
			
			$this->db->select('tahun');
			$this->db->from('tr_nilai_manfaat_penempatan_di_bpsbpih');			
			$this->db->where(array('tahun !=' => '0', 'tahun !=' => ''));
			$this->db->order_by('tahun', 'ASC');
			$this->db->group_by('tahun');

			$query = $this->db->get();

			return $result = $query->result();			
		}

		public function get_bps_bpih_nilai_manfaat_penempatan_di_bpsbpih(){
			
			$this->db->select('*');
			$this->db->from('tr_nilai_manfaat_penempatan_di_bpsbpih');		
			$query = $this->db->get();

			return $result = $query->result();			
		}

		public function get_nilai_manfaat_penempatan_di_bpsbpih($tahun){
			
			$this->db->select('*');
			$this->db->from('tr_nilai_manfaat_penempatan_di_bpsbpih');			
			$this->db->where('tahun', $tahun);
			$query = $this->db->get();

			return $result = $query->result();			
		}
	
  		public function insert_nilai_manfaat_penempatan_di_bpsbpih($data){
		    $this->db->insert_batch('tr_nilai_manfaat_penempatan_di_bpsbpih', $data);
		    return ($this->db->affected_rows() < 1) ? false : true;
		}	

		//NILAI MANFAAT PER PRODUK
		public function get_tahun_nilai_manfaat_produk(){
			
			$this->db->select('tahun');
			$this->db->from('tr_nilai_manfaat_produk');			
			$this->db->where(array('tahun !=' => '0', 'tahun !=' => ''));
			$this->db->group_by('tahun');

			$query = $this->db->get();

			return $result = $query->result();
			
		}

		public function get_bps_bpih_nilai_manfaat_produk(){
			
			$this->db->select('*');
			$this->db->from('tr_nilai_manfaat_produk');	
			$this->db->group_by('bidang');
			$query = $this->db->get();

			return $result = $query->result();
			
		}

		public function get_nilai_manfaat_produk($tahun){
			
			$this->db->select('*');
			$this->db->from('tr_nilai_manfaat_produk');			
			$this->db->where('tahun', $tahun);
			$query = $this->db->get();

			return $result = $query->result();			
		}
	
  		public function insert_nilai_manfaat_produk($data){
		    $this->db->insert_batch('tr_nilai_manfaat_produk', $data);
		    return ($this->db->affected_rows() < 1) ? false : true;
		}

	}
