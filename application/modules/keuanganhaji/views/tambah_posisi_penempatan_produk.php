<section class="content-header">
  <h1><i class="fa fa-kaaba"></i> &nbsp; Tambah Dana Haji yang Ditempatkan  </h1>        
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body px-5 mx-5 smy-form-body">
  
        <?php 

        if(isset($_POST['submit'])) {

           // Buat sebuah tag form untuk proses import data ke database
          echo form_open_multipart(base_url('keuanganhaji/import_posisi_penempatan_produk/'.$file_excel), 'class="form-horizontal"' );

          
          echo "<table class='table table-bordered table-striped'>
         
          <tr>
            <th>Bulan</th>
            <th>Giro</th>
            <th>Tabungan</th>
            <th>Deposito</th>
            <th>Jumlah</th>            
          </tr>";
          
          $numrow = 1;     

          foreach($sheet as $row){ 
            // Ambil data pada excel sesuai Kolom            

            $bulan=$row['A']; 
            $giro=$row['B']; 
            $tabungan=$row['B']; 
            $deposito=$row['C']; 
            $jumlah=$row['D'];
           

            if($numrow > 1){
              
              echo "<tr>";
              echo "<td>".$bulan."</td>"; 
              echo "<td>".$giro."</td>";
              echo "<td>".$tabungan."</td>";
              echo "<td>".$deposito."</td>";
              echo "<td>".$jumlah."</td>";            
              echo "</tr>";
            }
            
            $numrow++; // Tambah 1 setiap kali looping
          }
          
          echo "</table>";          
       
          ?>  
          <hr>
          <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                  <button class='btn btn-primary btn-block' type='submit' name='import'>Import</button>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                  <a href="<?php echo base_url("keuanganhaji/posisi_penempatan_produk") ?>" class="btn btn-default btn-block">Cancel</a>
                </div>
              </div>
            </div>

          </div>
          <?php
          
          echo form_close(); 

        } else {

          echo form_open_multipart(base_url('keuanganhaji/tambah_posisi_penempatan_produk'), 'class="form-horizontal"' )?> 
           
            <div class="form-group">
              <p class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i><strong> Panduan Import Data</strong></p>

              <ol class="panduan-pengisian">
                <li>Ekstensi File yang didukung hanya .xlsx</li>
                <li>Data yang diimport harus mengikuti template yang sudah disediakan.</li>
                <li>Kolom Tahun wajib diisi</li>
                <li>Data yang dapat diimport hanya data satu bulan</li>
                <li>Format Tahun : 2020, dst</li>               
                <li>Format bulan  : Januari, Februari, Maret, dst</li>               
              </ol>
              
            </div>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                <input type="file" name="file" class="form-control" required="">
              </div>
            </div>
            <div class="row mt-5">
              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text-right">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <a href="<?=base_url('public/template-excel/keuanganhaji/posisi_penempatan_produk.xlsx'); ?>" class="btn btn-primary btn-block"><i class="fas fa-file-excel"></i> Unduh Template Excel</a>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right btn-block"><i class="fas fa-upload"></i> Upload</button>
                    <input type="hidden" name="submit" value="Upload" >
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <a href="<?php echo base_url('keuanganhaji/posisi_penempatan_produk') ?>" class="btn btn-default btn-block">Cancel</a>
                  </div>
                </div>
              </div>
              
            </div>

          <?php echo form_close();  
        }
        ?>

         
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>  
</section> 


<script>
    $("#posisi_penempatan_produk").addClass('active');
</script>



  