<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function dd($data){
	print_r($data); exit();
}

function search_class(){
	if (is_admin() == 1) {
		$class = 'col-lg-7 col-md-7 col-sm-12 col-xs-12 p-0';
	}
	else {
		$class = 'col-lg-8 col-md-6 col-sm-12 col-xs-12';
	}

	return $class;
}

function generate_class(){
	if (is_admin() == 1) {
		$class = 'col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right p-0';
	}
	else {
		$class = 'col-lg-4 col-md-6 col-sm-12 col-xs-12 text-right p-0';
	}

	return $class;
}

function year_class(){
	$class = 'col-lg-4 col-md-4 col-sm-12 col-xs-12';

	return $class;
}

function create_class(){
	$class = 'col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right p-0';

	return $class;
}

function form_group_class(){
	if (is_admin() == 1) {
		$class = '';
	}
	else {
		$class = 'padding-left';
	}

	return $class;
}

function url_validation(){
	if (is_admin() == 1) {
		$url = '';
	}
	else {
		$url = 'visitor/';
	}

	return $url;
}

function dropdown_year(){

	$year = [];
	$first_year = 2015;
	for ($i=0; $i <= 10; $i++) { 
		$year[]['tahun'] = $first_year ++;
	}

	?>
		<select class="form-control select2" name="tahun" id="tahun">
          <option value="0">-- Silahkan Pilih Tahun --</option>
          <?php foreach ($year as $rowTahun): ?>

            <option value="<?php echo $rowTahun['tahun'] ?>"><?php echo $rowTahun['tahun'] ?></option>
            
          <?php endforeach ?>
        </select>
	<?php
	
}

function dropdown_year_dashboard(){

	$year = [];
	$first_year = 2018;
	for ($i=0; $i <= 10; $i++) { 
		$year[]['tahun'] = $first_year ++;
	}

	?>
		<select class="form-control select2" name="tahun" id="tahun" onChange="change_year(this.value)">
          <?php foreach ($year as $rowTahun): ?>

            <option value="<?php echo $rowTahun['tahun'] ?>"><?php echo $rowTahun['tahun'] ?></option>
            
          <?php endforeach ?>
        </select>
	<?php
	
}

function sum_data_by_month($data){
		$arrMonth = [
			'januari',
			'februari',
			'maret',
			'april',
			'mei',
			'juni',
			'juli',
			'agustus',
			'september',
			'oktober',
			'november',
			'desember'
		];
		
		$total = 0;

		if ($data) {
			for ($i=0; $i < 12 ; $i++) { 
				
				$getMonth = $arrMonth[$i];
				$total += (int) str_replace(',', '', $data->$getMonth);
			}
		}

		$formatNumber = number_format($total, 2);
		
		// $convert = $this->formatCurrency($formatNumber);
		$arrData = [
			'unformated' => $total,
			'formated' => $formatNumber
		];

		return $arrData;
}

function formatCurrency($data){
	$data = str_replace(' ', '', $data);

	$text = array('','','Rb','Jt','M','T');
	$input = $data;
	$input = explode(',',$input);
	$label = (count($input) > 6) ? 'T' : $text[count($input)];
	
	return $input[0].(count($input)>1?','.round($input[1]/10):'').' '.$label;
}

function formatCurrency_max_milyar($data){
	// $data = '4,359,660,000,000.00';
	$data = str_replace(' ', '', $data);
	$text = array('','','Rb','Jt','M','T');
	$input = $data;
	$input = explode(',',$input);
	
	if ($text[count($input)] == 'T') {
		$return = $input[0]. ',' . $input[1] .(count($input)>1?','.round($input[2]/10):'').' M';
	}
	else {
		$return = $input[0].(count($input)>1?','.round($input[1]/10):'').' '.$text[count($input)];

	}
	
	return $data;
}

function convert_month_to_array($data){
	$arrMonth = [
		'januari',
		'februari',
		'maret',
		'april',
		'mei',
		'juni',
		'juli',
		'agustus',
		'september',
		'oktober',
		'november',
		'desember'
	];
	
	$total = 0;
	$arrData = [];
	
	if ($data) 		
		for ($i=0; $i < 12 ; $i++) { {

			$getMonth = $arrMonth[$i];
			$month = ucfirst($getMonth);

			$arrData[] = [
				"year" => ($month == 'Agustus') ? 'Aug' : substr($month, 0, 3),
				"income" => $data->$getMonth,
			];
		}
	}

	return json_encode($arrData);
}

function convert_str($data){
	$comma = str_replace(',', '', $data);
	$space = str_replace(' ', '', $comma);
	return $space;
}