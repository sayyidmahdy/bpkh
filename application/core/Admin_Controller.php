<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $url = $this->router->fetch_method();
        $search = 'export';
        
        if(!preg_match("/{$search}/i", $url)) {
            
            if( !($this->session->userdata('role') == 1) )
            {
                show_error('Anda tidak memiliki akses pada halaman ini.');
            }
        }

    }
}